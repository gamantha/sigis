<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\jui\Datepicker;
use app\modules\organization\models\Employee ;

/* @var $this yii\web\View */
/* @var $model app\modules\hardware\models\Schedule */
/* @var $form yii\widgets\ActiveForm */
?>

    <h1><?php
    /*
    echo '1. raw + rule pair<br/>2. raw-rule pair yang terakhir yang menang<br/>3. absensi dilhat dari schedule group<br/><br/>';
echo 'user id ' . $_GET['id'];
echo '<br/>';
$employee = Employee::find()->andWhere(['id' => $_GET['id']])->One();
echo 'PIN ' . $employee->org_id;
echo '<br/>';
echo 'Organization ' . $employee->departmentRole->department->organization_id;

*/
     ?></h1>
<div class="schedule-form">

    <?php $form = ActiveForm::begin(); ?>


    <?php

    echo '<br/>start period';
    echo Datepicker::widget([
     'name'=>'from',
     'value'=>$from,
         'dateFormat' => 'yyyy-MM-dd',
          'options' => ['class' => 'form-control','style' => 'z-index:99;'],
    ]);
    echo '<br/>finish period';
    echo Datepicker::widget([
     'name'=>'to',
          'value'=>$to,
         'dateFormat' => 'yyyy-MM-dd',
          'options' => ['class' => 'form-control','maxlength'=>'25','style' => 'z-index:99;'],
    ]);
?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' =>'btn btn-primary', 'name'=>'search']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>



<div class="schedule-index">
    <h1><?= Html::encode($this->title) ?></h1>
<?php

$grid_columns =  [
 'footer' => 'sdfadadsa',
   // ['class' => 'yii\grid\SerialColumn'],
   'datetime',
   [
'label' => 'day',
'value' => function($model) {
return  isset($model['datetime']) ? date_format( date_create($model['datetime']),"l") : '';
}

   ],
   'name',
   [
    'attribute' => 'name',
    'footerOptions'=>[
     //'id' => 'tunjangan_jabatan_' . $project->id,

     'label' => 'sada',
     'value' => '0',
 ],


   ],
   'schedule_type',
   'optional',
   'masuk',
   'keluar',
   'deltamasuk',
   'deltakeluar',
    //'id',

];
/*
echo  GridView::widget([
        'dataProvider' => $absensiDataProvider,
     //   'filterModel' => $searchModel,
     'showFooter'=>TRUE,
'columns' =>$grid_columns,

    ]);
*/
    ?>






</div>
