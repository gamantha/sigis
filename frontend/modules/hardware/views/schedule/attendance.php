<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\jui\Datepicker;
use app\modules\organization\models\Employee ;
use app\modules\organization\models\UserEmployee ;
use app\modules\organization\models\Department;

/* @var $this yii\web\View */
/* @var $model app\modules\hardware\models\Schedule */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Report Absensi');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Personalia'), 'url' => ['default/ketentuanpersonalia']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji dan Upah'), 'url' => ['default/gajidanupah']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/gajibulanan']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/perhitungan']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/absensi']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/reportabsensi']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <h1><?php
    /*
    echo '1. raw + rule pair<br/>2. raw-rule pair yang terakhir yang menang<br/>3. absensi dilhat dari schedule group<br/><br/>';
echo 'user id ' . $_GET['id'];
echo '<br/>';
$employee = Employee::find()->andWhere(['id' => $_GET['id']])->One();
echo 'PIN ' . $employee->org_id;
echo '<br/>';
echo 'Organization ' . $employee->departmentRole->department->organization_id;

*/
     ?></h1>
<div class="schedule-form">

    <?php $form = ActiveForm::begin(); ?>


    <?php


    //print_r($dept_roles);

    $employee = Employee::userEmployee();
    //echo $employee->departmentRole->department->organization->id;



echo 'PIN<br/>';
    echo Html::textInput('id',$id,

      ['class' => 'form-control']
    );
    echo '<br/>start period';
    echo Datepicker::widget([
     'name'=>'from',
     'value'=>$from,
         'dateFormat' => 'yyyy-MM-dd',
          'options' => ['class' => 'form-control','style' => 'z-index:99;'],
    ]);
    echo '<br/>finish period';
    echo Datepicker::widget([
     'name'=>'to',
          'value'=>$to,
         'dateFormat' => 'yyyy-MM-dd',
          'options' => ['class' => 'form-control','maxlength'=>'25','style' => 'z-index:99;'],
    ]);
?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' =>'btn btn-primary', 'name'=>'search']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>



<div class="schedule-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>

    </p>

<?php
$total_masuk = 0;
$grid_columns =  [
   // ['class' => 'yii\grid\SerialColumn'],

   [
'label' => 'datetime',
'value' => function($model, $key, $index, $column) {
//return  isset($model['datetime']) ? date_format( date_create($model['datetime']),"l") : '';
return $key;
},
   'footer' => 'TOTAL KEHADIRAN',],

   [
'label' => 'day',
'value' => function($model, $key, $index, $column) {
return  isset($key) ? date_format(date_create($key),"l") : '';

},
   ],

   [
    'label' => 'ada data rawnya?',
    'value' => function($model, $key, $index, $column) {
    return  (key($model) == 'NONE') ? key($model): 'YES';
    //return key($model);

    },
   ],

   [
    'label' => 'Optional',
    'value' => function($model, $key, $index, $column) {
    //return  ($model['name'] == 'NO DATA') ? $model['name']: 'YES';
    $retmodel = [];
    foreach ($model as $modell){
     $retmodel = $modell;
    }
    //return $model[key($model)]['optional'];
    return $retmodel['optional'];
    //return sizeof($model);
    //return print_r($model);
    },
   ],

  // 'name',
   //'schedule_type',
   //'optional',
   ['label' => 'masuk',
       'value' => function($model, $key, $index, $column) {
        $retmodel = [];
        foreach ($model as $modell){
         if($modell['grouping'] == 'in'){
            $retmodel = $modell;
         }

        }
            return  (isset($retmodel['masuk'])) ? $retmodel['masuk'] : '';
       },
  ],

  ['label' => 'Terlambat',
      'value' => function($model, $key, $index, $column) {
       $retmodel = [];
       foreach ($model as $modell){
        if($modell['grouping'] == 'in'){
           $retmodel = $modell;
        }
if(isset($retmodel['deltamasuk']->y))
{

 $interval = $retmodel['deltamasuk'];
/*
  if(isset($interval->s)){
   $uraian = $interval->h . ' hours ' . $interval->i . ' minutes ' . $interval->s . ' second';
  } else {
     $uraian = 'p';
  }
  */
  //$uraian = date_format($dt,"Y/m/d H:i:s");

$uraian = 'telat';
} else {
 $interval = new DateInterval("P0D");
 $uraian = '';
}

       }
  //         return  (isset($retmodel['deltamasuk'])) ? $uraian : '';
  return $uraian;
      },
      'pageSummary' => function ($summary, $data, $widget) {
       $summary = 0;
    foreach ($data as $dat) {
     if (empty($dat)) {
     } else {
       $summary++;
     }
    }
       return $summary; },
 ],

 ['label' => 'Keluar',
     'value' => function($model, $key, $index, $column) {
      $retmodel = [];
      foreach ($model as $modell){
       if($modell['grouping'] == 'out'){
          $retmodel = $modell;
       }

      }
          return  (isset($retmodel['keluar'])) ? $retmodel['keluar'] : '';
     },
],

['label' => 'Delta keluar',
    'value' => function($model, $key, $index, $column) {
      $uraian = 'fad';
     $retmodel = [];


return $uraian;
    },

],
/*
   'keluar',
   //'delta_keluar',
   [
    'label' => 'delta keluar',
    'attribute' => 'deltakeluar',
      'pageSummary' => function ($summary, $data, $widget) {
         $summary = 0;
    foreach ($data as $dat) {
     if (empty($dat)) {
     } else {
       $summary++;
     }
    }
       return $summary; },
   ],

*/
   [
      'label' => 'Masuk count',
      //'format' => 'raw',
      'pageSummary' => true,
      'value' => function($models) {
       $masuk = 0;
       $keluar = 0;
       foreach($models as $model) {

if(isset($model['masuk'])){
 $masuk = 1;
}

if(isset($model['keluar'])){
 $keluar = 1;
}

       }

if($masuk && $keluar) {
 return '1';
} else {
      return '0';
}

      },

      //'footer' => $total_masuk,
   ],
   /*
   [
    'label' => 'Lembur',
    'pageSummary' => true,
    'value' => function($model) {
     $return = '';
   if(isset($model['lembur_status'])) {
    $return = ($model['lembur_status'] == 'approved') ? '1' : '';
   }
   return $return;
    },
   // 'value' => function($model) {
   // return  ($model['lembur_status'] == 'approved') ? 1 : '';
   // },

   ],
   [
      'label' => 'Cuti',
            'pageSummary' => true,
            'value' => function($model) {
             $return = '';
           if(isset($model['leave_type'])) {
            $return = ($model['leave_type'] == 'cuti') ? '1' : '';
           }
           return $return;
            },
      //      'value' => function($model) {
        //    return  ($model['leave_type'] == 'cuti') ? 1 : '';
          //  },
   ],
   [
      'label' => 'Ijin',
      'value' => function($model) {
       $return = '';
     if(isset($model['leave_type'])) {
      $return = ($model['leave_type'] == 'ijin') ? '1' : '';
     }
     return $return;
      },
   ],
   [
      'label' => 'Sakit',
            'pageSummary' => true,
            'value' => function($model) {
             $return = '';
           if(isset($model['leave_type'])) {
            $return = ($model['leave_type'] == 'sakit') ? '1' : '';
           }
           return $return;
            },
   ],
   [
      'label' => 'Alpa',
            'pageSummary' => true,
            'value' => function($model) {
             $return = '';
           if(isset($model['leave_type'])) {
            $return = ($model['leave_type'] == 'alpa') ? '1' : '';
           }
           return $return;
            },
   ],
   [
      'label' => 'Libur',
            'pageSummary' => true,
      'value' => function($model) {
         $return = '';
       if(isset($model['optional'])) {
        $return = ($model['optional'] == 'exception') ? '1' : '';
       }
       return $return;
      },
   ],
*/

];

echo GridView::widget([
        'dataProvider' => $activeDataProvider,
     //   'filterModel' => $searchModel,
     'showFooter'=>TRUE,
'columns' =>$grid_columns,
//'columns' => ['id',],

'options' => [
 'style'=>'width: 1800px; margin-left: -300px;',

],
'showPageSummary' => true,

    ]);

    echo '<br/><br/>';
    echo '<pre>';
    echo 'CATATAN<br/>';
    echo '1. ';
    print_r($activearray);

    echo '</pre>';
    ?>







</div>
