<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\hardware\models\Schedule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="schedule-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'department_role_id')->textInput() ?>

    <?= $form->field($model, 'start_period')->textInput() ?>

    <?= $form->field($model, 'finish_period')->textInput() ?>

    <?= $form->field($model, 'schedule_type')->dropDownList([ 'onetime' => 'Onetime', 'yearly' => 'Yearly', 'monthly' => 'Monthly', 'weekly' => 'Weekly', 'daily' => 'Daily', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'day')->textInput() ?>

    <?= $form->field($model, 'start_time')->textInput() ?>

    <?= $form->field($model, 'finish_time')->textInput() ?>

    <?= $form->field($model, 'start_scan')->textInput() ?>

    <?= $form->field($model, 'finish_scan')->textInput() ?>

    <?= $form->field($model, 'optional')->dropDownList([ 'false' => 'False', 'true' => 'True', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'priority')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
