<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\hardware\models\RawSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="raw-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'terminal_id') ?>

    <?= $form->field($model, 'pin') ?>

    <?= $form->field($model, 'datetime') ?>

    <?= $form->field($model, 'verified') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'workcode') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
