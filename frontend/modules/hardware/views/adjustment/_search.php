<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\hardware\models\AdjustmentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adjustment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'start_period') ?>

    <?= $form->field($model, 'finish_period') ?>

    <?= $form->field($model, 'tipe') ?>

    <?= $form->field($model, 'value_1') ?>

    <?php // echo $form->field($model, 'value_2') ?>

    <?php // echo $form->field($model, 'value_3') ?>

    <?php // echo $form->field($model, 'employee_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
