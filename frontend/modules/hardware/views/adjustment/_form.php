<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\hardware\models\Adjustment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adjustment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'start_period')->textInput() ?>

    <?= $form->field($model, 'finish_period')->textInput() ?>

    <?= $form->field($model, 'tipe')->dropDownList([ 'kasbon' => 'Kasbon', 'pph' => 'Pph', 'telat' => 'Telat', 'bpjs' => 'Bpjs', 'lembur' => 'Lembur', 'keluar kota' => 'Keluar kota', 'cuti' => 'Cuti', 'lain-lain' => 'Lain-lain', 'makan' => 'Makan', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'value_1')->textInput() ?>

    <?= $form->field($model, 'value_2')->textInput() ?>

    <?= $form->field($model, 'value_3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'employee_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
