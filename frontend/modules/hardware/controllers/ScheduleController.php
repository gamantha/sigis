<?php

namespace app\modules\hardware\controllers;

use Yii;
use common\components\AbsensiDataProvider;
use app\modules\hardware\models\Schedule;
use app\modules\hardware\models\ScheduleGrouping;
use app\modules\hardware\models\Terminal ;
use app\modules\hardware\models\Raw ;
use app\modules\hardware\models\RawSearch ;
use app\modules\hardware\models\ScheduleSearch;
use app\modules\hardware\models\Leave ;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\SqlDataProvider;
use app\modules\organization\models\Employee;
use app\modules\organization\models\DepartmentRole;
use app\modules\organization\models\Department;


/**
 * ScheduleController implements the CRUD actions for Schedule model.
 */
class ScheduleController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Schedule models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScheduleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Schedule model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
public function actionRekapitulasi( $from, $to)
{

 if (Yii::$app->request->post())
 {

  $from = Yii::$app->request->post()['from'];
   $to = Yii::$app->request->post()['to'];

  return $this->redirect(['rekapitulasi', 'from'=>$from ,'to'=>$to]);
}
$employee = Employee::userEmployee();
$organization_id = $employee->departmentRole->department->organization->id;


//$department_role_id_list = [2,3,4,5];
$department_id_array = Department::find()
->andWhere(['organization_id' => $organization_id])
->asArray()
->All();

$department_id_list = ArrayHelper::getColumn($department_id_array, 'id');
//$department_id_list = [2,3];
echo '<br/><br/>';
echo '<br/><br/>';
echo '<pre>';
print_r($department_id_array);
echo '</pre>';


$department_role_id_list = DepartmentRole::find()
->andWhere(['in','department_id', $department_id_list])
->asArray()
->All();

$terminals = Terminal::find()->andWhere(['status'=>'active', 'organization_id' => $organization_id])->asArray()->All();
$terminalarray2 = ArrayHelper::map($terminals, 'id', 'id');
$terminalarray = array_values($terminalarray2);

$fromdate=date_create($from);
$datecounter = $fromdate;
$todate=date_create($to);

while ($datecounter <= $todate) //iterate for every date
{

 $employeemodels = Employee::find()
  ->andWhere(['in','department_role_id', $department_role_id_list])
 ->andWhere(['status'=>'active'])
 ->All();


echo date_format($datecounter,"Y-m-d") . '<br/>';
//print_r($department_role_id_list);
       $datecounter->add(new \DateInterval('P1D'));
}

 return $this->render('rekapitulasi', [

     'from' => $from,
     'to' => $to,
   //  'absensiDataProvider' => $absensiDataProvider,
 ]);




}












public function getActiveArray($id, $from, $to) {

 $employee = Employee::userEmployee();
 $organization_id = $employee->departmentRole->department->organization->id;

 ### PASTIKAN ada GET FROM, TO, ID
 if (Yii::$app->request->post())
 {
  $from = Yii::$app->request->post()['from'];
  $to = Yii::$app->request->post()['to'];
  $id = Yii::$app->request->post()['id'];
  return $this->redirect(['attendance', 'id' => $id, 'from'=>$from ,'to'=>$to]);
 }
 ### Get semua daftar role untuk organisasi tersebut
 $dept_list_array = Department::find()->andWhere(['organization_id' => $organization_id])->asArray()->All();
 $dept_list = ArrayHelper::getColumn($dept_list_array, 'id');
 $dept_role_list_array = DepartmentRole::find()->andWhere(['in','department_id',$dept_list])->asArray()->All();
 $dept_role_list = ArrayHelper::getColumn($dept_role_list_array, 'id');

 ### Setelah dapat daftar semua role, get One employee yang aktif yang merupakan member dari role2 tersebut
 $employeemodel = Employee::find()
 ->andWhere(['org_id' => $id, 'status'=>'active'])
 ->andWhere(['in','department_role_id',$dept_role_list])
 ->One();
 $absensiarray = array();
 $activearray = array();
 $leavearray = array();

 ### Kalau ada employee-nya maka lanjut

 if(sizeof($employeemodel) > 0) {
  $organization_id = $employeemodel->departmentRole->department->organization_id;
  $department_role_id = $employeemodel->department_role_id;
  $pin = $employeemodel->org_id;

  ### Get semua mesin yang aktif untuk organisasi tersebut
  $terminals = Terminal::find()->andWhere(['status'=>'active', 'organization_id' => $organization_id])->asArray()->All();
  $terminalarray2 = ArrayHelper::map($terminals, 'id', 'id');
  $terminalarray = array_values($terminalarray2);

  ### Loop untuk setiap hari dari FROM sampai TO
  $fromdate=date_create($from);
  $datecounter = $fromdate;
  $todate=date_create($to);
  while ($datecounter <= $todate)
  {
   //echo date_format($datecounter,"Y-m-d") . '<br/>';
   ### get semua raws dari mesin yang aktif UNTUK HARI ITU SAJA
   $rawsinadate = Raw::find()
   ->andWhere(['>=','datetime',date_format($datecounter,"Y-m-d")])
   ->andWhere(['<=','datetime',date_format($datecounter->add(new \DateInterval('P1D')),"Y-m-d")])
   ->andWhere(['in','pin', $pin])->andWhere(['in','terminal_id', $terminalarray])
   ->orderBy('datetime ASC')
   ->asArray()->All();
   ### Harus di sub lagi datenya karena diatas tadi di add
   $datecounter->sub(new \DateInterval('P1D'));
   ### get schedule untuk periode from-to dengan urutan order dimulai dari TIPE -> OPTIONAL -> PRIORITY
   $schedules = Schedule::find()->andWhere(['department_role_id' => $department_role_id])
   ->andWhere(['or',['start_period' => null], ['<=','start_period',date_format($datecounter,"Y-m-d")]])
   ->andWhere(['or',['finish_period' => null], ['>=','finish_period',date_format($datecounter,"Y-m-d")]])
   ->orderBy([new \yii\db\Expression('FIELD (schedule_type, "daily","weekly", "monthly", "yearly","onetime")')])
   //->addOrderBy([new \yii\db\Expression('FIELD (optional, "true","false", "exception")')])
   ->addOrderBy('priority ASC')
   ->asArray()
   ->All();

    $absensiarray[date_format($datecounter,"Y-m-d")]['schedules']['daily'] = [];
    $absensiarray[date_format($datecounter,"Y-m-d")]['schedules']['weekly'] = [];


  if(sizeof($schedules) > 0) {
   foreach($schedules as $schedule)
   {
    if ($schedule['schedule_type'] == 'daily') {
     $absensiarray = $this->matchWithDailySchedule($rawsinadate, $schedule,$absensiarray,$datecounter);
     array_push($absensiarray[date_format($datecounter,"Y-m-d")]['schedules']['daily'], $schedule);
    }
    else if ($schedule['schedule_type'] == 'weekly' && (date_format($datecounter,"w") == $schedule['day'])) {
     $absensiarray = $this->matchWithWeeklySchedule($rawsinadate, $schedule,$absensiarray,$datecounter);
     array_push($absensiarray[date_format($datecounter,"Y-m-d")]['schedules']['weekly'], $schedule);
    }
   }
  } else {
   ### NO SCHEDULE THEN tidak ada alpa
   $absensiarray[date_format($datecounter,"Y-m-d")]['schedules'] = 'NO SCHEDULE';
  }




   $activearray = $this->getActive($activearray, $absensiarray, $datecounter);
   $datecounter->add(new \DateInterval('P1D'));
  }

  if (sizeof($absensiarray) > 0) {
   array_shift($absensiarray);
  }
  if (sizeof($leavearray) > 0) {
   array_shift($leavearray);
  }


  $searchModel = new RawSearch();
  $query = Raw::find()->andWhere(['in', 'terminal_id', $terminalarray])
  ->andWhere(['in', 'pin', $pin])
  ->andWhere(['>=', 'datetime',$from])
  ->andWhere(['<=', 'datetime', $to]);

} else {

}

$absensiDataProvider = new ArrayDataProvider([
//'query' => $schedulequery,
'allModels' => $absensiarray,
'pagination' => [
    'pageSize' => 40,
],
]);
$activeDataProvider = new ArrayDataProvider([
//'query' => $schedulequery,
'allModels' => $activearray,
'pagination' => [
    'pageSize' => 40,
],
]);

/*
  return $this->render('attendance', [
      'id'=>$id,
      'from' => $from,
      'to' => $to,
      'absensiDataProvider' => $absensiDataProvider,
      'absensiarray' => $absensiarray,
      'activeDataProvider' => $activeDataProvider,
  ]);
  */

  return $activearray;

}









public function actionAttendance($id, $from, $to)
    {
      $schedules = new Schedule();
     $activearray = $schedules->activeArray($id, $from, $to);

     if (Yii::$app->request->post())
     {
      $from = Yii::$app->request->post()['from'];
      $to = Yii::$app->request->post()['to'];
      $id = Yii::$app->request->post()['id'];
      return $this->redirect(['attendance', 'id' => $id, 'from'=>$from ,'to'=>$to]);
     }

    $absensiarray = $schedules->absensiArray($id, $from, $to);

$absensiDataProvider = new ArrayDataProvider([
    //'query' => $schedulequery,
'allModels' => $absensiarray,
    'pagination' => [
        'pageSize' => 40,
    ],
]);
$activeDataProvider = new ArrayDataProvider([
    //'query' => $schedulequery,
'allModels' => $activearray,
    'pagination' => [
        'pageSize' => 40,
    ],
]);
      return $this->render('attendance', [
          'id'=>$id,
          'from' => $from,
          'to' => $to,
          'absensiDataProvider' => $absensiDataProvider,
          'absensiarray' => $absensiarray,
          'activeDataProvider' => $activeDataProvider,
          'activearray' => $activearray
      ]);



    }



    public function getActive($activearray, $absensiarray, $datecounter)
    {


     if (isset($absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily']))
     {
       $actives = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'];
       foreach($actives as $activekey => $activevalue)
       {
        $ingroup = $this->processGrouping($activevalue['schedule']['id']);

       $activearray[date_format($datecounter,"Y-m-d")][$absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'][$activekey]['raw']] = $activevalue;
               $activearray[date_format($datecounter,"Y-m-d")][$absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'][$activekey]['raw']]['grouping'] = $ingroup[0];

               if ($ingroup[0] == 'in')
               {
                //$absensiarray = $this->processGroupingIn($absensiarray, $datecounter, $rawindate, $activevalue['schedule']);

    $rawindate = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'][$activekey]['raw'];
                $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['masuk'] = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'][$activekey]['raw'];


    if ($rawindate != 'NONE') {

                if ((date_create(date_format(date_create($activevalue['schedule']['finish_time']), "H:i:s"))) < (date_create(date_format(date_create($rawindate), "H:i:s"))))
                {
                 $interval = date_create(date_format(date_create($rawindate), "H:i:s"))->diff(date_create(date_format(date_create($activevalue['schedule']['finish_time']), "H:i:s")));
                // $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltamasuk'] = $interval->h . ' hours ' . $interval->i . ' minutes ' . $interval->s . ' second';
                 $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltamasuk'] = $interval;
                } else {
                 $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltamasuk']  = '';
                }

    }


               } else if ($ingroup[0] == 'out') {
                $rawindate = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'][$activekey]['raw'];
                $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['keluar'] = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'][$activekey]['raw'];


                if ($rawindate != 'NONE') {

                                if ((date_create(date_format(date_create($activevalue['schedule']['start_time']), "H:i:s"))) > (date_create(date_format(date_create($rawindate), "H:i:s"))))
                                {
                                 $interval = date_create(date_format(date_create($rawindate), "H:i:s"))->diff(date_create(date_format(date_create($activevalue['schedule']['start_time']), "H:i:s")));
                                 $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltakeluar'] = $interval->h . ' hours ' . $interval->i . ' minutes ' . $interval->s . ' second';
                                } else {
                                 $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltakeluar']  = '';
                                }

                }


               } else {
               // $absensiarray[date_format($datecounter,"Y-m-d")]['grouping'] = 'false';
               }

       }
      }

//COBA KALAU tidak perlu di bedakan antara daily dan weekly. semuany aakan masuk ke satu array, array tanggal dan yang prioritasnya terbesar yang menang


    //COBA KALAU tidak perlu di bedakan antara daily dan weekly. semuany aakan masuk ke satu array, array tanggal dan yang prioritasnya terbesar yang menang

       if (isset($absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly']))
       {
        $weeklyactives = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'];
        foreach($weeklyactives as $weeklyactivekey => $weeklyactivevalue)
        {
         $ingroup = $this->processGrouping($weeklyactivevalue['schedule']['id']);
         $activearray[date_format($datecounter,"Y-m-d")][$absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$weeklyactivekey]['raw']] = $weeklyactivevalue;
         $activearray[date_format($datecounter,"Y-m-d")][$absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$weeklyactivekey]['raw']]['grouping'] = $ingroup[0];
             if ($ingroup[0] == 'in')
               {
                $rawindate = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$weeklyactivekey]['raw'];
                $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['masuk'] = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$weeklyactivekey]['raw'];
                if ($rawindate != 'NONE') {
                 if ((date_create(date_format(date_create($weeklyactivevalue['schedule']['finish_time']), "H:i:s"))) < (date_create(date_format(date_create($rawindate), "H:i:s"))))
                 {
                  $interval = date_create(date_format(date_create($rawindate), "H:i:s"))->diff(date_create(date_format(date_create($weeklyactivevalue['schedule']['finish_time']), "H:i:s")));
                  $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltamasuk'] = $interval;
                 } else {
                  $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltamasuk']  = '';
                 }
                }
          } else if ($ingroup[0] == 'out') {
                $rawindate = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$weeklyactivekey]['raw'];
                $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['keluar'] = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$weeklyactivekey]['raw'];


                if ($rawindate != 'NONE') {

                                if ((date_create(date_format(date_create($weeklyactivevalue['schedule']['start_time']), "H:i:s"))) > (date_create(date_format(date_create($rawindate), "H:i:s"))))
                                {
                                 $interval = date_create(date_format(date_create($rawindate), "H:i:s"))->diff(date_create(date_format(date_create($weeklyactivevalue['schedule']['start_time']), "H:i:s")));
                                 $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltakeluar'] = $interval->h . ' hours ' . $interval->i . ' minutes ' . $interval->s . ' second';
                                } else {
                                 $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltakeluar']  = '';
                                }

                }
               } else {
               }
        }

       }

     return $activearray;
    }












    public function matchWithDailySchedule($rawsinadate, $schedule,$absensiarray,$datecounter)
    {
     $tobepushed = [];
     $alpa = true;






     if (sizeof($rawsinadate) > 0) {
      foreach($rawsinadate as $rawindate)
      {
       if ((date_format(date_create($rawindate['datetime']), "H:i:s") >= date_format(date_create($schedule['start_scan']), "H:i:s")) &&
       (date_format(date_create($rawindate['datetime']), "H:i:s") <= date_format(date_create($schedule['finish_scan']), "H:i:s"))
       )
       {
         $tobepushed['raw'] = $rawindate['datetime'];
         $tobepushed['optional'] = $schedule['optional'];
         $tobepushed['schedule'] = $schedule;
         $absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'][$schedule['id']] =  $tobepushed;
         $alpa = false;
        }
      }
     } else {
      ### check schedule is optional or exception. OPTIONAL & EXCEPTION ONLY WORK ON DAYS
      if($schedule['optional'] == 'true'){
       $alpa = false;
       $tobepushed['raw'] = 'NONE';
                $tobepushed['optional'] = $schedule['optional'];

      } else if($schedule['optional'] == 'exception'){
            $alpa = false;
            $tobepushed['raw'] = 'NONE';
                     $tobepushed['optional'] = $schedule['optional'];

     } else {
      $alpa = true;
      $tobepushed['raw'] = 'NONE';
               $tobepushed['optional'] = $schedule['optional'];

     }
     $tobepushed['schedule'] = $schedule;
     $absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'][$schedule['id']] =  $tobepushed;
     }

     return $absensiarray;
    }












    /**
     * Creates a new Schedule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Schedule();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Schedule model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Schedule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }
















    /**
     * Finds the Schedule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Schedule the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Schedule::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
