<?php

namespace app\modules\hardware;

class Hardware extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\hardware\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
