<?php

namespace app\modules\hardware\models;

/**
 * This is the ActiveQuery class for [[TerminalModel]].
 *
 * @see TerminalModel
 */
class TerminalModelQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TerminalModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TerminalModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}