<?php

namespace app\modules\hardware\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\hardware\models\Schedule;

/**
 * ScheduleSearch represents the model behind the search form about `app\modules\hardware\models\Schedule`.
 */
class ScheduleSearch extends Schedule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'department_role_id', 'day', 'priority'], 'integer'],
            [['name', 'start_period', 'finish_period', 'schedule_type', 'start_time', 'finish_time', 'start_scan', 'finish_scan', 'optional'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Schedule::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'department_role_id' => $this->department_role_id,
            'start_period' => $this->start_period,
            'finish_period' => $this->finish_period,
            'day' => $this->day,
            'start_time' => $this->start_time,
            'finish_time' => $this->finish_time,
            'start_scan' => $this->start_scan,
            'finish_scan' => $this->finish_scan,
            'priority' => $this->priority,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'schedule_type', $this->schedule_type])
            ->andFilterWhere(['like', 'optional', $this->optional]);

        return $dataProvider;
    }
}
