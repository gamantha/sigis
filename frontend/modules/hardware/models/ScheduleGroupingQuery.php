<?php

namespace app\modules\hardware\models;

/**
 * This is the ActiveQuery class for [[ScheduleGrouping]].
 *
 * @see ScheduleGrouping
 */
class ScheduleGroupingQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ScheduleGrouping[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ScheduleGrouping|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}