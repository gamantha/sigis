<?php

namespace app\modules\hardware\models;

use Yii;

/**
 * This is the model class for table "terminal".
 *
 * @property integer $id
 * @property integer $organization_id
 * @property integer $model_id
 * @property string $name
 * @property string $configuration
 * @property string $status
 *
 * @property Raw[] $raws
 * @property TerminalModel $model
 * @property Organization $organization
 */
class Terminal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'terminal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['organization_id', 'model_id'], 'integer'],
            [['configuration', 'status'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'organization_id' => Yii::t('app', 'Organization ID'),
            'model_id' => Yii::t('app', 'Model ID'),
            'name' => Yii::t('app', 'Name'),
            'configuration' => Yii::t('app', 'Configuration'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRaws()
    {
        return $this->hasMany(Raw::className(), ['terminal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(TerminalModel::className(), ['id' => 'model_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganization()
    {
        return $this->hasOne(Organization::className(), ['id' => 'organization_id']);
    }

    /**
     * @inheritdoc
     * @return TerminalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TerminalQuery(get_called_class());
    }
}
