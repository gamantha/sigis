<?php

namespace app\modules\hardware\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\hardware\models\Adjustment;

/**
 * AdjustmentSearch represents the model behind the search form about `app\modules\hardware\models\Adjustment`.
 */
class AdjustmentSearch extends Adjustment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'value_1', 'value_2', 'employee_id'], 'integer'],
            [['start_period', 'finish_period', 'tipe', 'value_3'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Adjustment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'start_period' => $this->start_period,
            'finish_period' => $this->finish_period,
            'value_1' => $this->value_1,
            'value_2' => $this->value_2,
            'employee_id' => $this->employee_id,
        ]);

        $query->andFilterWhere(['like', 'tipe', $this->tipe])
            ->andFilterWhere(['like', 'value_3', $this->value_3]);

        return $dataProvider;
    }
}
