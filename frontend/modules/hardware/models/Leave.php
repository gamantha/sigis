<?php

namespace app\modules\hardware\models;

use Yii;

/**
 * This is the model class for table "leave".
 *
 * @property integer $id
 * @property string $leave_type
 * @property integer $employee_id
 * @property string $start_period
 * @property string $finish_period
 * @property string $status
 */
class Leave extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'leave';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['leave_type', 'status'], 'string'],
            [['employee_id'], 'integer'],
            [['start_period', 'finish_period'], 'safe'],
             //[['start_period', 'finish_period'], 'date','format' => 'Y-m-d'],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'leave_type' => Yii::t('app', 'Leave Type'),
            'employee_id' => Yii::t('app', 'Employee ID'),
            'start_period' => Yii::t('app', 'Start Period'),
            'finish_period' => Yii::t('app', 'Finish Period'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @inheritdoc
     * @return LeaveQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LeaveQuery(get_called_class());
    }
}
