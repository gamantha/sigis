<?php

namespace app\modules\hardware\models;


use Yii;
use app\modules\organization\models\Employee;
use app\modules\organization\models\DepartmentRole;
use app\modules\organization\models\Department;
use app\modules\hardware\models\Adjustment;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

/**
 * This is the model class for table "schedule".
 *
 * @property integer $id
 * @property string $name
 * @property integer $department_role_id
 * @property string $start_period
 * @property string $finish_period
 * @property string $schedule_type
 * @property integer $day
 * @property string $start_time
 * @property string $finish_time
 * @property string $start_scan
 * @property string $finish_scan
 * @property string $optional
 * @property integer $priority
 *
 * @property DepartmentRole $departmentRole
 */
class Schedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public function countTelat($id,$from,$to){
        $haritelat=0;
        $adjustment = Adjustment::find()
            ->andWhere(['employee_id'=> $id])
            ->andWhere(['tipe'=>'telat'])
            ->andWhere(['in', 'start_period', $_REQUEST['start_period']])
            ->andWhere(['in', 'finish_period', $_REQUEST['finish_period']])
            ->One();
        if (isset($adjustment)){
            $nilaiadjustment = 1;
            $nilaiadjustment = $adjustment->value_1;
        }
        else {
            $nilaiadjustment = 2;
        }
        return $nilaiadjustment;
    }

    public function getDenda($id){
        $denda=1;
        return $denda;
    }

    public static function tableName()
    {
        return 'schedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['department_role_id', 'day', 'priority'], 'integer'],
            [['start_period', 'finish_period', 'start_time', 'finish_time', 'start_scan', 'finish_scan'], 'safe'],
            [['schedule_type', 'optional'], 'required'],
            [['schedule_type', 'optional'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'department_role_id' => Yii::t('app', 'Department Role ID'),
            'start_period' => Yii::t('app', 'Start Period'),
            'finish_period' => Yii::t('app', 'Finish Period'),
            'schedule_type' => Yii::t('app', 'Schedule Type'),
            'day' => Yii::t('app', 'Day'),
            'start_time' => Yii::t('app', 'Start Time'),
            'finish_time' => Yii::t('app', 'Finish Time'),
            'start_scan' => Yii::t('app', 'Start Scan'),
            'finish_scan' => Yii::t('app', 'Finish Scan'),
            'optional' => Yii::t('app', 'Optional'),
            'priority' => Yii::t('app', 'Priority'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentRole()
    {
        return $this->hasOne(DepartmentRole::className(), ['id' => 'department_role_id']);
    }

    /**
     * @inheritdoc
     * @return ScheduleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ScheduleQuery(get_called_class());
    }



    public function activeArray($id, $from, $to) {
     $employee = Employee::userEmployee();
     $organization_id = $employee->departmentRole->department->organization->id;
     ### PASTIKAN ada GET FROM, TO, ID

     ### Get semua daftar role untuk organisasi tersebut
     $dept_list_array = Department::find()->andWhere(['organization_id' => $organization_id])->asArray()->All();
     $dept_list = ArrayHelper::getColumn($dept_list_array, 'id');
     $dept_role_list_array = DepartmentRole::find()->andWhere(['in','department_id',$dept_list])->asArray()->All();
     $dept_role_list = ArrayHelper::getColumn($dept_role_list_array, 'id');

     ### Setelah dapat daftar semua role, get One employee yang aktif yang merupakan member dari role2 tersebut
     $employeemodel = Employee::find()
     ->andWhere(['org_id' => $id, 'status'=>'active'])
     ->andWhere(['in','department_role_id',$dept_role_list])
     ->One();
     $absensiarray = array();
     $activearray = array();
     $leavearray = array();

     ### Kalau ada employee-nya maka lanjut
     if(sizeof($employeemodel) > 0) {
      $organization_id = $employeemodel->departmentRole->department->organization_id;
      $department_role_id = $employeemodel->department_role_id;
      $pin = $employeemodel->org_id;

      ### Get semua mesin yang aktif untuk organisasi tersebut
      $terminals = Terminal::find()->andWhere(['status'=>'active', 'organization_id' => $organization_id])->asArray()->All();
      $terminalarray2 = ArrayHelper::map($terminals, 'id', 'id');
      $terminalarray = array_values($terminalarray2);

      ### Loop untuk setiap hari dari FROM sampai TO
      $fromdate=date_create($from);
      $datecounter = $fromdate;
      $todate=date_create($to);
      while ($datecounter <= $todate)
      {

       //echo date_format($datecounter,"Y-m-d") . '<br/>';
       ### get semua raws dari mesin yang aktif UNTUK HARI ITU SAJA
//print_r($pin);
       $rawsinadate = Raw::find()
       ->andWhere(['>=','datetime',date_format($datecounter,"Y-m-d")])
       ->andWhere(['<=','datetime',date_format($datecounter->add(new \DateInterval('P1D')),"Y-m-d")])
       ->andWhere(['in','pin', [$pin]])
       ->andWhere(['in','terminal_id', $terminalarray])
       ->orderBy('datetime ASC')
       ->asArray()
       ->All();
       ### Harus di sub lagi datenya karena diatas tadi di add

       $datecounter->sub(new \DateInterval('P1D'));

       ### get schedule untuk periode from-to dengan urutan order dimulai dari TIPE -> OPTIONAL -> PRIORITY
       $schedules = Schedule::find()->andWhere(['department_role_id' => $department_role_id])
       ->andWhere(['or',['start_period' => null], ['<=','start_period',date_format($datecounter,"Y-m-d")]])
       ->andWhere(['or',['finish_period' => null], ['>=','finish_period',date_format($datecounter,"Y-m-d")]])
       ->orderBy([new \yii\db\Expression('FIELD (schedule_type, "daily","weekly", "monthly", "yearly","onetime")')])
       //->addOrderBy([new \yii\db\Expression('FIELD (optional, "true","false", "exception")')])
       ->addOrderBy('priority ASC')
       ->asArray()
       ->All();

        $absensiarray[date_format($datecounter,"Y-m-d")]['schedules']['daily'] = [];
        $absensiarray[date_format($datecounter,"Y-m-d")]['schedules']['weekly'] = [];





      if(sizeof($schedules) > 0) {
       foreach($schedules as $schedule)
       {
        if ($schedule['schedule_type'] == 'daily') {
         $absensiarray = $this->matchWithDailySchedule($rawsinadate, $schedule,$absensiarray,$datecounter);
         array_push($absensiarray[date_format($datecounter,"Y-m-d")]['schedules']['daily'], $schedule);
        }
        else if ($schedule['schedule_type'] == 'weekly' && (date_format($datecounter,"w") == $schedule['day'])) {
         $absensiarray = $this->matchWithWeeklySchedule($rawsinadate, $schedule,$absensiarray,$datecounter);
         array_push($absensiarray[date_format($datecounter,"Y-m-d")]['schedules']['weekly'], $schedule);
        }
       }
      } else {
       ### NO SCHEDULE THEN tidak ada alpa
       $absensiarray[date_format($datecounter,"Y-m-d")]['schedules'] = 'NO SCHEDULE';
      }

       $activearray = $this->getActive($activearray, $absensiarray, $datecounter);

       $datecounter->add(new \DateInterval('P1D'));

      }


      if (sizeof($absensiarray) > 0) {
       array_shift($absensiarray);
      }
      if (sizeof($leavearray) > 0) {
       array_shift($leavearray);
      }


      $searchModel = new RawSearch();
      $query = Raw::find()->andWhere(['in', 'terminal_id', $terminalarray])
      ->andWhere(['in', 'pin', $pin])
      ->andWhere(['>=', 'datetime',$from])
      ->andWhere(['<=', 'datetime', $to]);


    }
      return $activearray;


    }

    public function getActive($activearray, $absensiarray, $datecounter)
    {


     if (isset($absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily']))
     {
       $actives = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'];
       foreach($actives as $activekey => $activevalue)
       {
        $ingroup = $this->processGrouping($activevalue['schedule']['id']);

       $activearray[date_format($datecounter,"Y-m-d")][$absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'][$activekey]['raw']] = $activevalue;
               $activearray[date_format($datecounter,"Y-m-d")][$absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'][$activekey]['raw']]['grouping'] = $ingroup[0];

               if ($ingroup[0] == 'in')
               {
                //$absensiarray = $this->processGroupingIn($absensiarray, $datecounter, $rawindate, $activevalue['schedule']);

    $rawindate = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'][$activekey]['raw'];
                $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['masuk'] = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'][$activekey]['raw'];


    if ($rawindate != 'NONE') {

                if ((date_create(date_format(date_create($activevalue['schedule']['finish_time']), "H:i:s"))) < (date_create(date_format(date_create($rawindate), "H:i:s"))))
                {
                 $interval = date_create(date_format(date_create($rawindate), "H:i:s"))->diff(date_create(date_format(date_create($activevalue['schedule']['finish_time']), "H:i:s")));
                // $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltamasuk'] = $interval->h . ' hours ' . $interval->i . ' minutes ' . $interval->s . ' second';
                 $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltamasuk'] = $interval;
                } else {
                 $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltamasuk']  = '';
                }

    }


               } else if ($ingroup[0] == 'out') {
                $rawindate = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'][$activekey]['raw'];
                $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['keluar'] = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'][$activekey]['raw'];


                if ($rawindate != 'NONE') {

                                if ((date_create(date_format(date_create($activevalue['schedule']['start_time']), "H:i:s"))) > (date_create(date_format(date_create($rawindate), "H:i:s"))))
                                {
                                 $interval = date_create(date_format(date_create($rawindate), "H:i:s"))->diff(date_create(date_format(date_create($activevalue['schedule']['start_time']), "H:i:s")));
                                 $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltakeluar'] = $interval->h . ' hours ' . $interval->i . ' minutes ' . $interval->s . ' second';
                                } else {
                                 $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltakeluar']  = '';
                                }

                }


               } else {
               // $absensiarray[date_format($datecounter,"Y-m-d")]['grouping'] = 'false';
               }

       }
      }

    //COBA KALAU tidak perlu di bedakan antara daily dan weekly. semuany aakan masuk ke satu array, array tanggal dan yang prioritasnya terbesar yang menang
       if (isset($absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly']))
       {
        $weeklyactives = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'];
        foreach($weeklyactives as $weeklyactivekey => $weeklyactivevalue)
        {
         $ingroup = $this->processGrouping($weeklyactivevalue['schedule']['id']);
         $activearray[date_format($datecounter,"Y-m-d")][$absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$weeklyactivekey]['raw']] = $weeklyactivevalue;
         $activearray[date_format($datecounter,"Y-m-d")][$absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$weeklyactivekey]['raw']]['grouping'] = $ingroup[0];
             if ($ingroup[0] == 'in')
               {
                $rawindate = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$weeklyactivekey]['raw'];
                $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['masuk'] = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$weeklyactivekey]['raw'];
                if ($rawindate != 'NONE') {
                 if ((date_create(date_format(date_create($weeklyactivevalue['schedule']['finish_time']), "H:i:s"))) < (date_create(date_format(date_create($rawindate), "H:i:s"))))
                 {
                  $interval = date_create(date_format(date_create($rawindate), "H:i:s"))->diff(date_create(date_format(date_create($weeklyactivevalue['schedule']['finish_time']), "H:i:s")));
                  $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltamasuk'] = $interval;
                 } else {
                  $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltamasuk']  = '';
                 }
                }
          } else if ($ingroup[0] == 'out') {
                $rawindate = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$weeklyactivekey]['raw'];
                $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['keluar'] = $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$weeklyactivekey]['raw'];


                if ($rawindate != 'NONE') {

                                if ((date_create(date_format(date_create($weeklyactivevalue['schedule']['start_time']), "H:i:s"))) > (date_create(date_format(date_create($rawindate), "H:i:s"))))
                                {
                                 $interval = date_create(date_format(date_create($rawindate), "H:i:s"))->diff(date_create(date_format(date_create($weeklyactivevalue['schedule']['start_time']), "H:i:s")));
                                 $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltakeluar'] = $interval->h . ' hours ' . $interval->i . ' minutes ' . $interval->s . ' second';
                                } else {
                                 $activearray[date_format($datecounter,"Y-m-d")][$rawindate]['deltakeluar']  = '';
                                }

                }
               } else {
               }
        }

       }

     return $activearray;
    }


public function absensiArray($id, $from, $to){
$absensiarray = $this->activeArray($id, $from, $to);

 return $absensiarray;
}
public function test()
{
 echo 'test';
}




    public function matchWithDailySchedule($rawsinadate, $schedule,$absensiarray,$datecounter)
    {

     $tobepushed = [];
     $alpa = true;

     if (sizeof($rawsinadate) > 0) {
      foreach($rawsinadate as $rawindate)
      {
       if ((date_format(date_create($rawindate['datetime']), "H:i:s") >= date_format(date_create($schedule['start_scan']), "H:i:s")) &&
       (date_format(date_create($rawindate['datetime']), "H:i:s") <= date_format(date_create($schedule['finish_scan']), "H:i:s"))
       )
       {
         $tobepushed['raw'] = $rawindate['datetime'];
         $tobepushed['optional'] = $schedule['optional'];
         $tobepushed['schedule'] = $schedule;
         $absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'][$schedule['id']] =  $tobepushed;
         $alpa = false;
        }
      }
     } else {
      ### check schedule is optional or exception. OPTIONAL & EXCEPTION ONLY WORK ON DAYS
      if($schedule['optional'] == 'true'){
       $alpa = false;
       $tobepushed['raw'] = 'NONE';
                $tobepushed['optional'] = $schedule['optional'];

      } else if($schedule['optional'] == 'exception'){
            $alpa = false;
            $tobepushed['raw'] = 'NONE';
                     $tobepushed['optional'] = $schedule['optional'];

     } else {
      $alpa = true;
      $tobepushed['raw'] = 'NONE';
               $tobepushed['optional'] = $schedule['optional'];

     }
     $tobepushed['schedule'] = $schedule;
     $absensiarray[date_format($datecounter,"Y-m-d")]['active']['daily'][$schedule['id']] =  $tobepushed;
     }

     return $absensiarray;

    }








    public function matchWithWeeklySchedule($rawsinadate, $schedule,$absensiarray,$datecounter)
    {
     $tobepushed = [];
     //$alpa = true;
     //echo '<br/><br/><br/> day of week : ' . date_format($datecounter,"w");


     if($schedule['optional'] == 'true'){
              $tobepushed['optional'] = $schedule['optional'];
                      $tobepushed['schedule'] = $schedule;
              if (sizeof($rawsinadate) > 0) {
               foreach($rawsinadate as $rawindate)
               {
                if ((date_format(date_create($rawindate['datetime']), "H:i:s") >= date_format(date_create($schedule['start_scan']), "H:i:s"))
                && (date_format(date_create($rawindate['datetime']), "H:i:s") <= date_format(date_create($schedule['finish_scan']), "H:i:s")))
                {
                  $tobepushed['raw'] = $rawindate['datetime'];
                  $tobepushed['optional'] = $schedule['optional'];

                  $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$schedule['id']] =  $tobepushed;
                  $alpa = false;
                 } else {
                  $tobepushed['raw'] = $rawindate['datetime'];
                  $tobepushed['optional'] = $schedule['optional'];

                  $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$schedule['id']] =  $tobepushed;
                 }
               }
              } else {
               $tobepushed['raw'] = 'NONE';
               $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$schedule['id']] =  $tobepushed;
              }




     } else if($schedule['optional'] == 'exception'){
      $tobepushed['optional'] = $schedule['optional'];
              $tobepushed['schedule'] = $schedule;

      if (sizeof($rawsinadate) > 0) {
       foreach($rawsinadate as $rawindate)
       {
        if ((date_format(date_create($rawindate['datetime']), "H:i:s") >= date_format(date_create($schedule['start_scan']), "H:i:s"))
        && (date_format(date_create($rawindate['datetime']), "H:i:s") <= date_format(date_create($schedule['finish_scan']), "H:i:s")))
        {
          $tobepushed['raw'] = $rawindate['datetime'];
          $tobepushed['optional'] = $schedule['optional'];

          $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$schedule['id']] =  $tobepushed;
          $alpa = false;
         } else {
          $tobepushed['raw'] = $rawindate['datetime'];
          $tobepushed['optional'] = $schedule['optional'];

          $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$schedule['id']] =  $tobepushed;
         }
       }
      } else {
       $tobepushed['raw'] = 'NONE';
            $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$schedule['id']] =  $tobepushed;

      }


    } else {

     $tobepushed['optional'] = $schedule['optional'];
             $tobepushed['schedule'] = $schedule;
     if (sizeof($rawsinadate) > 0) {
      foreach($rawsinadate as $rawindate)
      {
       if ((date_format(date_create($rawindate['datetime']), "H:i:s") >= date_format(date_create($schedule['start_scan']), "H:i:s"))
       && (date_format(date_create($rawindate['datetime']), "H:i:s") <= date_format(date_create($schedule['finish_scan']), "H:i:s")))
       {
         $tobepushed['raw'] = $rawindate['datetime'];
         $tobepushed['optional'] = $schedule['optional'];

         $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$schedule['id']] =  $tobepushed;
         $alpa = false;
        }
      }
     } else {
      $tobepushed['raw'] = 'NONE';
      $absensiarray[date_format($datecounter,"Y-m-d")]['active']['weekly'][$schedule['id']] =  $tobepushed;
     }
    }




     return $absensiarray;
    }




public function lemburKaryawan($employee_id, $from, $to)
{

 $fromdate=date_create($from);
 $datecounter = $fromdate;
 $todate=date_create($to);
 $absensiarray[] = array();
 $jumlahlembur = 0;

     while ($datecounter <= $todate) {

      $lemburs = Leave::find()
      ->andWhere(['employee_id' => $employee_id])
            ->andWhere(['leave_type' => 'lembur'])
            ->andWhere(['status' => 'approved'])
            ->andWhere(['<=','start_period',date_format($datecounter->add(new \DateInterval('P1D')),"Y-m-d")])
             ->andWhere(['>=','finish_period',date_format($datecounter->sub(new \DateInterval('P1D')),"Y-m-d")])

      ->All();
      foreach ($lemburs as $lembur){
      $jumlahlembur++;
       $absensiarray[date_format($datecounter,"Y-m-d")]['lembur'] = $lembur->leave_type;
       // $leavearray[date_format($datecounter,"Y-m-d")]['employee_id'] = $leave->employee_id;
           $absensiarray[date_format($datecounter,"Y-m-d")]['lembur_status'] = $lembur->status;
      }

      $datecounter->add(new \DateInterval('P1D'));
     }

 return $jumlahlembur;

}


    public function processGrouping($schedule_id)
    {
     $grouping_in =  ScheduleGrouping::find()
     ->orWhere(['schedule_id_in'=>$schedule_id])
      ->One();
      $grouping_out =  ScheduleGrouping::find()
      ->orWhere(['schedule_id_out'=>$schedule_id])
       ->One();
       if (sizeof($grouping_in) > 0 ) {
         return ['in', $grouping_in];
       }
       if (sizeof($grouping_out) > 0 ) {
               return ['out', $grouping_out];
       }
    }




    public function processGroupingIn($absensiarray, $datecounter, $rawindate, $schedule)
    {

     $absensiarray[date_format($datecounter,"Y-m-d")]['masuk'] = $rawindate['datetime'];
     if ((date_create(date_format(date_create($schedule['finish_time']), "H:i:s"))) < (date_create(date_format(date_create($rawindate['datetime']), "H:i:s"))))
     {
      $interval = date_create(date_format(date_create($rawindate['datetime']), "H:i:s"))->diff(date_create(date_format(date_create($schedule['finish_time']), "H:i:s")));
      $absensiarray[date_format($datecounter,"Y-m-d")]['deltamasuk'] = $interval;
     } else {
      $absensiarray[date_format($datecounter,"Y-m-d")]['deltamasuk'] = '';
     }

     $absensiarray[date_format($datecounter,"Y-m-d")]['grouping'] = 'true';
     $groupmodel = $this->processGrouping($schedule['id'])[1];
     $absensiarray[date_format($datecounter,"Y-m-d")]['name'] = $groupmodel->group_name;
     return $absensiarray;
    }

    public function processGroupingOut($absensiarray, $datecounter, $rawindate, $schedule)
    {
     if ((date_create(date_format(date_create($schedule['finish_time']), "H:i:s"))) < (date_create(date_format(date_create($rawindate['datetime']), "H:i:s")))) {
      $interval = date_create(date_format(date_create($rawindate['datetime']), "H:i:s"))->diff(date_create(date_format(date_create($schedule['start_time']), "H:i:s")));
      $absensiarray[date_format($datecounter,"Y-m-d")]['deltakeluar'] = $interval->h . ' hours ' . $interval->i . ' minutes ' . $interval->s . ' second';
     } else {
      $absensiarray[date_format($datecounter,"Y-m-d")]['deltakeluar'] = '';
     }
     $absensiarray[date_format($datecounter,"Y-m-d")]['keluar'] = $rawindate['datetime'];
     $absensiarray[date_format($datecounter,"Y-m-d")]['grouping'] = 'true';
     $groupmodel = $this->processGrouping($schedule['id'])[1];
     $absensiarray[date_format($datecounter,"Y-m-d")]['name'] = $groupmodel->group_name;
     return $absensiarray;
    }




}
