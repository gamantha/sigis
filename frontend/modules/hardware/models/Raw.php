<?php

namespace app\modules\hardware\models;

use Yii;

/**
 * This is the model class for table "raw".
 *
 * @property string $id
 * @property integer $terminal_id
 * @property string $pin
 * @property string $datetime
 * @property string $verified
 * @property string $status
 * @property string $workcode
 * @property string $created_at
 *
 * @property Terminal $terminal
 */
class Raw extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'raw';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['terminal_id'], 'integer'],
            [['datetime', 'created_at'], 'safe'],
            [['pin', 'verified', 'status', 'workcode'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'terminal_id' => Yii::t('app', 'Terminal ID'),
            'pin' => Yii::t('app', 'Pin'),
            'datetime' => Yii::t('app', 'Datetime'),
            'verified' => Yii::t('app', 'Verified'),
            'status' => Yii::t('app', 'Status'),
            'workcode' => Yii::t('app', 'Workcode'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerminal()
    {
        return $this->hasOne(Terminal::className(), ['id' => 'terminal_id']);
    }

    /**
     * @inheritdoc
     * @return RawQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RawQuery(get_called_class());
    }
}
