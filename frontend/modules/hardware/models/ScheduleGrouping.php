<?php

namespace app\modules\hardware\models;

use Yii;

/**
 * This is the model class for table "schedule_grouping".
 *
 * @property integer $id
 * @property string $group_name
 * @property integer $schedule_id_in
 * @property integer $schedule_id_out
 *
 * @property Schedule $scheduleIdIn
 * @property Schedule $scheduleIdOut
 */
class ScheduleGrouping extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'schedule_grouping';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['schedule_id_in', 'schedule_id_out'], 'integer'],
            [['group_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'group_name' => Yii::t('app', 'Group Name'),
            'schedule_id_in' => Yii::t('app', 'Schedule Id In'),
            'schedule_id_out' => Yii::t('app', 'Schedule Id Out'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScheduleIdIn()
    {
        return $this->hasOne(Schedule::className(), ['id' => 'schedule_id_in']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScheduleIdOut()
    {
        return $this->hasOne(Schedule::className(), ['id' => 'schedule_id_out']);
    }

    /**
     * @inheritdoc
     * @return ScheduleGroupingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ScheduleGroupingQuery(get_called_class());
    }
}
