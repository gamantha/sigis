<?php

namespace app\modules\hardware\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\hardware\models\Raw;

/**
 * RawSearch represents the model behind the search form about `app\modules\hardware\models\Raw`.
 */
class RawSearch extends Raw
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'terminal_id'], 'integer'],
            [['pin', 'datetime', 'verified', 'status', 'workcode', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Raw::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'terminal_id' => $this->terminal_id,
            'datetime' => $this->datetime,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'pin', $this->pin])
            ->andFilterWhere(['like', 'verified', $this->verified])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'workcode', $this->workcode]);

        return $dataProvider;
    }
}
