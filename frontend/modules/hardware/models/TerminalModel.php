<?php

namespace app\modules\hardware\models;

use Yii;

/**
 * This is the model class for table "terminal_model".
 *
 * @property integer $id
 * @property string $model_name
 * @property string $make
 * @property integer $year
 * @property string $default_configuration
 *
 * @property Terminal[] $terminals
 */
class TerminalModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'terminal_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year'], 'integer'],
            [['default_configuration'], 'string'],
            [['model_name', 'make'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'model_name' => Yii::t('app', 'Model Name'),
            'make' => Yii::t('app', 'Make'),
            'year' => Yii::t('app', 'Year'),
            'default_configuration' => Yii::t('app', 'Default Configuration'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerminals()
    {
        return $this->hasMany(Terminal::className(), ['model_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return TerminalModelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TerminalModelQuery(get_called_class());
    }
}
