<?php

namespace app\modules\sig\models;

use Yii;

/**
 * This is the model class for table "sig_bpjs".
 *
 * @property integer $id
 * @property string $tipe
 * @property integer $value
 */
class SigBpjs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
    public function bpjs($id,$project_id,$from,$to){
        $bpjs='';
        return $bpjs;
    }
    
    public function test(){
        return 'test';
    }
    
    public static function tableName()
    {
        return 'sig_bpjs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipe'], 'string'],
            [['value'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipe' => 'Tipe',
            'value' => 'Value',
        ];
    }

    /**
     * @inheritdoc
     * @return SigBpjsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SigBpjsQuery(get_called_class());
    }
}
