<?php

namespace app\modules\sig\models;

use Yii;

/**
 * This is the model class for table "quota_ijin".
 *
 * @property integer $id
 * @property integer $golongan_id
 * @property integer $tipeijin_id
 * @property integer $quota_bulan
 * @property integer $quota_tahun
 *
 * @property SigGolongan $golongan
 * @property RefTipeijin $tipeijin
 */
class QuotaIjin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quota_ijin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['golongan_id', 'tipeijin_id', 'quota_bulan', 'quota_tahun'], 'required'],
            [['golongan_id', 'tipeijin_id', 'quota_bulan', 'quota_tahun'], 'integer'],
            [['golongan_id'], 'exist', 'skipOnError' => true, 'targetClass' => SigGolongan::className(), 'targetAttribute' => ['golongan_id' => 'id']],
            [['tipeijin_id'], 'exist', 'skipOnError' => true, 'targetClass' => RefTipeijin::className(), 'targetAttribute' => ['tipeijin_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'golongan_id' => 'Golongan ID',
            'tipeijin_id' => 'Tipeijin ID',
            'quota_bulan' => 'Quota Bulan',
            'quota_tahun' => 'Quota Tahun',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGolongan()
    {
        return $this->hasOne(SigGolongan::className(), ['id' => 'golongan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipeijin()
    {
        return $this->hasOne(RefTipeijin::className(), ['id' => 'tipeijin_id']);
    }

    /**
     * @inheritdoc
     * @return QuotaIjinQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new QuotaIjinQuery(get_called_class());
    }
}
