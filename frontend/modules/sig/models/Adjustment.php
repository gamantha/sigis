<?php

namespace app\modules\sig\models;

use Yii;

/**
 * This is the model class for table "adjustment".
 *
 * @property string $id
 * @property string $start_period
 * @property string $finish_period
 * @property string $tipe
 * @property integer $value_1
 * @property string $value_2
 * @property string $value_3
 * @property integer $employee_id
 */
class Adjustment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adjustment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_period', 'finish_period', 'tipe', 'value_1', 'value_2', 'value_3', 'employee_id'], 'required'],
            [['start_period', 'finish_period'], 'safe'],
            [['tipe'], 'string'],
            [['value_1', 'value_2', 'employee_id'], 'integer'],
            [['value_3'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'start_period' => Yii::t('app', 'Start Period'),
            'finish_period' => Yii::t('app', 'Finish Period'),
            'tipe' => Yii::t('app', 'Tipe'),
            'value_1' => Yii::t('app', 'Value 1'),
            'value_2' => Yii::t('app', 'Value 2'),
            'value_3' => Yii::t('app', 'Value 3'),
            'employee_id' => Yii::t('app', 'Employee ID'),
        ];
    }

    /**
     * @inheritdoc
     * @return AdjustmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AdjustmentQuery(get_called_class());
    }
}
