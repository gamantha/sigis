<?php

namespace app\modules\sig\models;

/**
 * This is the ActiveQuery class for [[Golongan]].
 *
 * @see Golongan
 */
class GolonganQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Golongan[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Golongan|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}