<?php

namespace app\modules\sig\models;

/**
 * This is the ActiveQuery class for [[SigPengembalianKasbon]].
 *
 * @see SigPengembalianKasbon
 */
class SigPengembalianKasbonQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SigPengembalianKasbon[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SigPengembalianKasbon|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}