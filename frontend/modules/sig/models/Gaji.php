<?php

namespace app\modules\sig\models;

use Yii;

/**
 * This is the model class for table "sig_gaji".
 *
 * @property integer $id
 * @property integer $employee_id
 * @property integer $sig_project_id
 * @property integer $gaji_pokok
 * @property integer $tunjangan_jabatan
 * @property integer $pendapatan_intern
 * @property string $start_period
 * @property string $end_period
 *
 * @property Employee $employee
 * @property SigProject $sigProject
 */
class Gaji extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sig_gaji';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id', 'sig_project_id', 'gaji_pokok', 'tunjangan_jabatan', 'pendapatan_intern'], 'integer'],
            [['start_period', 'end_period'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'employee_id' => Yii::t('app', 'Employee ID'),
            'sig_project_id' => Yii::t('app', 'Sig Project ID'),
            'gaji_pokok' => Yii::t('app', 'Gaji Pokok'),
            'tunjangan_jabatan' => Yii::t('app', 'Tunjangan Jabatan'),
            'pendapatan_intern' => Yii::t('app', 'Pendapatan Intern'),
            'start_period' => Yii::t('app', 'Start Period'),
            'end_period' => Yii::t('app', 'End Period'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSigProject()
    {
        return $this->hasOne(SigProject::className(), ['id' => 'sig_project_id']);
    }

    /**
     * @inheritdoc
     * @return GajiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GajiQuery(get_called_class());
    }
}
