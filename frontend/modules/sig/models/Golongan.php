<?php

namespace app\modules\sig\models;
use app\modules\organization\models\Employee;

use Yii;

/**
 * This is the model class for table "sig_golongan".
 *
 * @property integer $id
 * @property string $golongan
 * @property integer $department_role_id
 * @property string $masa_kerja_minimal
 * @property integer $tunjangan_makan_transport
 * @property integer $tunjangan_premi_hadir
 * @property integer $tunjangan_bonus_hadir
 * @property integer $tunjangan_lembur
 * @property integer $tunjangan_lembur_luarkota
 * @property integer $denda_keterlambatan
 * @property integer $plafon_kasbon
 * @property integer $plafon_kesehatan
 *
 * @property DepartmentRole $departmentRole
 */
class Golongan extends \yii\db\ActiveRecord
{

 public $day;
 public $month;
 public $year;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sig_golongan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['department_role_id', 'tunjangan_makan_transport', 'tunjangan_premi_hadir', 'tunjangan_bonus_hadir', 'tunjangan_lembur', 'tunjangan_lembur_luarkota', 'denda_keterlambatan', 'plafon_kasbon', 'plafon_kesehatan','day','month','year'], 'integer'],
       [['golongan', 'masa_kerja_minimal'], 'string', 'max' => 255],
            //['golongan', 'unique', 'message'=>'nama golongan tidak unik'],
                        [['masa_kerja_minimal','department_role_id'], 'unique', 'targetAttribute' => ['masa_kerja_minimal', 'department_role_id'], 'message'=>'sudah ada golongan dengan jabatan/masa kerja ini'],
                             //                 [['day','month','year'], 'unique', 'targetAttribute' => ['day','month','year'], 'message'=>'sudah ada golongan dengan jabatan/masa kerja ini'],
                        ['department_role_id', 'required', 'message' => 'jabatan kosong']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'golongan' => Yii::t('app', 'Golongan'),
            'department_role_id' => Yii::t('app', 'Department Role ID'),
            'masa_kerja_minimal' => Yii::t('app', 'Masa Kerja Minimal'),
            'tunjangan_makan_transport' => Yii::t('app', 'Tunjangan Makan Transport'),
            'tunjangan_premi_hadir' => Yii::t('app', 'Tunjangan Premi Hadir'),
            'tunjangan_bonus_hadir' => Yii::t('app', 'Tunjangan Bonus Hadir'),
            'tunjangan_lembur' => Yii::t('app', 'Tunjangan Lembur'),
            'tunjangan_lembur_luarkota' => Yii::t('app', 'Tunjangan Lembur Luarkota'),
            'denda_keterlambatan' => Yii::t('app', 'Denda Keterlambatan'),
            'plafon_kasbon' => Yii::t('app', 'Plafon Kasbon'),
            'plafon_kesehatan' => Yii::t('app', 'Plafon Kesehatan'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentRole()
    {
        return $this->hasOne(\app\modules\organization\models\DepartmentRole::className(), ['id' => 'department_role_id']);
    }

    /**
     * @inheritdoc
     * @return GolonganQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GolonganQuery(get_called_class());
    }

    public function beforeValidate()
    {
         if (parent::beforeValidate()) {
         // ...custom code here...
        /* if (sizeof(explode(":",$this->masa_kerja_minimal)) > 2) {
         //do nothing
         } else {
         $this->masa_kerja_minimal = $this->day . ':' . $this->month. ':' . $this->year;
        }
        */
         return true;
         } else {
         return false;
         }
    }

    public function afterValidate()
    {
         if (parent::afterValidate()) {
         // ...custom code here...
        // $this->masa_kerja_minimal = null;
         return true;
         } else {
         return false;
         }
    }

    public function beforeSave($insert)
    {
         if (parent::beforeSave($insert)) {
         // ...custom code here...
       //  $this->masa_kerja_minimal = $this->day . ':' . $this->month. ':' . $this->year;
         return true;
         } else {
         return false;
         }
    }

    public function test()
    {

     return 'test';
    }
    public function golonganEmployee($id)
    {
      //$golongan = new
      echo '<br/><br/><br/><br/>';
      echo 'GOLONGAN';
      $all = parent::find()
      //->andWhere([])
      ->One();
      $employee = Employee::findOne($id);
      //echo sizeof($all);
      echo $employee->employment->first_day;
     return ;
    }


}
