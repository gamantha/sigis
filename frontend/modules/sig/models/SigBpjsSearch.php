<?php

namespace app\modules\sig\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\sig\models\SigBpjs;

/**
 * SigBpjsSearch represents the model behind the search form about `app\modules\sig\models\SigBpjs`.
 */
class SigBpjsSearch extends SigBpjs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'value'], 'integer'],
            [['tipe'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SigBpjs::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'value' => $this->value,
        ]);

        $query->andFilterWhere(['like', 'tipe', $this->tipe]);

        return $dataProvider;
    }
}
