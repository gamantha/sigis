<?php

namespace app\modules\sig\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\sig\models\Golongan;

/**
 * GolonganSearch represents the model behind the search form about `app\modules\sig\models\Golongan`.
 */
class GolonganSearch extends Golongan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'department_role_id', 'tunjangan_makan_transport', 'tunjangan_premi_hadir', 'tunjangan_bonus_hadir', 'tunjangan_lembur', 'tunjangan_lembur_luarkota', 'denda_keterlambatan', 'plafon_kasbon', 'plafon_kesehatan'], 'integer'],
            [['golongan', 'masa_kerja_minimal'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Golongan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
         //   $this->masa_kerja_minimal = null;
            return $dataProvider;
        }
     $this->masa_kerja_minimal = null;
        $query->andFilterWhere([
            'id' => $this->id,
            'department_role_id' => $this->department_role_id,
            'masa_kerja_minimal' => $this->masa_kerja_minimal,
            'tunjangan_makan_transport' => $this->tunjangan_makan_transport,
            'tunjangan_premi_hadir' => $this->tunjangan_premi_hadir,
            'tunjangan_bonus_hadir' => $this->tunjangan_bonus_hadir,
            'tunjangan_lembur' => $this->tunjangan_lembur,
            'tunjangan_lembur_luarkota' => $this->tunjangan_lembur_luarkota,
            'denda_keterlambatan' => $this->denda_keterlambatan,
            'plafon_kasbon' => $this->plafon_kasbon,
            'plafon_kesehatan' => $this->plafon_kesehatan,
        ]);

        $query->andFilterWhere(['like', 'golongan', $this->golongan]);

        return $dataProvider;
    }
}
