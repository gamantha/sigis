<?php

namespace app\modules\sig\models;

use Yii;

/**
 * This is the model class for table "sig_project".
 *
 * @property integer $id
 * @property string $name
 * @property string $shortname
 * @property string $status
 *
 * @property SigGaji[] $sigGajis
 */
class SigProject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sig_project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'string'],
            [['name', 'shortname'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'shortname' => Yii::t('app', 'Shortname'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSigGajis()
    {
        return $this->hasMany(SigGaji::className(), ['sig_project_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return SigProjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SigProjectQuery(get_called_class());
    }
}
