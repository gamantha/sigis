<?php

namespace app\modules\sig\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\sig\models\Siggaji;

/**
 * SiggajiSearch represents the model behind the search form about `app\modules\sig\models\Siggaji`.
 */
class SiggajiSearch extends Siggaji
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'employee_id', 'sig_project_id', 'gaji_pokok', 'tunjangan_jabatan', 'pendapatan_intern'], 'integer'],
            [['start_period', 'end_period'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Siggaji::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'employee_id' => $this->employee_id,
            'sig_project_id' => $this->sig_project_id,
            'gaji_pokok' => $this->gaji_pokok,
            'tunjangan_jabatan' => $this->tunjangan_jabatan,
            'pendapatan_intern' => $this->pendapatan_intern,
            'start_period' => $this->start_period,
            'end_period' => $this->end_period,
        ]);

        return $dataProvider;
    }
}
