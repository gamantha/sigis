<?php

namespace app\modules\sig\models;
use app\modules\organization\models\Employee;

use Yii;

/**
 * This is the model class for table "sig_gaji".
 *
 * @property integer $id
 * @property integer $employee_id
 * @property integer $sig_project_id
 * @property integer $gaji_pokok
 * @property integer $tunjangan_jabatan
 * @property integer $pendapatan_intern
 * @property string $start_period
 * @property string $end_period
 *
 * @property Employee $employee
 * @property SigProject $sigProject
 */
class Siggaji extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sig_gaji';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            		           [['employee_id', 'sig_project_id', 'gaji_pokok', 'tunjangan_jabatan', 'pendapatan_intern', 'bca', 'btn'], 'integer'],
            //[['start_period', 'end_period'], 'safe']
            [['start_period','end_period'], 'required', 'message' => 'period kosong']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Employee ID',
            'sig_project_id' => 'Sig Project ID',
            'gaji_pokok' => 'Gaji Pokok',
            'tunjangan_jabatan' => 'Tunjangan Jabatan',
            'pendapatan_intern' => 'Pendapatan Intern',
            'bca' => Yii::t('app', 'Bca'),
'btn' => Yii::t('app', 'Btn'),
            'start_period' => 'Start Period',
            'end_period' => 'End Period',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSigProject()
    {
        return $this->hasOne(SigProject::className(), ['id' => 'sig_project_id']);
    }

    /**
     * @inheritdoc
     * @return SiggajiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SiggajiQuery(get_called_class());
    }
}
