<?php

namespace app\modules\sig\models;
use app\modules\organization\models\Employee;

use Yii;

/**
 * This is the model class for table "ijin".
 *
 * @property integer $id
 * @property integer $employee_id
 * @property integer $tipeijin_id
 * @property string $start
 * @property string $finish
 * @property string $status
 * @property integer $manager_id
 * @property string $created
 * @property string $modified
 *
 * @property Employee $employee
 */
class Ijin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ijin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id', 'tipeijin_id', 'start', 'finish', 'status', 'manager_id', 'created', 'modified'], 'required'],
            [['employee_id', 'tipeijin_id', 'manager_id'], 'integer'],
            [['start', 'finish', 'created', 'modified'], 'safe'],
            [['status'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Employee ID',
            'tipeijin_id' => 'Tipeijin ID',
            'start' => 'Start',
            'finish' => 'Finish',
            'status' => 'Status',
            'manager_id' => 'Manager ID',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @inheritdoc
     * @return IjinQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IjinQuery(get_called_class());
    }
}
