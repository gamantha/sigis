<?php

namespace app\modules\sig\models;

use Yii;

/**
 * This is the model class for table "sig_pph".
 *
 * @property integer $id
 * @property string $tipe
 * @property integer $value_1
 * @property integer $value_2
 */
class Sigpph extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sig_pph';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipe'], 'string'],
            [['operator'], 'string'],
            [['value_1', 'value_2'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipe' => 'Tipe',
            'value_1' => 'Value 1',
            'value_2' => 'Value 2',
            'operator' => 'Operator',
            'sig_project_id' => 'Sig Project ID',
        ];
    }

    /**
     * @inheritdoc
     * @return SigPphQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SigPphQuery(get_called_class());
    }
}
