<?php

namespace app\modules\sig\models;

use Yii;

/**
 * This is the model class for table "sig_pengembalian_kasbon".
 *
 * @property integer $id
 * @property integer $kasbon_id
 * @property integer $pembayaran_ke
 * @property string $jatuh_tempo
 * @property integer $besaran
 * @property string $status
 *
 * @property SigKasbon $kasbon
 */
class SigPengembalianKasbon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sig_pengembalian_kasbon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kasbon_id', 'pembayaran_ke', 'besaran'], 'integer'],
            [['jatuh_tempo'], 'safe'],
            [['status'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kasbon_id' => Yii::t('app', 'Kasbon ID'),
            'pembayaran_ke' => Yii::t('app', 'Pembayaran Ke'),
            'jatuh_tempo' => Yii::t('app', 'Jatuh Tempo'),
            'besaran' => Yii::t('app', 'Besaran'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKasbon()
    {
        return $this->hasOne(SigKasbon::className(), ['id' => 'kasbon_id']);
    }

    /**
     * @inheritdoc
     * @return SigPengembalianKasbonQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SigPengembalianKasbonQuery(get_called_class());
    }
}
