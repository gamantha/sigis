<?php

namespace app\modules\sig\models;

/**
 * This is the ActiveQuery class for [[Ijin]].
 *
 * @see Ijin
 */
class IjinQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Ijin[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Ijin|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
