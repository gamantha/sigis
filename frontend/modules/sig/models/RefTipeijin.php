<?php

namespace app\modules\sig\models;

use Yii;

/**
 * This is the model class for table "ref_tipeijin".
 *
 * @property integer $id
 * @property string $tipeijin
 * @property string $time_limit
 * @property string $sesudah
 *
 * @property QuotaIjin[] $quotaIjins
 */
class RefTipeijin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_tipeijin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipeijin'], 'string'],
            [['time_limit', 'sesudah'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipeijin' => 'Tipeijin',
            'time_limit' => 'Time Limit',
            'sesudah' => 'Sesudah',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuotaIjins()
    {
        return $this->hasMany(QuotaIjin::className(), ['tipeijin_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return RefTipeijinQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RefTipeijinQuery(get_called_class());
    }
}
