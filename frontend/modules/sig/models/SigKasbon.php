<?php

namespace app\modules\sig\models;

use Yii;
use app\modules\organization\models\Employee;

/**
 * This is the model class for table "sig_kasbon".
 *
 * @property integer $id
 * @property integer $employee_id
 * @property integer $sig_project_id
 * @property integer $pengajuankasbon
 * @property integer $potonganperbulan
 * @property string $keterangan
 * @property string $status
 *
 * @property SigProject $sigProject
 * @property Employee $employee
 * @property SigPengembalianKasbon[] $sigPengembalianKasbons
 */
class SigKasbon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sig_kasbon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id', 'sig_project_id', 'pengajuankasbon', 'potonganperbulan'], 'integer'],
            [['keterangan'], 'string'],
                  ['employee_id', 'required', 'message' => 'NIK Kosong'],
                       [['pengajuankasbon','potonganperbulan'], 'required'],
            [['status'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'employee_id' => Yii::t('app', 'Employee ID'),
            'sig_project_id' => Yii::t('app', 'Sig Project ID'),
            'pengajuankasbon' => Yii::t('app', 'Pengajuankasbon'),
            'potonganperbulan' => Yii::t('app', 'Potonganperbulan'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSigProject()
    {
        return $this->hasOne(SigProject::className(), ['id' => 'sig_project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSigPengembalianKasbons()
    {
        return $this->hasMany(SigPengembalianKasbon::className(), ['kasbon_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return SigKasbonQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SigKasbonQuery(get_called_class());
    }
}
