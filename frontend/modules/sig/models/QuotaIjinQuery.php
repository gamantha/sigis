<?php

namespace app\modules\sig\models;

/**
 * This is the ActiveQuery class for [[QuotaIjin]].
 *
 * @see QuotaIjin
 */
class QuotaIjinQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return QuotaIjin[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return QuotaIjin|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
