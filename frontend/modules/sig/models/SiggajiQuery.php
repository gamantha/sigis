<?php

namespace app\modules\sig\models;

/**
 * This is the ActiveQuery class for [[Siggaji]].
 *
 * @see Siggaji
 */
class SiggajiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Siggaji[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Siggaji|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}