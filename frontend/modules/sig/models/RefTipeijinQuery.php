<?php

namespace app\modules\sig\models;

/**
 * This is the ActiveQuery class for [[RefTipeijin]].
 *
 * @see RefTipeijin
 */
class RefTipeijinQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return RefTipeijin[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RefTipeijin|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
