<?php

namespace app\modules\sig\models;

/**
 * This is the ActiveQuery class for [[SigProject]].
 *
 * @see SigProject
 */
class SigProjectQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SigProject[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SigProject|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}