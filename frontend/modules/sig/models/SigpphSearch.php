<?php

namespace app\modules\sig\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\sig\models\Sigpph;

/**
 * SigpphSearch represents the model behind the search form about `app\modules\sig\models\Sigpph`.
 */
class SigpphSearch extends Sigpph
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'value_1', 'value_2'], 'integer'],
            [['tipe'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sigpph::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'value_1' => $this->value_1,
            'value_2' => $this->value_2,
        ]);

        $query->andFilterWhere(['like', 'tipe', $this->tipe]);

        return $dataProvider;
    }
}
