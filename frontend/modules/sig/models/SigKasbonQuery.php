<?php

namespace app\modules\sig\models;

/**
 * This is the ActiveQuery class for [[SigKasbon]].
 *
 * @see SigKasbon
 */
class SigKasbonQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SigKasbon[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SigKasbon|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}