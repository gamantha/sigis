<?php

namespace app\modules\sig\models;

/**
 * This is the ActiveQuery class for [[SigBpjs]].
 *
 * @see SigBpjs
 */
class SigBpjsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SigBpjs[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SigBpjs|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}