<?php

namespace app\modules\sig\controllers;

use yii\web\Controller;


class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionPersonalia()
    {
        return $this->render('personalia');

    }
    public function actionAbsensi()
    {
        return $this->render('absensi');

    }
public function actionReportabsensi()
{

 return $this->render('reportabsensi');
}
    public function actionKetentuanpersonalia()
    {
        return $this->render('ketentuanpersonalia');

    }
    public function actionDatakaryawan()
    {
        return $this->render('datakaryawan');

    }
    public function actionGajidanupah()
    {
        return $this->render('gajidanupah');

    }
    public function actionPerhitungan()
    {
        return $this->render('perhitungan');

    }
    public function actionGajibulanan()
    {
        return $this->render('gajibulanan');

    }
    public function actionPerhitungangajibulanan()
    {
        return $this->render('perhitungangajibulanan');

    }
    public function actionPerhitunganabsensigajibulanan()
    {
        return $this->render('perhitunganabsensigajibulanan');

    }
    public function actionPph()
    {
        return $this->render('pph');

    }

}
