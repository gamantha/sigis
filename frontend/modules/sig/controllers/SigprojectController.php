<?php

namespace app\modules\sig\controllers;

use Yii;
use app\modules\sig\models\Sigproject;
use app\modules\sig\models\SigprojectSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SigprojectController implements the CRUD actions for Sigproject model.
 */
class SigprojectController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sigproject models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SigprojectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sigproject model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sigproject model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sigproject();
        $model->status = 'active';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
                  Yii::$app->getSession()->setFlash('success', 'proyek baru ditambahkan');
                 return $this->redirect(['siggaji/create']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Sigproject model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Sigproject model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
     $projects = SigProject::find()->andWhere(['status'=>'active'])->All();
     if (sizeof($projects) == 1) {
      Yii::$app->getSession()->setFlash('danger', 'daftar proyek tidak boleh kosong');
     } else {
     $model = $this->findModel($id);
     $model->status = 'inactive';
     $model->save();
     Yii::$app->getSession()->setFlash('success', 'proyek di hapus');
    }
        //return $this->redirect(['siggaji/create']);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Sigproject model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sigproject the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sigproject::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
