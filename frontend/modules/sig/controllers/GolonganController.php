<?php

namespace app\modules\sig\controllers;

use Yii;
use app\modules\sig\models\Golongan;
use app\modules\sig\models\GolonganSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\organization\models\Department;
use app\modules\organization\models\DepartmentRole;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * GolonganController implements the CRUD actions for Golongan model.
 */
class GolonganController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Golongan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GolonganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
                 'defaultOrder' => ['golongan' => 'SORT_DESC'],
                 'attributes' => ['golongan'],
        ];
        //$searchModel->masa_kerja_minimal = null;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Golongan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Golongan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $lookup = Array("0" => "a","1" => "b","2" => "c","3" => "d","4" => "e","5" => "f","6" => "g","7" => "h","8" => "i","9" =>   "j","10" => "k","11" => "l","12" => "m","13" => "n","14" => "o","15" => "p","16" => "q","17" => "r","18" => "s","19" => "t","20" => "u","21" => "v","22" => "w","23" => "x","24" => "y","25" => "z");

        $model = new Golongan();
        $departmentmodel = new Department();
            //if (\Yii::$app->user->can('createGolongan')) {
        if(true){
             if ($model->load(Yii::$app->request->post())) {
                 $model->golongan=$_REQUEST['golongan'];

                        foreach($lookup as $key=>$value){
                            if ($_REQUEST['golongan']==$value){
                                $rt=$key;
                            }
                        }
                 $find=Golongan::find()->andWhere(['golongan'=>$_REQUEST['golongan']])->One();
                if (isset ($find)){

                    $cari=Golongan::find()->All();
                    foreach ($cari as $look){
                        echo $look->golongan;

                        foreach($lookup as $key=>$value){
                            if($look->golongan==$value){
                                if($rt>$key){
                                    echo ' tidak dirubah';
                                }
                                else
                                {
                                    $geser=Golongan::find()->andWhere(['golongan'=>$value])->orderBy(['id' => SORT_DESC])->One();                                           //update geser dibawah akan merubah hasil query disini
                                    $n='';
                                    foreach($lookup as $key=>$value){
                                        if ($geser->golongan==$value){
                                            $n=$lookup[$key+1];
                                        }
                                    }
                                    echo ' > ';
                                    $geser->golongan=$n;
                                    echo $geser->golongan;
                                    echo '<br/>';
                                    echo '<pre>';
                                    //print_r($geser);
                                    echo $geser->masa_kerja_minimal;
                                    echo '</pre>';
                                    if ($geser->validate())
                                    {
                                    $geser->save();
                                   }

                                }
                            }
                        }

                        echo '</br>';
                    }


$intervalstring = 'P'.$model->year.'Y'.$model->month.'M'.$model->day.'D';
$model->masa_kerja_minimal = $intervalstring;
                   $model->save();
                    Yii::$app->getSession()->setFlash('success', 'Created a new golongan');
                     return $this->redirect('../default/ketentuanpersonalia');
                }
                else {
                 $intervalstring = 'P'.$model->year.'Y'.$model->month.'M'.$model->day.'D';
                 $model->masa_kerja_minimal = $intervalstring;
                    $model->save();
                    Yii::$app->getSession()->setFlash('success', 'Created a new golongan');
                     return $this->redirect('../default/ketentuanpersonalia');
                }
               /* echo '<pre>YEAAAA<br/>';

                $interval = new \DateInterval($intervalstring);
                //print_r($model);
                echo $intervalstring;
                echo '</pre>';
*/
             } else {
                 return $this->render('create', [
                     'model' => $model,
                     //'rolemodel' => $rolemodel,
                     'departmentmodel' => $departmentmodel,
                 ]);
            }
       } else {
       //echo 'ga bisa';
        Yii::$app->getSession()->setFlash('error', 'You don\'t have permission');

        //return $this->redirect(\Yii::$app->request->getReferrer());

         return $this->redirect('../default/ketentuanpersonalia');
       }
    }

    /**
     * Updates an existing Golongan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $rolemodel = DepartmentRole::find()->where(['id' => $model->department_role_id])->one();
        $departmentmodel = Department::find()->where(['id' => $rolemodel->department_id])->one();
        //$departmentmodel = new Department();
        //$rolemodel = new DepartmentRole();

                    //if (\Yii::$app->user->can('createGolongan')) {
                    if(true) {

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
              return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'rolemodel' => $rolemodel,
                'departmentmodel' => $departmentmodel,
            ]);
        }
       } else {

        Yii::$app->getSession()->setFlash('error', 'You don\'t have permission');

        //return $this->redirect(\Yii::$app->request->getReferrer());

         return $this->redirect('index');
       }
    }

    /**
     * Deletes an existing Golongan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */

    public function actionDelete($id)
    {
        $lookup = Array("0" => "a","1" => "b","2" => "c","3" => "d","4" => "e","5" => "f","6" => "g","7" => "h","8" => "i","9" =>   "j","10" => "k","11" => "l","12" => "m","13" => "n","14" => "o","15" => "p","16" => "q","17" => "r","18" => "s","19" => "t","20" => "u","21" => "v","22" => "w","23" => "x","24" => "y","25" => "z");

        //$lookup = Array("a" => "0","b" => "1","c" => "2","d" => "3","e" => "4","f" => "5","g" => "6","h" => "7","i" => "8","j" =>   "9","k" => "10","l" => "11","m" => "12","n" => "13","o" => "14","p" => "15","q" => "16","r" => "17","s" => "18","t" => "19","u" => "20","v" => "21","w" => "22","x" => "23","y" => "24","z" => "25");

        $coba = $this->findModel($id);
        $bisa = Golongan::find()->andWhere(['golongan'=>$coba->golongan])->One();
        $hasilcoba = $bisa->golongan;

        $model = new Golongan();
        $departmentmodel = new Department();
            //if (\Yii::$app->user->can('createGolongan')) {
        if(true){

                 $model->golongan=$hasilcoba;

                        foreach($lookup as $key=>$value){
                            if ($hasilcoba==$value){
                                $rt=$key;
                            }
                        }
                 $find=Golongan::find()->andWhere(['golongan'=>$hasilcoba])->One();
                if (isset ($find)){

                    $cari=Golongan::find()->All();
                    foreach ($cari as $look){
                        echo $look->golongan;

                        foreach($lookup as $key=>$value){
                            if($look->golongan==$value){
                                if($rt>$key){
                                    echo ' tidak dirubah';
                                }
                                else
                                {
                                    $geser=Golongan::find()->andWhere(['golongan'=>$value])->orderBy(['id' => SORT_ASC])->One();                                           //update geser dibawah akan merubah hasil query disini
                                    $n='';
                                    foreach($lookup as $key=>$value){
                                        if ($geser->golongan==$value){
                                            $n=$lookup[$key-1];
                                        }
                                    }
                                    echo ' > ';
                                    $geser->golongan=$n;
                                    echo $geser->golongan;
                                    echo '<br/>';
                                    echo '<pre>';
                                    //print_r($geser);
                                    echo $geser->masa_kerja_minimal;
                                    echo '</pre>';
                                    if ($geser->validate())
                                    $geser->save();

                                }
                            }
                        }

                        echo '</br>';
                    }

                   // echo '<pre>';
                   // print_r($model);
                    //echo '</pre>';
                   $this->findModel($id)->delete();
                    Yii::$app->getSession()->setFlash('success', 'Created a new golongan');
                     return $this->redirect('../default/ketentuanpersonalia');
                }
                else {
                    $this->findModel($id)->delete();
                    Yii::$app->getSession()->setFlash('success', 'Created a new golongan');
                     return $this->redirect('../default/ketentuanpersonalia');
                }



       } else {
       //echo 'ga bisa';
        Yii::$app->getSession()->setFlash('error', 'You don\'t have permission');

        //return $this->redirect(\Yii::$app->request->getReferrer());

         return $this->redirect('../default/ketentuanpersonalia');
       }

    }

    /**
     * Finds the Golongan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Golongan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Golongan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public static function getDeparments()
    {
     $data=  Department::find()->all();
    $value=(count($data)==0)? [''=>'']: \yii\helpers\ArrayHelper::map($data, 'id','department_name');

     return $value;
    }

    public static function getRolesbyDepartments($dept_id)
    {
     $data = DepartmentRole::find()->where(['department_id'=>$dept_id])->select(['id','department_role_name AS name'])->asArray()->all();
     $value = (count($data) == 0) ? ['' => ''] : $data;

     return $value;

    }


    public static function actionRoles()
    {

     $out = [];
if (isset($_POST['depdrop_parents'])) {
    $parents = $_POST['depdrop_parents'];
    if ($parents != null) {
        $cat_id = $parents[0];
        $out = self::getRolesbyDepartments($cat_id);
        echo Json::encode(['output' => $out, 'selected' => '']);
        return;
    }
}
echo Json::encode(['output' => '', 'selected' => '']);


    }
}
