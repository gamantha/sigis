<?php

namespace app\modules\sig\controllers;

use Yii;
use app\modules\sig\models\SigBpjs;
use app\modules\sig\models\Siggaji;
use app\modules\organization\models\EmployeeBank;
use app\modules\sig\models\SigBpjsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

/**
 * SigbpjsController implements the CRUD actions for SigBpjs model.
 */
class SigpayrollController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SigBpjs models.
     * @return mixed
     */
    public function actionIndex()
    {
     echo '<br/><br/>sadadsa';
        $searchModel = new SigBpjsSearch();
        $employeebank = new EmployeeBank();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
if(isset($_POST['search'])){

$employeebank->load(Yii::$app->request->post());

 $query = Siggaji::find()
 ->andWhere(['in', 'start_period', $_REQUEST['start_period']])
 ->andWhere(['in', 'end_period', $_REQUEST['finish_period']])
 ->andWhere(['in', 'sig_project_id', $_GET['id']]);

 $dataProvider = new ActiveDataProvider([
     //'allModels' => $gajiarray,
     'query' => $query,
     'pagination' => [
         'pageSize' => 70,
     ],
     'sort' => [
         'defaultOrder' => [
         ]
     ],
 ]);



}

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'employeebank' => $employeebank,
        ]);
    }

    /**
     * Displays a single SigBpjs model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SigBpjs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SigPayroll();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SigBpjs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SigBpjs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SigBpjs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SigBpjs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SigPayroll::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
