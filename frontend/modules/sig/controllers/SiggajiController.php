<?php

namespace app\modules\sig\controllers;

use Yii;
use app\modules\sig\models\Siggaji;
use yii\helpers\ArrayHelper;
use app\modules\organization\models\Employee;
use app\modules\sig\models\SigProject;
use app\modules\sig\models\SigKasbon;
use app\modules\hardware\models\Schedule;
use app\modules\sig\models\Golongan;
use app\modules\sig\models\SigPengembalianKasbon;
use app\modules\sig\models\SiggajiSearch;
use app\modules\organization\models\EmployeeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\grid\GridView;
use yii\widgets\ListView;
use yii\base\Model;
use kartik\grid\EditableColumnAction;
use yii\data\ActiveDataProvider;
use kartik\editable\EditableAsset;
use kartik\editable\EditablePjaxAsset;
use kartik\popover\PopoverXAsset;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;

use kartik\mpdf\Pdf;





/**
 * SiggajiController implements the CRUD actions for Siggaji model.
 */
class SiggajiController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }



 public function actionKasbon($id){

if(isset($_POST['SigKasbon']['employee_id']))
 {
$emp_id = $_POST['SigKasbon']['employee_id'];
$emp_model = Employee::find()->andWhere(['id' => $emp_id])->One();
$kasbonview = 'kasbon';
$model = SigKasbon::find()->andWhere(['employee_id' => $emp_id, 'sig_project_id' => $id, 'status' => 'active'])->One();
        if (isset($model))
        {
          //SUDAH ADA KASBON => BISA SUDAH ADA PENGEMBALIAN ATAU BELUM ADA PENGEMBALIAN
         $pengembalians = SigPengembalianKasbon::find()->andWhere(['kasbon_id' => $model->id])->all();
              if (sizeof($pengembalians) > 0) {
                   } else {
                       $count = count(Yii::$app->request->post('SigPengembalianKasbon', []));
                      $pengembalians = [new SigPengembalianKasbon()];
                      for($i = 1; $i < $count; $i++) {
                          $pengembalians[] = new SigPengembalianKasbon();
                      }
                   }

$kasbonview = 'kasbon2';


        } else {
         // BELUM ADA KASBON => BELUM ADA PENGEMBALIAN
          $model = new SigKasbon();
          $model->employee_id = $emp_id;
         // $count = count(Yii::$app->request->post('SigPengembalianKasbon', []));
          $count = 0;
         $pengembalians = [new SigPengembalianKasbon()];
         for($i = 1; $i < $count; $i++) {
             $pengembalians[] = new SigPengembalianKasbon();
         }


        }
        if (isset($_POST['save'])) {
           $model->load(Yii::$app->request->post());
           $model->sig_project_id = $id;
           $model->status = 'active';
           $today = new \DateTime();
           $howmanymonth = ceil($model->pengajuankasbon / $model->potonganperbulan);
           $model->save();
           Yii::$app->getSession()->setFlash('success', 'Kasbon di save');
           $pengembalians = SigPengembalianKasbon::find()->andWhere(['kasbon_id' => $model->id])->all();
           if(sizeof($pengembalians) < 1) {
              $count = $howmanymonth;
              $sisa = $model->pengajuankasbon;
             $pengembalians = [new SigPengembalianKasbon()];
             for($i = 0; $i < $count; $i++) {
                 $pengembalians[] = new SigPengembalianKasbon();
                 $pengembalians[$i]->besaran = ($sisa > $model->potonganperbulan) ? $model->potonganperbulan: $sisa;
                 $sisa = $sisa - $model->potonganperbulan;
                  $pengembalians[$i]->kasbon_id = $model->id;
                 $pengembalians[$i]->pembayaran_ke = $i;
                 $pengembalians[$i]->status = 'active';
                        $bulan = "P1M";
                        $pengembalians[$i]->jatuh_tempo = $today->add(new \DateInterval($bulan))->format("Y-m-d");
                        $pengembalians[$i]->save(false);
                    }
                   } else {
                    echo 'remove all. then recreate';
                   }

                  $emp_id = $_POST['SigKasbon']['employee_id'];
                  $emp_model = Employee::find()->andWhere(['id' => $emp_id])->One();

                  $model = SigKasbon::find()->andWhere(['employee_id' => $emp_id, 'sig_project_id' => $id, 'status' => 'active'])->One();
                  $kasbonview = 'kasbon2';


         }
   else if (isset($_POST['tutup'])) {

    $model->status = 'inactive';
    $model->save();

          Yii::$app->getSession()->setFlash('success', 'Kasbon di tutup');

                    return $this->redirect(['kasbon','id' => $model->sig_project_id]);

   }

   $golongan = new Golongan;
                           return $this->render($kasbonview, [
                            'model' => $model,
                            'emp_model' => $emp_model,
                            'pengembalians' => $pengembalians,
                            'golongan' => $golongan,
                           ]);


           } else {
            // belum search NIK
            $model = new SigKasbon();
            $emp_model = new Employee();
            $count = count(Yii::$app->request->post('SigPengembalianKasbon', []));
           $pengembalians = [new SigPengembalianKasbon()];
               for($i = 1; $i < $count; $i++) {
                   $pengembalians[] = new SigPengembalianKasbon();
               }
                $golongan = new Golongan;

           return $this->render('kasbon', [
            'model' => $model,
            'emp_model' => $emp_model,
            'pengembalians' => $pengembalians,
                           'golongan' => $golongan,
           ]);

           }

    }



    public function actionPotongan($id)
    {
     return $this->render('potongan', [$id
     ]);
    }

    public function actionPendapatan($id, $from, $to)
    {


     $schedules = new Schedule();

    $activearray = $schedules->activeArray('9001', $from, $to);
    $absensiarray = $schedules->absensiArray('9001', $from, $to);

    //echo '<pre>';
    //print_r($activearray);
    //echo '</pre>';
     $dataProvider = new ActiveDataProvider([
         'pagination' => false,
         'query' => Siggaji::find()
         ->andWhere(['sig_project_id' => $id])
         ->andWhere(['in', 'start_period', (isset($from)) ? [$from] : ['0']])
                 ->andWhere(['in', 'end_period', (isset($to)) ? [$to]: ['0']])

   ]);

           if($_POST) {
                if(isset($_POST['print'])) {
                     $content =  $this->renderPartial('_rekapform', ['dataProvider' => $dataProvider,]);
                     $pdf = Yii::$app->pdf;
                     $pdf->content = $content;
                     return $pdf->render();
                } else if(isset($_POST['search'])) {
                 $from = Yii::$app->request->post()['start_period'];
                  $to = Yii::$app->request->post()['finish_period'];

                     return $this->redirect(['pendapatan', 'id' => $id, 'from'=>$from ,'to'=>$to]);

                }
           }


    echo  $content =  $this->render('_pendapatanform', [
      'dataProvider' => $dataProvider,
      'from' => $from,
      'to' => $to,
      'activearray' => $activearray,
      ]);


    }

    public function actionPilihproyek_pendapatan()
    {

     $projects = SigProject::find()->andWhere(['status'=> 'active'])->All();
     $dataProvider = new ActiveDataProvider([
    'query' => SigProject::find()->andWhere(['status'=> 'active']),
         'pagination' => false,
         ]);
     return $this->render('pilihproyek_pendapatan', [
'projects' => $projects
     ]);
    }

    public function actionPilihproyek_potongan()
    {

     $projects = SigProject::find()->andWhere(['status'=> 'active'])->All();
     $dataProvider = new ActiveDataProvider([
    'query' => SigProject::find()->andWhere(['status'=> 'active']),
         'pagination' => false,
]);


     return $this->render('pilihproyek_potongan', [
'projects' => $projects
     ]);

    }

    public function actionPilihproyek_payroll()
    {

     $projects = SigProject::find()->andWhere(['status'=> 'active'])->All();
     $dataProvider = new ActiveDataProvider([
    'query' => SigProject::find()->andWhere(['status'=> 'active']),
         'pagination' => false,
]);


     return $this->render('pilihproyek_payroll', [
'projects' => $projects
     ]);

    }
    /**
     * Lists all Siggaji models.
     * @return mixed
     */
    public function actionIndex()
    {
        //$searchModel = new SiggajiSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [

        ]);
    }

    /**
     * Displays a single Siggaji model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }



public function actionCreate()
{

 $employeelist = Employee::find()->andWhere(['status' => 'active'])->select(['id', 'org_id'])->asArray()->All();
       $dataProvider = new ArrayDataProvider([
           'allModels' => $employeelist,
           'pagination' => false,
           'sort' => [
           'attributes' => ['id', 'org_id'],
           ],
       ]);


if(isset($_POST['edit'])){
       $gajisss = Yii::$app->request->post('Siggaji', []);


          foreach ($gajisss as $key => $gajiss) { //iterate project_id

            $doesprojectexist = Siggaji::find()
            ->andWhere(['sig_project_id' => $key])
            ->andWhere(['start_period' => $_POST['start_period']])
            ->andWhere(['end_period' => $_POST['finish_period']])

            ->All();

            if(sizeof($doesprojectexist) > 0){
             echo '<br/>ada isi';
             foreach ($gajiss as $key2 => $gajis) { //iterate employee_id

                    foreach ($gajis as $key3 => $gaji) { //get siggaji_id
                     //UNTUK project yang baru ditambahkan maka tidak akan ada siggaji id0nya

                       $gajimodel = $this->findModel($key3);
                       $gajimodel->gaji_pokok = $gaji['gaji_pokok'];
                       $gajimodel->tunjangan_jabatan = $gaji['tunjangan_jabatan'];
                       $gajimodel->pendapatan_intern = $gaji['pendapatan_intern'];
                       $gajimodel->bca = $gaji['bca'];
                       $gajimodel->btn = $gaji['btn'];
          if($gajimodel->save()) {
                          Yii::$app->getSession()->setFlash('success', 'Edit saved');

          } else {
           print_r($gajimodel->errors);
           Yii::$app->getSession()->setFlash('error', 'Edit error');
          }

              }

             }


            } else {
             echo '<br/>KOSONG';
$savingarray = [];

               foreach ($gajiss as $key2 => $gajis) { //iterate employee_id
                       $gajimodel = new Siggaji();
                       $gajimodel->sig_project_id = $key;
                       $gajimodel->employee_id = $key2;
                       $gajimodel->start_period = $_POST['start_period'];
                       $gajimodel->end_period = $_POST['finish_period'];
                       print_r($gajis);
                       //echo $gajis;
                       echo '<br/>';
                       $gajimodel->gaji_pokok = $gajis[0]['gaji_pokok'];
                                 $gajimodel->tunjangan_jabatan = $gajis[1]['tunjangan_jabatan'];
                                           $gajimodel->pendapatan_intern = $gajis[2]['pendapatan_intern'];
                                           $gajimodel->bca = $gajis[3]['bca'];
                                           $gajimodel->btn = $gajis[4]['btn'];
                       array_push($savingarray, [$key2, $key, $gajimodel->gaji_pokok, $gajimodel->tunjangan_jabatan, $gajimodel->pendapatan_intern, $gajimodel->bca, $gajimodel->btn, $gajimodel->start_period, $gajimodel->end_period]);

                     }
                     if(Yii::$app->db->createCommand()->batchInsert('sig_gaji', ['employee_id', 'sig_project_id', 'gaji_pokok','tunjangan_jabatan','pendapatan_intern','bca','btn','start_period','end_period'], $savingarray)->execute())
                    {
                    } else {
                       ECHO 'ERROR';
                    }









            }




          }

    return $this->redirect('../default/gajibulanan');

}  elseif(isset($_POST['create'])){

 $gajisss = Yii::$app->request->post('Siggaji', []);
$savingarray = [];

foreach ($gajisss as $key => $gajiss) { //iterate project_id
  foreach ($gajiss as $key2 => $gajis) { //iterate employee_id
          $gajimodel = new Siggaji();
          $gajimodel->sig_project_id = $key;
          $gajimodel->employee_id = $key2;
          $gajimodel->start_period = $_POST['start_period'];
          $gajimodel->end_period = $_POST['finish_period'];

          $gajimodel->gaji_pokok = $gajis[0]['gaji_pokok'];
          $gajimodel->tunjangan_jabatan = $gajis[1]['tunjangan_jabatan'];
          $gajimodel->pendapatan_intern = $gajis[2]['pendapatan_intern'];
          $gajimodel->bca = $gajis[3]['bca'];
          $gajimodel->btn = $gajis[4]['btn'];
          array_push($savingarray, [$key2, $key, $gajimodel->gaji_pokok, $gajimodel->tunjangan_jabatan, $gajimodel->pendapatan_intern, $gajimodel->bca, $gajimodel->btn, $gajimodel->start_period, $gajimodel->end_period]);

        }}

      if(Yii::$app->db->createCommand()->batchInsert('sig_gaji', ['employee_id', 'sig_project_id', 'gaji_pokok','tunjangan_jabatan','pendapatan_intern','bca','btn','start_period','end_period'], $savingarray)->execute())
     {
     } else {
        ECHO 'ERROR';
     }
         Yii::$app->getSession()->setFlash('success', 'Edit saved');
         return $this->redirect('../default/gajibulanan');

} elseif(isset($_POST['search'])){

  $gaji = [new Siggaji()];

$gajis = Siggaji::find()
->andWhere(['in', 'start_period', [$_REQUEST['start_period']]])
->andWhere(['in', 'end_period', [$_REQUEST['finish_period']]])
->asArray()
->All();

if(sizeof($gajis) > 0) {

 $gajiarray = ArrayHelper::map($gajis, 'sig_project_id', 'id','employee_id');

 $query = Siggaji::find()
 ->andWhere(['in', 'start_period', [$_REQUEST['start_period']]])
 ->andWhere(['in', 'end_period', [$_REQUEST['finish_period']]]);

 $dataProvider = new ArrayDataProvider([
     'allModels' => $gajiarray,
     'pagination' => [
         'pageSize' => 70,
     ],
     'sort' => [
         'defaultOrder' => [
         ]
     ],
 ]);

} else {


  $gajiarray = [new Siggaji()];
 $employeelist = Employee::find()->andWhere(['status' => 'active'])->select(['id', 'org_id'])->asArray()->All();
       $dataProvider = new ArrayDataProvider([
           'allModels' => $employeelist,
           'pagination' => false,
           'sort' => [
           'attributes' => ['id', 'org_id'],
           ],
       ]);

}

        return $this->render('search', [
          //  'gajis' => $gajiarray,
            'start_period' => $_POST['start_period'],
            'finish_period' => $_POST['finish_period'],
            'dataProvider' => $dataProvider, //arraydataprovider -> employeelist
        ]);

       } else { //KONDISI SAAT BARU MASUK

        $gaji = [new Siggaji()];
        //return $this->render('_searchform', [
         return $this->render('search', [
           //  'model' => $gaji,
             'start_period' => '',
             'finish_period' => '',
             'dataProvider' => $dataProvider,
         ]);

       }

}
    /**
     * Creates a new Siggaji model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreatdsadadae3()
    {
        $model = array();
        $newgaji = new Siggaji();

        array_push($model, $newgaji);
        //$searchModel = new SiggajiSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $employeelist = Employee::find()->andWhere(['status' => 'active'])->select(['id', 'org_id'])->asArray()->All();
              $dataProvider = new ArrayDataProvider([
                  'allModels' => $employeelist,
                  'pagination' => false,
                  'sort' => [
                  'attributes' => ['id', 'org_id'],
                  ],
              ]);
                $employeesearchModel = new EmployeeSearch();
                $employeedataProvider = $employeesearchModel->search(Yii::$app->request->queryParams);
                $last_period = Siggaji::find()->orderBy('end_period DESC')->one();
                if(isset($last_period)){
                    $x=$last_period->end_period;
                } else {
                    $x=0;
                }

                $first_period = Siggaji::find()->orderBy('start_period ASC')->one();

            if($_REQUEST) {
            if ((!empty($_REQUEST['start_period'])) && (!empty($_REQUEST['finish_period']))) {
            //if (false) {

            $start_period = $_REQUEST['start_period'];
            $finish_period = $_REQUEST['finish_period'];
            if(($start_period > $x) && ($finish_period > $start_period)) {
            //if (true) {
             foreach($_REQUEST as $key => $value) {
                 $exploded = explode("-",$key);
                 if (sizeof($exploded) > 3) {
                 $employeelist[$exploded[3]][$exploded[0] . '-' . $exploded[2]] = $value;
                }
             }

            foreach ($employeelist as $gajiemployee){
            $i = 0;
            foreach($gajiemployee as $key => $value) {

                   $exploded = explode("-",$key);
                 if ($i % 3 == 2) {
                   $gajimodel = new Siggaji();

                   $gajimodel->employee_id = $gajiemployee['id'];
                       $gajimodel->sig_project_id = $exploded[1];
                         $gajimodel->employee_id = $gajiemployee['id'];
                         $gajimodel->start_period = $start_period;
                         $gajimodel->end_period = $finish_period;
                  }
                        if (sizeof($exploded) > 1) {
                          $gajimodel->{$exploded[0]} = is_null($value) ? '0' : $value;
                          if ($i % 3 == 1)
                             {
                              if ($gajimodel->validate()) {
                              $gajimodel->save();} else {
                                   Yii::$app->getSession()->setFlash('danger', 'input tidak lengkap');
                                     return $this->redirect('create');
                              }
                             }
                         }
                           $i++;
                             }
            }
       Yii::$app->getSession()->setFlash('success', 'data gaji sudah masuk');
  return $this->redirect('../default/gajibulanan');
} else {
echo 'invalid period selection. please click back';
echo '<br/>last period : ';
echo $last_period->end_period;
echo '<br/>start period : ';
echo $start_period;
echo '<br/>finish period : ';
echo $finish_period;
}
} else {
   echo 'data periode tidak diisi. click back di browser';
}
        } else {
            return $this->render('create', [
                'model' => $model,
            //    'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'employeedataProvider' => $employeedataProvider,
                'employeesearchModel' => $employeesearchModel,
            ]);
        }
    }

    /**
     * Updates an existing Siggaji model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Siggaji model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Siggaji model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Siggaji the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Siggaji::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

public function actionEdit()
{

 $model = array();
         $newgaji = new Siggaji();

 array_push($model, $newgaji);

 //  $searchModel = new SiggajiSearch();
  // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


     $employeelist = Employee::find()->andWhere(['status' => 'active'])->select(['id', 'org_id'])->asArray()->All();
$gajilist = Siggaji::find()
->andWhere(['in', 'start_period', '0'])
->andWhere(['in', 'end_period', '0']);

           $dataProvider = new ActiveDataProvider([
               'query' => $gajilist,
               'pagination' => false,
               'sort' => [
                   'attributes' => ['id', 'org_id'],
               ],
           ]);
                     $employeesearchModel = new EmployeeSearch();
                     $employeedataProvider = $employeesearchModel->search(Yii::$app->request->queryParams);


if($_POST) {

$dataProvider = new ActiveDataProvider([
  'pagination' => false,
'query' => Siggaji::find()
->andWhere(['in', 'start_period', $_REQUEST['start_period']])
->andWhere(['in', 'end_period', $_REQUEST['finish_period']]),
]);
}

   return $this->render('_editform', [

   'model' => $model,
//    'searchModel' => $searchModel,
   'dataProvider' => $dataProvider,
   'employeedataProvider' => $employeedataProvider,
   'employeesearchModel' => $employeesearchModel,
   ]);

}


protected function printReport($content) {

    $pdf = new Pdf([
        // set to use core fonts only
        //'mode' => Pdf::MODE_CORE,
        // A4 paper format
        'format' => Pdf::FORMAT_A4,
        'orientation' => Pdf::ORIENT_PORTRAIT,
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER,
        // your html content input
        'content' => $content,
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting
        'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
        // any css to be embedded if required
        'cssInline' => '.kv-heading-1{font-size:18px}',
         // set mPDF properties on the fly
        'options' => ['title' => 'Krajee Report Title'],
         // call mPDF methods on the fly
        'methods' => [
            'SetHeader'=>['Krajee Report Header'],
            'SetFooter'=>['{PAGENO}'],
        ]
    ]);

    // return the pdf output as per the destination setting
    return $pdf->render();

}


    public function actionRekap($id)
    {
         $dataProvider = new ActiveDataProvider([
             'pagination' => false,
             'query' => Siggaji::find()
             ->andWhere(['sig_project_id' => $id])
             ->andWhere(['in', 'start_period', (isset($_REQUEST['start_period'])) ? $_REQUEST['start_period'] : '0'])
                     ->andWhere(['in', 'end_period', (isset($_REQUEST['finish_period'])) ? $_REQUEST['finish_period'] : '0'])

       ]);

     if($_POST) {
          if(isset($_POST['print'])) {
               $content =  $this->renderPartial('_rekapform', ['dataProvider' => $dataProvider,]);
               $pdf = Yii::$app->pdf;
               $pdf->content = $content;
               return $pdf->render();
          } else if(isset($_POST['search'])) {
                return $this->redirect(['rekap','id' => $id,'start_period' => $_REQUEST['start_period'],'finish_period' => $_REQUEST['finish_period']]);
           /*$dataProvider = new ActiveDataProvider([
               'pagination' => false,
               'query' => Siggaji::find()
               ->andWhere(['sig_project_id' => $id])
               ->andWhere(['in', 'start_period', (isset($_REQUEST['start_period'])) ? $_REQUEST['start_period'] : '0'])
               ->andWhere(['in', 'end_period', (isset($_REQUEST['finish_period'])) ? $_REQUEST['finish_period'] : '0'])
         ]);*/
          }
     }
        echo  $content =  $this->render('_rekapform', [
          'dataProvider' => $dataProvider,
          ]);

    }

    public function actionRekapitulasi()
    {

     $projects = SigProject::find()->andWhere(['status'=> 'active'])->All();
     $dataProvider = new ActiveDataProvider([
    'query' => SigProject::find()->andWhere(['status'=> 'active']),
         'pagination' => false,
]);

//$this->view->title = 'Posts List';
return $this->render('rekapitulasi', ['projects' => $projects]);


//return $this->render('rekapitulasi');

    }






public function actionVerifikasiabsen($id, $from, $to)
{
 return $this->render('verifikasiabsen',[
  'id'=>$id,
  'from' => $from,
  'to' => $to,
 ]);
}


public function actions()
{

 return ArrayHelper::merge(parent::actions(), [
    'editbook' => [                                       // identifier for your editable action
        'class' => EditableColumnAction::className(),     // action class name
        'modelClass' => Employee::className(),                // the update model class
     /*   'outputValue' => function ($model, $attribute, $key, $index) {
             $fmt = Yii::$app->formatter;
             $value = $model->$attribute;                 // your attribute value
             if ($attribute === 'buy_amount') {           // selective validation by attribute
                 return $fmt->asDecimal($value, 2);       // return formatted value if desired
             } elseif ($attribute === 'publish_date') {   // selective validation by attribute
                 return $fmt->asDate($value, 'php:Y-m-d');// return formatted value if desired
             }
             return '';                                   // empty is same as $value
        },
        'outputMessage' => function($model, $attribute, $key, $index) {
              return '';                                  // any custom error after model save
        },
        */
        // 'showModelErrors' => true,                     // show model errors after save
        // 'errorOptions' => ['header' => '']             // error summary HTML options
        // 'postOnly' => true,
        // 'ajaxOnly' => true,
        // 'findModel' => function($id, $action) {},
        // 'checkAccess' => function($action, $model) {}
    ]
]);
}



}
