<?php

namespace app\modules\sig\controllers;

use Yii;
use app\modules\sig\models\Sigpph;
use app\modules\sig\models\SigpphSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SigpphController implements the CRUD actions for Sigpph model.
 */
class SigpphController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sigpph models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SigpphSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('_perhitunganform', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sigpph model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sigpph model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sigpph();
        $sigpro = $_GET['id'];
        
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->db->createCommand()
               ->batchInsert(sigpph::tableName(), ['id', 'tipe','operator','value_1','value_2','sig_project_id'],
               [
                 [$model->id, 'takehome_pay_1',$model['operator'][0],$model['value_1'][0],$model['value_2'][0],$sigpro],
                 [$model->id, 'takehome_pay_2',$model['operator'][1],$model['value_1'][1],$model['value_2'][1],$sigpro],
                 [$model->id, 'takehome_pay_3',$model['operator'][2],$model['value_1'][2],$model['value_2'][2],$sigpro],
                 [$model->id, 'tidak_punya_npwp','','',$model['value_2'][3],$sigpro],
               ])
               ->execute();
            Yii::$app->db->createCommand()
               ->batchInsert(sigpph::tableName(), ['id', 'tipe','value_1','sig_project_id'],
               [
                 [$model->id, 'TK',$model['value_1'][3],$sigpro],
                 [$model->id, 'K0',$model['value_1'][4],$sigpro],
                 [$model->id, 'K1',$model['value_1'][5],$sigpro],
                 [$model->id, 'K2',$model['value_1'][6],$sigpro],
                 [$model->id, 'K3',$model['value_1'][7],$sigpro],
               ])
               ->execute();
            return $this->redirect(['siggaji/pilihproyek_potongan']);
            $model->save();
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Sigpph model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $sigpph=SigPph::find()->where(['sig_project_id' => $model->sig_project_id])->All();
        $tipetosave = new Sigpph;
        $firstvaluetosave = new Sigpph;
        $secondvaluetosave = new Sigpph;
        $operatortosave = new Sigpph;
        $sigprojecttosave = new Sigpph;

        if ($sigpph->load(Yii::$app->request->post())
           && $tipetosave->load(Yii::$app->request->post())
           && $firstvaluetosave->load(Yii::$app->request->post())
           && $secondvaluetosave->load(Yii::$app->request->post())
           && $operatortosave->load(Yii::$app->request->post())
           && $sigprojecttosave->load(Yii::$app->request->post())
           ) {
            
            $model->save();
                
                  $i = 0;
                 foreach ($sigpph as $sigpphs){
                   $models->tipe = $tipetosave->tipe[$i];
                   $models->save();
                   $i++;
                  }

                  $i = 0;
                 foreach ($sigpph as $sigpphs){
                   $models->value_1 = $firstvaluetosave->value_1[$i];
                   $models->save();
                   $i++;
                  }
                  $i = 0;
                 foreach ($sigpph as $sigpphs){
                   $models->value_2 = $secondvaluetosave->value_2[$i];
                   $models->save();
                   $i++;
                  }
                  $i = 0;
                 foreach ($sigpph as $sigpphs){
                   $models->operator = $operatortosave->operator[$i];
                   $models->save();
                   $i++;
                  }
                 foreach ($sigpph as $sigpphs){
                   $models->sig_project_id = $sigprojecttosave->sig_project_id[$i];
                   $models->save();
                   $i++;
                  }
            
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Sigpph model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sigpph model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sigpph the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sigpph::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
