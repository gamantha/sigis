<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
use app\modules\organization\models\Employee;
use yii\helpers\ArrayHelper;
use app\modules\sig\models\Siggaji;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\sig\models\SigBpjsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sig Payroll';

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji dan Upah'), 'url' => ['default/gajidanupah']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/gajibulanan']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pilih Proyek'), 'url' => ['siggaji/pilihproyek_payroll']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sig-payroll-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



<?php

$form = ActiveForm::begin();
$employeelist = Employee::find()->andWhere(['status' => 'active'])->asArray()->All();

  $employeearray=ArrayHelper::map($employeelist,'id','org_id');



echo $form->field($employeebank, 'bank_name')->dropDownList(['prompt'=>'Select...','bca' => 'BCA', 'btn'=>'BTN', ])->label('Nama Bank');
echo 'start period';
echo DatePicker::widget([
 'name'=>'start_period',
     'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','style' => 'z-index:99;'],
      'value' => (isset($_POST['start_period']) ? $_POST['start_period'] : ''),

]);
echo '<br/>finish period';
echo DatePicker::widget([
 'name'=>'finish_period',
     'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','maxlength'=>'25','style' => 'z-index:99;'],
            'value' => (isset($_POST['finish_period']) ? $_POST['finish_period'] : ''),
]);
echo '<br/>';
echo '<br/>';
 ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            //['class' => 'yii\grid\SerialColumn'],

            //'id',

            [
             'label' => 'NIK ',
             'contentOptions' =>['class' => 'table_class'],

             'value'=>function($data) {
        return isset($data->employee->org_id) ? ($data->employee->org_id):"";
   //return ($data->departmentRole->department->department_name);
                },
            ],

            [
             'label' => 'Nama Depan ',
             'contentOptions' =>['class' => 'table_class'],
             'value'=>function($data) {
           return isset($data->employee->employeeProfiles->first_name) ? ($data->employee->employeeProfiles->first_name):"";
   //return(date('Y-m-d', strtotime($data->employments->first_day)));
                },
            ],
            [
             'label' => 'Nama Belakang ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
   return isset($data->employee->employeeProfiles->last_name) ? ($data->employee->employeeProfiles->last_name):"";
    //return(date('Y-m-d', strtotime($data->employments->start)));
                },
            ],

            [
             'label' => 'NO. ACCOUNT ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
               //echo '<br/><br/><br/><br/>' . $data->employee->id->employeeBanks->id;
    if(sizeof($data->employee->employeeBanks) > 0){
//     echo $data->employee->employeeBanks->id;
 $banks = $data->employee->employeeBanks;
 foreach ($banks as $bank){
  if ($bank->bank_name == $_REQUEST['EmployeeBank']['bank_name']) {
        $retval = $bank->account_number;
    //    $retval = 'yes';
  } else {
  $retval = '';
 }
  //print_r($_POST);
 }

    } else {
     $retval = '';
    }
    return $retval;

   //return(date('Y-m-d', strtotime($data->employments->finish)));
                },
            ],

            [
             'label' => 'NAMA ACCOUNT ',
                'contentOptions' =>['class' => 'table_class'],
                'value'=>function($data) {
                 //echo '<br/><br/><br/><br/>' . $data->employee->id->employeeBanks->id;
      if(sizeof($data->employee->employeeBanks) > 0){
  //     echo $data->employee->employeeBanks->id;
   $banks = $data->employee->employeeBanks;
   foreach ($banks as $bank){
    if ($bank->bank_name == $_REQUEST['EmployeeBank']['bank_name']) {
          $retval = $bank->account_name;
    } else {
    $retval = '';
   }
    //print_r($_POST);
   }

      } else {
       $retval = '';
      }
      return $retval;

                  },
            ],
            //GAJI POKOK DARI SIG_GAJI

            [
             'label' => 'JUMLAH (gaji pokok)',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
    //return isset($data->sig_gaji->gaji_pokok) ? ($data->sig_gaji->gaji_pokok):"";
   //return ($data->employments->contract_type);
   return $data->gaji_pokok;
                },
            ],

       //     ['class' => 'yii\grid\ActionColumn',
         //              'template'=>'{update}{delete}',

           //              ],



        ],
    ]);


                 echo Html::submitButton('Search', ['class' => 'btn btn-primary', 'name' => 'search']);



    ?>
 <?php ActiveForm::end(); ?>
</div>
