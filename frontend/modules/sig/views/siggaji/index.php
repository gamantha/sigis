<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\sig\models\SiggajiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Siggajis';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji dan Upah'), 'url' => ['default/gajidanupah']];?>
<div class="siggaji-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Siggaji', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'employee_id',
            'sig_project_id',
            'gaji_pokok',
            'tunjangan_jabatan',
            // 'pendapatan_intern',
            // 'start_period',
            // 'end_period',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
