<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\jui\Datepicker;


/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Siggaji */

$this->title = 'Verifikasi Absen';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji dan Upah'), 'url' => ['default/gajidanupah']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/gajibulanan']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/absensi']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="siggaji-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="schedule-form">

        <?php $form = ActiveForm::begin(); ?>


        <?php
    echo 'NIK<br/>';
        echo Html::textInput('id',$id,

          ['class' => 'form-control']
        );
        ?>
        <br/>
<div class="well">

ARIF : bagian ini otomatis terisi dengan informasi employee

</div>
        <?php
        echo '<br/>start period';
        echo Datepicker::widget([
         'name'=>'from',
         'value'=>$from,
             'dateFormat' => 'yyyy-MM-dd',
              'options' => ['class' => 'form-control','style' => 'z-index:99;'],
        ]);
        echo '<br/>finish period';
        echo Datepicker::widget([
         'name'=>'to',
              'value'=>$to,
             'dateFormat' => 'yyyy-MM-dd',
              'options' => ['class' => 'form-control','maxlength'=>'25','style' => 'z-index:99;'],
        ]);
    ?>
    <br/>
<div class="well">

ARIF : bagian ini radio button untuk tipe absen
dan box keterangan

</div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' =>'btn btn-primary', 'name'=>'search']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>


</div>
