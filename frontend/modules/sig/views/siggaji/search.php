

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
//use yii\grid\GridView;
use kartik\grid\GridView;
use common\Headergrouping;
use app\modules\sig\models\SigProject;
use app\modules\sig\models\Siggaji;
use app\modules\sig\models\SiggajiSearch;
use app\modules\organization\models\Employee;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\View;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Siggaji */
/* @var $form yii\widgets\ActiveForm */

//$this->registerJsFile(Url::base() . '/js/jquery.floatThead.js' , ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Url::base() . '/js/tableHeadFixer.js' , ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = Yii::t('app', 'Input Gaji');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Personalia'), 'url' => ['default/ketentuanpersonalia']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji dan Upah'), 'url' => ['default/gajidanupah']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/gajibulanan']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="siggaji-form">

 <p>
     <?= Html::a('Tambah PT', ['sigproject/create'], ['class' => 'btn btn-success']) ?>

     <?= Html::a('Hapus PT', ['sigproject/index'], ['class' => 'btn btn-danger']) ?>
 </p>
    <?php $form = ActiveForm::begin(); ?>
<p><i>* period yang baru harus sesudah period gaji terakhir</i></p>
<?php
echo 'start period';
echo DatePicker::widget([
 'name'=>'start_period',
     'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','style' => 'z-index:99;'],
      'value' => $start_period,
]);
echo '<br/>finish period';
echo DatePicker::widget([
 'name'=>'finish_period',
     'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','maxlength'=>'25','style' => 'z-index:99;'],
         'value' => $finish_period,
]);
echo '<br/>';

 ?>

    <?= Html::submitButton('Search', ['class' => 'btn btn-primary', 'name' => 'search']) ?>
<?php

$gajinotexist = isset($dataProvider->getModels()[1]['org_id']);
$searched = (!empty($_POST['start_period']) && !empty($_POST['finish_period']));

$gridcolumns = [
[
 'label' => 'NIK ',
 //'attribute' => 'org_id',
    'headerOptions' =>['rowspan' => 2,],
    //'contentOptions' =>['class' => 'table_class',
   // 'style' => 'z-index:99;'
  // ],
'value'=>function  ($data,$key,$index,$widget) {
if (array_key_exists('id', $data))
{
 $employee = Employee::find()->andWhere(['id'=> $data['id']])->One();
return $employee->org_id;
 //'kondisi saat kosong/baru'
} else {
 $employee = Employee::findOne($key);
 return $employee->org_id;
}
//return $data->employee->org_id;

//
},

],

[
 'label' => 'Nama ',
 //'attribute' => 'org_id',
 'header' => '<div style="
 margin:0 -10px 0 -10px;"></div><div>Nama</div>',
    'headerOptions' =>[
 'rowspan' => 2,
   ],
    'contentOptions' =>['class' => 'table',
    //'style' => 'z-index:99;'
   ],
'value'=>function ($data,$key,$index,$widget) {

 if (array_key_exists('id', $data))
 {
  $employee = Employee::find()->andWhere(['id'=> $data['id']])->One();
 //return $employee->org_id;
  //'kondisi saat kosong/baru'
 } else {
  $employee = Employee::findOne($key);
  // /return $employee->org_id;
 }

if(isset($employee->employeeProfiles->first_name)) {
 $fn = $employee->employeeProfiles->first_name;
} else {
 $fn = '';
}

if(isset($employee->employeeProfiles->last_name)) {
 $ln = $employee->employeeProfiles->last_name;
} else {
 $ln = '';
}

    return $fn . ' ' .  $ln;


},
],

];


$projects = SigProject::find()->andWhere(['status'=> 'active'])->All();

$columnindex = 0;
$headercolumns=[];
foreach ($projects as $project){

$projectcolumn = [
 'name'=>$project->name,
 'start'=>$columnindex + 3, //indeks kolom 3
 'end'=>$columnindex + 5, //indeks kolom 4
];
 array_push($headercolumns, $projectcolumn);
 $columnindex = $columnindex + 3;

}
$gaji_pokok_column_total = 0;
$tunjangan_jabatan_column_total = 0;
$pendapatan_intern_column_total = 0;
$gajimodelarray = $dataProvider->getModels();

$row_total[] = array();


foreach ($projects as $index => $project)
{
$gaji_pokok_column_total = 0;
$tunjangan_jabatan_column_total = 0;
$pendapatan_intern_column_total = 0;
$bca_column_total = 0;
$btn_column_total = 0;

$project_row_total = 0;

                 if(!$gajinotexist) {
                        foreach($gajimodelarray as $index2 => $gajimodelarray2)
                        {

if(isset($gajimodelarray2[$project->id])) {
 $rt = $gajimodelarray2[$project->id];
} else {
 $rt = 0;
}

                         $gajisub = Siggaji::find()->andWhere(['id' => $rt])->One();
if(sizeof($gajisub) > 0) {

} else {
 $gajisub = new Siggaji();
 $gajisub->gaji_pokok = 0;
 $gajisub->tunjangan_jabatan = 0;
 $gajisub->pendapatan_intern = 0;
}
                          $gaji_pokok_column_total = $gaji_pokok_column_total + $gajisub->gaji_pokok;
                           $tunjangan_jabatan_column_total = $tunjangan_jabatan_column_total + $gajisub->tunjangan_jabatan;
                            $pendapatan_intern_column_total = $pendapatan_intern_column_total + $gajisub->pendapatan_intern;
                            $project_row_total = $gajisub->gaji_pokok + $gajisub->tunjangan_jabatan;

                        if (isset($row_total[$index2])) {

                        } else {
                         $row_total[$index2] = 0;
                        }
                       $row_total[$index2] = $row_total[$index2] + $project_row_total;

                        }
                 }

 $gajipokokcolumns =
        [
         'label' => 'Gaji pokok ',
         'attribute' => 'gaji_pokok',
         'header' => '<div style="
         margin:0 -10px 0 -10px;"></div><div>Gaji pokok</div>',
         'headerOptions' => [
          'rowspan' => 2,
         ],
         'footerOptions'=>[
          'id' => 'gaji_pokok_' . $project->id,
          'value' => '0',

          ],
          'footer' => $gaji_pokok_column_total ,

            'pageSummary'=>true,
            'pageSummaryFunc' => GridView::F_SUM,
            'options'=>[
             'value' => $project->id,
            ],
            //'contentOptions' =>['class' => 'table_class'],
            'format' => 'raw',
         //   'value' => $form->field($project, "[$index]gaji_pokok")->label(false),

        'value'=>function ($data,$key,$index,$widget) {

                $gaji_id = isset($data[$widget->options['value']]) ? $data[$widget->options['value']] : '';
                $employee_id = isset($data['id']) ? $data['id'] : $key;

                $projectid = $widget->options['value'];
                if ($gaji_id == '') {
                          $gajipok = new Siggaji();
                } else {
                         $gajipok = Siggaji::find()->andWhere(['id' =>$gaji_id])->One();
                }
                    return '<input type="text" class="gaji_pokok inputgaji col' .$widget->options['value'].' row'.$employee_id.'" value="'. $gajipok->gaji_pokok.'" name="Siggaji['.$projectid.']['. $employee_id . ']['.$gaji_id.'][gaji_pokok]" id="siggaji-'. $projectid. '-'.$employee_id. '-'. $gaji_id .'-gaji_pokok">';
               },
       ];

  $tunjanganjabatancolumns =
  [
   //'label' => 'Tunjangan Jabatan',
     'header' => '<div style="

     ">' . $project->name . '</div><div>Tunjangan Jabatan</div>',
   'attribute' => 'tunjangan_jabatan',
   'footerOptions'=>[
    'id' => 'tunjangan_jabatan_' . $project->id,
    'value' => '0',
],
'footer' => $tunjangan_jabatan_column_total ,
      'pageSummary'=>true,
      'options'=>[
       'value' => $project->id,
      ],
      'contentOptions' =>['class' => 'table_class'],
      'format' => 'raw',
  'value'=>function ($data,$key,$index,$widget) {
   $gaji_id = isset($data[$widget->options['value']]) ? $data[$widget->options['value']] : '';
   $employee_id = isset($data['id']) ? $data['id'] : $key;

   $projectid = $widget->options['value'];


   if ($gaji_id == '') {
             $gajipok = new Siggaji();
   } else {
            $gajipok = Siggaji::find()->andWhere(['id' =>$gaji_id])->One();
   }
   return '<input type="text" class="tunjangan_jabatan inputgaji col' .$widget->options['value'].' row'.$employee_id.'" value="'. $gajipok->tunjangan_jabatan.'" name="Siggaji['.$projectid.']['. $employee_id . ']['.$gaji_id.'][tunjangan_jabatan]" id="siggaji-'. $projectid. '-'.$employee_id. '-'. $gaji_id .'-tunjangan_jabatan">';

  },
 ];

$pendapataninterncolumns =   [
 'header' => '<div style="
     margin:0 -10px 0 -10px;
 ">Pendapatan internal</div>',
   'attribute' => 'pendapatan_intern',
   'footerOptions'=>[
    'id' => 'pendapatan_intern_' . $project->id,
    'value' => '0',
],
'footer' => $pendapatan_intern_column_total ,
      'pageSummary'=>true,
      'options'=>[
       'value' => $project->id,
      ],
      'contentOptions' =>['class' => 'table_class'],
      'format' => 'raw',
      'value'=>function ($data,$key,$index,$widget) {
       $gaji_id = isset($data[$widget->options['value']]) ? $data[$widget->options['value']] : '';
       $employee_id = isset($data['id']) ? $data['id'] : $key;

       $projectid = $widget->options['value'];


       if ($gaji_id == '') {
                 $gajipok = new Siggaji();
       } else {
                $gajipok = Siggaji::find()->andWhere(['id' =>$gaji_id])->One();
       }
       return '<input type="text" class="pendapatan_intern inputgaji col' .$widget->options['value'].' '.$employee_id.'" value="'. $gajipok->pendapatan_intern.'" name="Siggaji['.$projectid.']['. $employee_id . ']['.$gaji_id.'][pendapatan_intern]" id="siggaji-'. $projectid. '-'.$employee_id. '-'. $gaji_id .'-pendapatan_intern">';

      },
 ];

 $subtotalcolumn =   [
  'header' => '<div style="
      margin:0 -10px 0 -10px;
  ">Sub Total</div>',
 //   'attribute' => 'subtotal',
    //'footerOptions'=>[
    // 'id' => 'subtotal_' . $project->id,
    // 'value' => '0',
 //],
 //'footer' => $subtotal_column_total ,
       'pageSummary'=>true,
       'options'=>[
        'value' => $project->id,
       ],
       'contentOptions' =>['class' => 'table_class'],
       'format' => 'raw',
       'value'=>function ($data,$key,$index,$widget) {
        $gaji_id = isset($data[$widget->options['value']]) ? $data[$widget->options['value']] : '';
        $employee_id = isset($data['id']) ? $data['id'] : $key;

        $projectid = $widget->options['value'];


        if ($gaji_id == '') {
                  $gajipok = new Siggaji();
        } else {
                 $gajipok = Siggaji::find()->andWhere(['id' =>$gaji_id])->One();
        }
            //return '<div class="subtotal" style="width:150px;" id="subtotal-'. $employee_id.'">Rp. '.$total.'</div>';
            return $gajipok->gaji_pokok + $gajipok->tunjangan_jabatan;
        //return '<input type="text" class="subtotal inputgaji col' .$widget->options['value'].' '.$employee_id.'" value="'. $gajipok->pendapatan_intern.'" name="Siggaji['.$projectid.']['. $employee_id . ']['.$gaji_id.'][pendapatan_intern]" id="siggaji-'. $projectid. '-'.$employee_id. '-'. $gaji_id .'-pendapatan_intern">';

       },
  ];

  $bcacolumn =   [
   'header' => '<div style="
       margin:0 -10px 0 -10px;
   ">BCA</div>',
     'attribute' => 'bca',
     'footerOptions'=>[
      'id' => 'bca_' . $project->id,
      'value' => '0',
  ],
  'footer' => $bca_column_total ,
        'pageSummary'=>true,
        'options'=>[
         'value' => $project->id,
        ],
        'contentOptions' =>['class' => 'table_class'],
        'format' => 'raw',
        'value'=>function ($data,$key,$index,$widget) {
         $gaji_id = isset($data[$widget->options['value']]) ? $data[$widget->options['value']] : '';
         $employee_id = isset($data['id']) ? $data['id'] : $key;

         $projectid = $widget->options['value'];


         if ($gaji_id == '') {
                   $gajipok = new Siggaji();
         } else {
                  $gajipok = Siggaji::find()->andWhere(['id' =>$gaji_id])->One();
         }
         return '<input type="text" class="bca inputgaji col' .$widget->options['value'].' '.$employee_id.'" value="'. $gajipok->bca.'" name="Siggaji['.$projectid.']['. $employee_id . ']['.$gaji_id.'][bca]" id="siggaji-'. $projectid. '-'.$employee_id. '-'. $gaji_id .'-bca">';

        },
   ];

   $btncolumn =   [
    'header' => '<div style="
        margin:0 -10px 0 -10px;
    ">BTN</div>',
      'attribute' => 'btn',
      'footerOptions'=>[
       'id' => 'btn_' . $project->id,
       'value' => '0',
   ],
   'footer' => $btn_column_total ,
         'pageSummary'=>true,
         'options'=>[
          'value' => $project->id,
         ],
         'contentOptions' =>['class' => 'table_class'],
         'format' => 'raw',
         'value'=>function ($data,$key,$index,$widget) {
          $gaji_id = isset($data[$widget->options['value']]) ? $data[$widget->options['value']] : '';
          $employee_id = isset($data['id']) ? $data['id'] : $key;

          $projectid = $widget->options['value'];


          if ($gaji_id == '') {
                    $gajipok = new Siggaji();
          } else {
                   $gajipok = Siggaji::find()->andWhere(['id' =>$gaji_id])->One();
          }
          return '<input type="text" class="btn inputgaji col' .$widget->options['value'].' '.$employee_id.'" value="'. $gajipok->btn.'" name="Siggaji['.$projectid.']['. $employee_id . ']['.$gaji_id.'][btn]" id="siggaji-'. $projectid. '-'.$employee_id. '-'. $gaji_id .'-btn">';

         },
    ];




 array_push($gridcolumns, $gajipokokcolumns, $tunjanganjabatancolumns, $pendapataninterncolumns, $subtotalcolumn, $bcacolumn, $btncolumn);
  //array_push($gridcolumns, $gajipokokcolumns);
}



              $totalcolumns =  [
                'header' => 'Total',
                     'format' => 'raw',
                         'headerOptions' =>['style' => 'z-index:50;'],
                'options'=>[
                 'value' => $project->id,
                ],
                   //'contentOptions' =>['class' => 'table_class'],
                 'value'=>function($data, $key,$index, $widget) use ($row_total){
                         $employee_id = isset($data['id']) ? $data['id'] : $key;
                         $total = isset($row_total[$employee_id]) ? $row_total[$employee_id] : '0';
                  return '<div class="totalgaji" style="width:150px;" id="total-pendapatan-'. $employee_id.'">Rp. '.$total.'</div>';
              //return '<div class="totalgaji" style="width:150px;" id="total-pendapatan-'. $employee_id.'">Rp. sasa</div>';

                   },
               ];

              array_push($gridcolumns, $totalcolumns);



echo GridView::widget([
	'id'=>'user-grid',
	'summary'=>'Total {count} data',
 'filterPosition' => GridView::FILTER_POS_BODY,
 //'responsive' => true,
 'options' => [
  //'id' => 'renola',
    'style' => ($searched) ? 'width: 1800px; margin-left: -300px;' : 'display:none;',
     //'style'=>'width: 1800px; margin-left: -300px;',
 ],
 'footerRowOptions' => [

    'style' => 'background-color: yellow; z-index:99;'
 ],
 'tableOptions' => [
  'id' => 'fixTable',
    //'style' => ($searched) ? 'display:inline; width: 1800px; margin-left: -300px;' : 'display:none;',
 ],
  'headerRowOptions' => [
  ],
  'showFooter' => true,
  'showHeader' => true,
 'containerOptions' => [
  'style' => 'overflow: auto; height:500px;',
 ],
 'hover'=> true,
  'floatHeaderOptions'=>[
   'debug' => true,
  ],

 	'dataProvider'=>$dataProvider,
	'columns'=>$gridcolumns,

]);



$this->registerJs(
"
$(document).ready(function() {
     $('#fixTable').tableHeadFixer({'left' : 2,'right' : 1, 'foot' : true});
     //$('#fixTable').tableHeadFixer({'z-index' : 50});
     function Init()
     {

     }

Init();





$('.inputgaji').change(function(){
 var res = $(this).attr('id');
 var res2 = res.split('-');

var tot = 0;
var col_tot = 0;
var i = 0;
var toadd = 0;
$('.row' + res2[2]).each(function(){
 if ($(this).val() === '') {
    toadd = 0;
} else {
 toadd = $(this).val();
}
tot = parseInt(tot) + parseInt(toadd);
// 4 - gaji_pokok/...
 // 3 - siggaji id
 // 2 - employee_id
 // 1 - project_id
 // 0 - siggaji
});


$('.' + res2[4] + '.col' + res2[1]).each(function(){

 if ($(this).val() === '') {
    toadd = 0;
} else {
 toadd = $(this).val();
}
col_tot = parseInt(col_tot) + parseInt(toadd);
});

$('#' + res2[4] + '_' + res2[1]).html(col_tot);

$('#total-pendapatan-' + res2[2]).html('Rp. ' + tot);
});

});

"
, View::POS_END, 'my-options');


?>

<div class="form-group">





    <?php
if ($searched) {
             echo Html::submitButton((isset($dataProvider->getModels()[1]['org_id'])) ? 'Create' : 'Edit', ['class' => 'btn btn-primary', 'name' => (isset($dataProvider->getModels()[1]['org_id'])) ? 'create' : 'edit']);
            }

     ?>
</div>

    <?php ActiveForm::end(); ?>

</div>
