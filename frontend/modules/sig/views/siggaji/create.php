<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Siggaji */

$this->title = 'Create Siggaji';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji dan Upah'), 'url' => ['default/gajidanupah']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/gajibulanan']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="siggaji-create">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= $this->render('_formdup', [
        'model' => $model,
       // 'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        //'employeedataProvider' => $employeedataProvider,
      //  'employeesearchModel' => $employeesearchModel,

    ]) ?>

</div>
