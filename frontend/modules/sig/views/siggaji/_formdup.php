

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\Datepicker;
//use yii\grid\GridView;
use kartik\grid\GridView;
use common\Headergrouping;
use app\modules\sig\models\SigProject;
use app\modules\sig\models\Siggaji;
use app\modules\sig\models\SiggajiSearch;
use app\modules\organization\models\Employee;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\View;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Siggaji */
/* @var $form yii\widgets\ActiveForm */

//$this->registerJsFile(Url::base() . '/js/jquery.floatThead.js' , ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Url::base() . '/js/tableHeadFixer.js' , ['depends' => [\yii\web\JqueryAsset::className()]]);


?>

<div class="siggaji-form">

 <p>
     <?= Html::a('Tambah PT', ['sigproject/create'], ['class' => 'btn btn-success']) ?>

     <?= Html::a('Hapus PT', ['sigproject/index'], ['class' => 'btn btn-danger']) ?>
 </p>
    <?php $form = ActiveForm::begin(); ?>
<p><i>* period yang baru harus sesudah period gaji terakhir</i></p>
<?php
echo 'start period';
echo Datepicker::widget([
 'name'=>'start_period',
     'dateFormat' => 'yyyy-MM-dd',
      //'options' => ['class' => 'form-control','style' => 'z-index:99;'],
]);
echo '<br/>finish period';
echo Datepicker::widget([
 'name'=>'finish_period',
     'dateFormat' => 'yyyy-MM-dd',
      //'options' => ['class' => 'form-control','maxlength'=>'25','style' => 'z-index:99;'],
]);
echo '<br/>';

 ?>

    <?= Html::submitButton('Search', ['class' => 'btn btn-primary', 'name' => 'search']) ?>
<?php

                                      $gridcolumns = [
                                      [
                                       'label' => 'NIK ',
                                       //'attribute' => 'org_id',
                                          //'headerOptions' =>['style' => 'z-index:50;',
                                       'rowspan' => 2,
                                         ],
                                          'contentOptions' =>['class' => 'table_class',
                                         // 'style' => 'z-index:99;'
                                         ],
                                      'value'=>function  ($data,$key,$index,$widget) {
                                      //$employee = Employee::find()->andWhere(['id'=> $data['id']])->One();
                                      //return $employee->org_id;
                                      return $key;

                                      },
                                      ],

                                      [
                                       'label' => 'Nama ',
                                       //'attribute' => 'org_id',
                                          //'headerOptions' =>['style' => 'z-index:50;',
                                            'rowspan' => 2,
                                         ],
                                          'contentOptions' =>['class' => 'table_class',
                                         // 'style' => 'z-index:99;'
                                         ],
                                       'value'=>function  ($data,$key,$index,$widget) {
                                          $employee = Employee::find()->andWhere(['id'=> $key])->One();
                                                 if(isset($employee->employeeProfiles->first_name)) {
                                                  $fn = $employee->employeeProfiles->first_name;
                                                 } else {
                                                  $fn = '';
                                                 }

                                                 if(isset($employee->employeeProfiles->last_name)) {
                                                  $ln = $employee->employeeProfiles->last_name;
                                                 } else {
                                                  $ln = '';
                                                 }
                                              return $fn . ' ' .  $ln  ;
                                          },
                                      ],

                                      ];


                                        $projects = SigProject::find()->andWhere(['status'=> 'active'])->All();

                                        $columnindex = 0;
                                        $headercolumns=[];
                                         foreach ($projects as $project){

                                         $projectcolumn = [
                                          'name'=>$project->name,
                                          'start'=>$columnindex + 3, //indeks kolom 3
                                          'end'=>$columnindex + 5, //indeks kolom 4
                                         ];
                                          array_push($headercolumns, $projectcolumn);
                                          $columnindex = $columnindex + 3;

                                         }



                                         foreach ($projects as $index => $project)
                                         {
                                          $gajipokokcolumns =
                                                 [
                                     'label' => 'Gaji pokok ',
                                                  'attribute' => 'gaji_pokok',
                                                 'header' => '<div style="
                                                  margin:0 -10px 0 -10px;
                                                  "></div><div>Gaji pokok</div>',
                                                  'headerOptions' => [
                                                   'rowspan' => 2,
                                                  ],
                                                  'footerOptions'=>[
                                                   'id' => 'gaji_pokok_' . $project->id,
                                                   'value' => '0',
                                                   //function ($data,$key,$index,$widget) {return 'gaji_pokok-' .$widget->options['value'];},
                                         ],

                                                     'pageSummary'=>true,
                                                     'pageSummaryFunc' => GridView::F_SUM,
                                                     'options'=>[
                                                      'value' => $project->id,
                                                     ],
                                                     'contentOptions' =>['class' => 'table_class'],
                                                     'format' => 'raw',
                                                  //   'value' => $form->field($project, "[$index]gaji_pokok")->label(false),

                                                 'value'=>function ($data,$key,$index,$widget) {
                                                 //  return $form->field($project, "[$index]gaji_pokok")->label(false);
                                              //   return '<input type="text" class="gaji_pokok inputgaji col' .$widget->options['value'].' row'.$index.'" value="0" name="gaji_pokok-'.$data['id'].'-' . $widget->options['value'].'-'. $index. '">';
                                          return '<input type="text" class="gaji_pokok inputgaji col' .$widget->options['value'].' row'.$index.'" value="0" name="Siggaji['.$widget->options['value'].']['. $data['id']. '][gaji_pokok]" id="siggaji-'.$widget->options['value'] . '-'.$data['id'].'-gaji_pokok">';
                                                 },
                                                ];

                                           $tunjanganjabatancolumns =
                                           [
                                            //'label' => 'Tunjangan Jabatan',
                                              'header' => '<div style="

                                              ">' . $project->name . '</div><div>Tunjangan Jabatan</div>',
                                            'attribute' => 'tunjangan_jabatan',
                                            'footerOptions'=>[
                                             'id' => 'tunjangan_jabatan_' . $project->id,
                                             'value' => '0',
                                         ],
                                               'pageSummary'=>true,
                                               'options'=>[
                                                'value' => $project->id,
                                               ],
                                               'contentOptions' =>['class' => 'table_class'],
                                               'format' => 'raw',
                                           'value'=>function ($data,$key,$index,$widget) {
                                           return '<input type="text" class="tunjangan_jabatan inputgaji col' .$widget->options['value']. ' row'.$index.'" value="0" name="tunjangan_jabatan-'.$data['id'].'-' . $widget->options['value'].'-'. $index.'">';

                                           },
                                          ];

                                         $pendapataninterncolumns =   [
                                          'header' => '<div style="
                                              margin:0 -10px 0 -10px;
                                          ">Pendapatan internal</div>',
                                            'attribute' => 'pendapatan_intern',
                                            'footerOptions'=>[
                                             'id' => 'pendapatan_intern_' . $project->id,
                                             'value' => '0',
                                         ],
                                               'pageSummary'=>true,
                                               'options'=>[
                                                'value' => $project->id,
                                               ],
                                               'contentOptions' =>['class' => 'table_class'],
                                               'format' => 'raw',
                                               'value'=>function ($data,$key,$index,$widget) {
                                               return '<input type="text" class="pendapatan_intern inputgaji col' .$widget->options['value']. '" value="0" name="pendapatan_intern-'.$data['id'].'-' . $widget->options['value'].'-'. $index.'">';

                                               },
                                          ];


                                          //array_push($gridcolumns, $gajipokokcolumns, $tunjanganjabatancolumns, $pendapataninterncolumns);
                                           array_push($gridcolumns, $gajipokokcolumns);
                                         }



                                    $totalcolumns =  [
                                      'header' => 'Total',
                                           'format' => 'raw',
                                               'headerOptions' =>['style' => 'z-index:50;'],
                                      'options'=>[
                                       'value' => $project->id,
                                      ],
                                         //'contentOptions' =>['class' => 'table_class'],
                                       'value'=>function($data, $key,$index, $widget) {
                                        return '<div class="totalgaji" style="width:150px;" id="total-pendapatan-'. $index.'">Rp. </div>';

                                         },
                                     ];

                                       array_push($gridcolumns, $totalcolumns);


echo GridView::widget([
	'id'=>'user-grid',
	'summary'=>'Total {count} data',
 'filterPosition' => GridView::FILTER_POS_BODY,
 //'responsive' => true,
 'options' => [

   //'style'=>'width: 1800px; margin-left: -300px;',

  'id' => 'renola',
 ],
 'footerRowOptions' => [

    'style' => 'background-color: yellow; z-index:99;;'
 ],
 'tableOptions' => [
  'id' => 'fixTable',
 ],
  'headerRowOptions' => [
  ],
  'showFooter' => true,
  'showHeader' => true,
 'containerOptions' => [
  'style' => 'overflow: auto; height:500px;',
 ],
 'hover'=> true,
  'floatHeaderOptions'=>[
   'debug' => true,
  ],

 	'dataProvider'=>$dataProvider,
	'columns'=>$gridcolumns,


]);




















$this->registerJs(
"

$(document).ready(function() {
     $('#fixTable').tableHeadFixer({'left' : 2,'right' : 1, 'foot' : true});

$('.inputgaji').change(function(){
 var res = $(this).attr('name');
 var res2 = res.split('-');


var tot = 0;

var col_tot = 0;

$('.row' + res2[3]).each(function(){
tot = parseInt(tot) + parseInt($(this).val());
//alert($(this).val());

 // 3 - row index
 // 2 - project index
 // 1 - employee id
 // 0 - gajipokok/..
});

$('.' + res2[0] + '.col' + res2[2]).each(function(){
col_tot = parseInt(col_tot) + parseInt($(this).val());
//alert($(this).val());

 // 3 - row index
 // 2 - project index
 // 1 - employee id
 // 0 - gajipokok/..
});

$('#' + res2[0] + '_' + res2[2]).html(col_tot);
//alert('#' + res2[0] + '_' + res2[2]);
$('#total-pendapatan-' + res2[3]).html('Rp. ' + tot);
});

});

"
, View::POS_END, 'my-options');


?>

<div class="form-group">
    <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create']) ?>
</div>

    <?php ActiveForm::end(); ?>

</div>
