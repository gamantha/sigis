<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\grid\EditableColumnAction;
use common\Headergrouping;
use app\modules\sig\models\SigProject;
use app\modules\sig\models\Siggaji;
use app\modules\sig\models\SiggajiSearch;
use app\modules\organization\models\Employee;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Siggaji */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="siggaji-form">

    
    
 <p>
     <?= Html::a('Tambah PT', ['sigproject/create'], ['class' => 'btn btn-success']) ?>

     <?= Html::a('Hapus PT', ['create'], ['class' => 'btn btn-danger']) ?>
 </p>
    <?php $form = ActiveForm::begin(); ?>





<?php
$this->title = Yii::t('app', 'Gaji Bulanan');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Personalia'), 'url' => ['default/ketentuanpersonalia']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji dan Upah'), 'url' => ['default/gajidanupah']];
$this->params['breadcrumbs'][] = $this->title;

$gridcolumns = [
//'id',
'org_id',


[
'header' => 'Nama ',
   //'contentOptions' =>['class' => 'table_class'],
 'value'=>function($data) {
return ($data->employeeProfiles->first_name .' '. $data->employeeProfiles->last_name);
   },
],
//'department_role_id',
//'status',
];

$gridcolumns2 = [
'id',
[
'header' => 'Nama ',
   //'contentOptions' =>['class' => 'table_class'],
 'value'=>function($data) {
return ($data->id .' '. $data->id);
   },
],

];

$projects = SigProject::find()->All();

$columnindex = 0;
$headercolumns=[];
foreach ($projects as $project){

$projectcolumn = [
 'name'=>$project->name,
 'start'=>$columnindex + 2, //indeks kolom 3
 'end'=>$columnindex + 4, //indeks kolom 4
];
 array_push($headercolumns, $projectcolumn);
 $columnindex = $columnindex + 3;

}

foreach ($projects as $project)
{


 $gajipokokcolumns =         [

         'class'=>'kartik\grid\EditableColumn',


         'attribute'=>'org_id',
         'header' => 'Gaji Pokok ' . $project->name,
       //   'refreshGrid' => true,
         'options'=>[
          'value' => $project->id,
         ],


         'editableOptions'=>function($model, $key, $index, $widget){return [


             'inputType'=>kartik\editable\Editable::INPUT_SPIN,

            // 'formOptions'=>['action' => ['/sig/siggaji/editbook']], // point to the new action


         ];},

         'hAlign'=>'right',
         'vAlign'=>'middle',
         'width'=>'100px',
         'pageSummary'=>true
        ];











 array_push($gridcolumns, $gajipokokcolumns);
  //array_push($gridcolumns, $gajipokokcolumns);
}





//echo \common\components\Headergrouping::widget([
 echo \kartik\grid\GridView::widget([
	'id'=>'user-grid',
	//'summary'=>'Total {count} data',
 //'responsive' => true,
 //'hover'=> true,

  //'resizableColumns'=>true,
 //'mergeHeaders'=>$headercolumns,
 'showPageSummary' => true,
	'dataProvider'=>$employeedataProvider,
// 	'dataProvider'=>$dataProvider,
	'columns'=>$gridcolumns,

]);


?>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
