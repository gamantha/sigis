<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\Datepicker;
//use yii\grid\GridView;
use kartik\grid\GridView;
use common\Headergrouping;
use app\modules\sig\models\SigProject;
use app\modules\sig\models\Siggaji;
use app\modules\sig\models\SiggajiSearch;
use app\modules\organization\models\Employee;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Siggaji */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="siggaji-form">

 <p>
     <?= Html::a('Tambah PT', ['sigproject/create'], ['class' => 'btn btn-success']) ?>

     <?= Html::a('Hapus PT', ['create'], ['class' => 'btn btn-danger']) ?>
 </p>
    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'start_period')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','maxlength' => '255',],
    ])->label('Awal Periode') ?>
    <?= $form->field($model, 'end_period')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','maxlength' => '255',],
    ])->label('Akhir Periode') ?>


<?php


$gridcolumns = [
//'id',
'org_id',


[
'header' => 'Nama ',
   //'contentOptions' =>['class' => 'table_class'],
 'value'=>function($data) {
return ($data->employeeProfiles->first_name .' '. $data->employeeProfiles->last_name);
   },
],
//'department_role_id',
//'status',
];


$projects = SigProject::find()->All();

$columnindex = 0;
$headercolumns=[];
foreach ($projects as $project){

$projectcolumn = [
 'name'=>$project->name,
 'start'=>$columnindex + 2, //indeks kolom 3
 'end'=>$columnindex + 4, //indeks kolom 4
];
 array_push($headercolumns, $projectcolumn);
 $columnindex = $columnindex + 3;

}

foreach ($projects as $project)
{


 $gajipokokcolumns =         [
         'class'=>'kartik\grid\EditableColumn',

         //'attribute'=>'id',
         'header' => 'Gaji Pokok ' . $project->name,
          'refreshGrid' => true,
         'options'=>[
          'value' => $project->id,
         ],
          //'data' => 'fa']
      //   'format' => 'raw',
         'value' => function ($employeemodel,$key,$index,$widget) {
          //return $widget->width;
          $proj_id = $widget->options['value'];
$gaji = Siggaji::find()->andWhere(['employee_id'=>$employeemodel->id, 'sig_project_id'=>$proj_id])->One();
if (isset($gaji))
{
$gajipokok = $gaji->gaji_pokok;
} else {
 $gajipokok = 0;
}
return $gajipokok;

         },

         'editableOptions'=>function($model, $key, $index, $widget){return [

           'name' => 'gajipokok_' . $widget->options['value'] . '_' . $key,
             'header'=>'Masukkan gaji pokok ' . $key,
             'inputType'=>kartik\editable\Editable::INPUT_SPIN,
             'value' => (Siggaji::find()->andWhere(['employee_id'=>$key, 'sig_project_id' =>$widget->options['value']])->One()) ?  Siggaji::find()->andWhere(['employee_id'=>$key, 'sig_project_id' => $widget->options['value']])->One()->gaji_pokok : '0',
            'model' => Siggaji::find()->andWhere(['employee_id'=>$key, 'sig_project_id' => $widget->options['value']]),
             'attribute' => 'gaji_pokok',


         ];},

         'hAlign'=>'right',
         'vAlign'=>'middle',
         'width'=>'100px',
      //   'content' => function($data, $key, $index, $column){
       //   return 'darqewr';
       //  },
         'pageSummary'=>true
        ];











  $tunjanganjabatancolumns =        [
         'class'=>'kartik\grid\EditableColumn',

         //'attribute'=>'id',
         'header' => 'Tunjangan Jabatan',
         'content' => function ($model, $key, $index, $column) {
    return $key;
},

         'editableOptions'=>[
           'name' => 'tunjanganjabatan',
             'header'=>'Masukkan Tunjangan Jabatan',
             'inputType'=>kartik\editable\Editable::INPUT_SPIN,
         ],
         'hAlign'=>'right',
         'vAlign'=>'middle',
         'width'=>'100px',
         'pageSummary'=>true
        ];

   $pendapataninterncolumns =       [
         'class'=>'kartik\grid\EditableColumn',

         //'attribute'=>'id',
         'header' => 'Pendapatan Internal',
         'content' => function ($model, $key, $index, $column) {
    return $index;
},
         'editableOptions'=>[
           'name' => 'pendapataninternal',
             'header'=>'Masukkan pendapatan internal',
             'inputType'=>kartik\editable\Editable::INPUT_SPIN,
         ],
         'hAlign'=>'right',
         'vAlign'=>'middle',
         'width'=>'100px',
         'pageSummary'=>true
        ];

 array_push($gridcolumns, $gajipokokcolumns, $tunjanganjabatancolumns, $pendapataninterncolumns);
}



$totalcolumns =  [
  'header' => 'Total',
     //'contentOptions' =>['class' => 'table_class'],
   'value'=>function($data) {
return ('00');
     },
 ];

array_push($gridcolumns, $totalcolumns);



echo \common\components\Headergrouping::widget([
//echo GridView::widget([
	'id'=>'user-grid',
	'summary'=>'Total {count} data',
 'responsive' => true,
 'hover'=> true,

  'resizableColumns'=>true,
 'mergeHeaders'=>$headercolumns,
 'showPageSummary' => true,
	//'dataProvider'=>$employeedataProvider,
 	'dataProvider'=>$dataProvider,
	'columns'=>$gridcolumns,

]);
?>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
