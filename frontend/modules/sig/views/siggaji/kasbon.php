<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\bootstrap\Nav;
use kartik\nav\NavX;
use kartik\tabs\TabsX;
use yii\widgets\ActiveForm;
use app\modules\organization\models\Employee;
use app\modules\sig\models\SigPengembalianKasbon;
use yii\jui\Datepicker;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\sig\models\SiggajiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kasbon Karyawan';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji dan Upah'), 'url' => ['default/gajidanupah']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/gajibulanan']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Perhitungan'), 'url' => ['default/perhitungan']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="kasbon-form">
    <h1><?= Html::encode($this->title) ?></h1>

<?php

$this->registerJsFile(Url::base() . '/js/jquery.monthpicker.js' , ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJs(
"
$(document).ready(function() {
    //$('.yearpicker').monthpicker();
     //   $('#sigpengembaliankasbon-0-jatuh_tempo').monthpicker();
     //   $('#sigpengembaliankasbon-1-jatuh_tempo').monthpicker();
       //         $('#sigpengembaliankasbon-2-jatuh_tempo').monthpicker();
         //       $('#sigpengembaliankasbon-3-jatuh_tempo').monthpicker();
           //     $('#sigpengembaliankasbon-4-jatuh_tempo').monthpicker();
             //           $('#sigpengembaliankasbon-5-jatuh_tempo').monthpicker();
});
"
, View::POS_END, 'my-options');

$form = ActiveForm::begin();
$employeelist = Employee::find()->andWhere(['status' => 'active'])->asArray()->All();

  $employeearray=ArrayHelper::map($employeelist,'id','org_id');



    echo $form->field($model, 'employee_id')
->dropDownList($employeearray, ['id' => 'org_id', 'class'=>'input-large form-control','prompt'=>'Select...', ])
    ->label('NIK');


//echo Html::buttonInput('Search',['class' => 'btn btn-primary', 'id' => 'search_nik']);
   echo Html::submitButton('search', ['class' => 'btn btn-primary', 'name'=>'search_nik']);
echo '<br/><br/>';
    // echo $this->render('_search', ['model' => $searchModel]);




if ((isset($_POST['SigKasbon']['employee_id']))) //if search is clicked and with nik value
{

 if(isset($_POST['search_nik']) || isset($_POST['save'])) {
 //$empl = Employee::find()->andWhere(['id' => $_REQUEST['SigKasbon']['employee_id']])->One();
  $nama = $emp_model->employeeProfiles->first_name . ' ' . $emp_model->employeeProfiles->last_name;
  $date1 = new DateTime($emp_model->employments->first_day);
  $date2 = new DateTime(date("Y-m-d H:i:s"));
  $interval = $date2->diff($date1);
  //echo "difference " . $interval->y . " years, " . $interval->m." months, ".$interval->d." days ";
  //echo "difference " . $interval->days . " days ";
 $content1 =
     '<div class="row"><div class="col-sm-2">' . Html::label('Nama', 'nama') . '</div><div class="col-sm-1"> : </div><div class="col-sm-2">' . Html::label($nama, 'nama')  . '</div></div><br/>' .
     '<div class="row"><div class="col-sm-2">' . Html::label('Department', 'nama') . '</div><div class="col-sm-1"> : </div><div class="col-sm-2">' . Html::label($emp_model->departmentRole->department->department_name, 'department')    . '</div></div><br/>' .
     '<div class="row"><div class="col-sm-2">' . Html::label('Jabatan', 'nama') . '</div><div class="col-sm-1"> : </div><div class="col-sm-2">' . Html::label($emp_model->departmentRole->department_role_name, 'department')    . '</div></div><br/>' .
     '<div class="row"><div class="col-sm-2">' . Html::label('Masa Kerja', 'nama') . '</div><div class="col-sm-1"> : </div><div class="col-sm-2">'  . $interval->m . ' bulan '. $interval->days  . ' hari</div></div><br/>' .
     '<div class="row"><div class="col-sm-2">' . Html::label('Plafond kasbon', 'nama') . '</div><div class="col-sm-1"> : </div><div class="col-sm-2">' . '-- no golongan --'  . '</div></div><br/>'.
          '<div class="row"><div class="col-sm-2">' . Html::label('Pengajuan Kasbon', 'nama') . '</div><div class="col-sm-1"> : </div><div class="col-sm-2">' . $form->field($model, 'pengajuankasbon')->label(false)  . '</div>ada warning apabila diatas plafond</div><br/>'.
               '<div class="row"><div class="col-sm-2">' . Html::label('Besar Potongan / Bulan', 'nama') . '</div><div class="col-sm-1"> : </div><div class="col-sm-2">' .$form->field($model, 'potonganperbulan')->label(false)   . '</div></div><br/>'.
                              '<div class="row"><div class="col-sm-2">' . Html::label('Keterangan', 'nama') . '</div><div class="col-sm-1"> : </div><div class="col-sm-8">' .$form->field($model, 'keterangan')->label(false)   . '</div></div><br/>';


$content3 = '';
$terbayar = '';
$sisapinjaman =$model->pengajuankasbon;
if (sizeof($pengembalians) > 0) {
foreach ($pengembalians as $index => $pengembalian) {
 $content3 = $content3 . '<div class="row">';
   $content3 = $content3 .  '<div class="col-sm-1">';
  $content3 = $content3 .  ($index + 1);
  $content3 = $content3 .  '</div>';
  $content3 = $content3 .  '<div class="col-sm-2">';
           //   $content3 = $content3 .  $form->field($pengembalian, "[$index]jatuh_tempo")->label(false);
           $content3 = $content3 . Html::label($pengembalian->jatuh_tempo, 'department');
              $content3 = $content3 .  '</div><div class="col-sm-2">';
    //$content3 = $content3 .  $form->field($pengembalian, "[$index]besaran")->label(false);

     if (date('Y-m-d') < $pengembalian->jatuh_tempo) {
      $sisapinjaman = $sisapinjaman - $pengembalian->besaran;
      $terbayar = '';
     } else {
    $terbayar = '(terbayar)';
     }
          $content3 = $content3 . Html::label($pengembalian->besaran, 'department') . $terbayar;
     $content3 = $content3 .  '</div>';
     $content3 = $content3 .  '</div>';

}
}

$content3 = $content3 . '<div class="row">';
$content3 = $content3 .  '<div class="col-sm-1">';

$content3 = $content3 .  '</div>';
  $content3 = $content3 .  '<div class="col-sm-2">SISA PINJAMAN';

  $content3 = $content3 .  '</div>';
  $content3 = $content3 .  '<div class="col-sm-2">';
    $content3 = $content3 .  $sisapinjaman;
  $content3 = $content3 .  '</div>';
  $content3 = $content3 .  '</div>';


$content4 = '';
 $content4 = $content4 . 'tanggal '.Html::input('text','jatuh_tempo_baru','');
  $content4 = $content4 .'nilai ' .Html::input('text','besaran_baru','');
$content4 = $content4 .  Html::buttonInput('Pembayaran',['class' => 'btn btn-warning', 'id' => 'authorize_button']);
$content4 = $content4 .  Html::submitButton('Pembayaran', ['class' => 'btn btn-success', 'name'=>'bayar']);

        $items = [
            [
                'label'=>' Data Karyawan',
                'content'=>$content1,
                'active'=>true
            ],
            [
                'label'=>' Pengembalian',
                'content'=>$content3,
                //'content' => $content4,
            ],
        ];

        echo TabsX::widget([
            'items'=>$items,
            'position'=>TabsX::POS_ABOVE,
            'encodeLabels'=>false
        ]);


}
}

    ?>
    <div class="form-group">
     <?php
if ((isset($_POST['SigKasbon']['employee_id'])) && isset($_POST['search_nik']))
echo Html::submitButton('Save', ['class' => 'btn btn-primary', 'name'=>'save']);
      ?>
    </div>
        <?php ActiveForm::end(); ?>

</div>
