

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\Datepicker;
//use yii\grid\GridView;
use kartik\grid\GridView;
use common\Headergrouping;
use app\modules\sig\models\SigProject;
use app\modules\sig\models\Siggaji;
use app\modules\sig\models\SiggajiSearch;
use app\modules\organization\models\Employee;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\View;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Siggaji */
/* @var $form yii\widgets\ActiveForm */

//$this->registerJsFile(Url::base() . '/js/jquery.floatThead.js' , ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Url::base() . '/js/tableHeadFixer.js' , ['depends' => [\yii\web\JqueryAsset::className()]]);


?>

<div class="siggaji-form">

 <p>
     <?= Html::a('Tambah PT', ['sigproject/create'], ['class' => 'btn btn-success']) ?>

     <?= Html::a('Hapus PT', ['sigproject/index'], ['class' => 'btn btn-danger']) ?>
 </p>
    <?php $form = ActiveForm::begin(); ?>
<p><i>* period yang baru harus sesudah period gaji terakhir</i></p>
<?php
echo 'start period';
echo Datepicker::widget([
 'name'=>'start_period',
     'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','style' => 'z-index:99;'],
]);
echo '<br/>finish period';
echo Datepicker::widget([
 'name'=>'finish_period',
     'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','maxlength'=>'25','style' => 'z-index:99;'],
]);
echo '<br/>';
echo '<br/>';
echo '<br/>';
echo '<br/>';
echo '<br/>';
echo '<br/>';
echo '<br/>';
echo '<br/>';
echo '<br/>';
echo '<br/>';
echo '<br/>';
 ?>


<?php
$gridcolumns = [
 /*[
  'label' => 'ID ',
  //'attribute' => 'org_id',
     'headerOptions' =>['style' => 'z-index:50;',
 'rowspan' => 2,
    ],
     'contentOptions' =>['class' => 'table_class',
    // 'style' => 'z-index:99;'
    ],
 'value'=>function ($data) {
 $employee = Employee::find()->andWhere(['id'=> $data['id']])->One();
 return $employee->id;
 },
 ],
 */
[
 'label' => 'NIK ',
 //'attribute' => 'org_id',
    'headerOptions' =>['style' => 'z-index:50;',
 'rowspan' => 2,
   ],
    'contentOptions' =>['class' => 'table_class',
   // 'style' => 'z-index:99;'
   ],
'value'=>function ($data) {
$employee = Employee::find()->andWhere(['id'=> $data['id']])->One();
return $employee->org_id;
},
],

[
 'label' => 'Nama ',
 //'attribute' => 'org_id',
    'headerOptions' =>['style' => 'z-index:50;',
 'rowspan' => 2,
   ],
    'contentOptions' =>['class' => 'table_class',
   // 'style' => 'z-index:99;'
   ],
'value'=>function ($data) {
$employee = Employee::find()->andWhere(['id'=> $data['id']])->One();
//return Html::input('text',$data);
//return print_r($data);

if(isset($employee->employeeProfiles->first_name)) {
 $fn = $employee->employeeProfiles->first_name;
} else {
 $fn = '';
}

if(isset($employee->employeeProfiles->last_name)) {
 $ln = $employee->employeeProfiles->last_name;
} else {
 $ln = '';
}
//return isset($employee->employeeProfiles->first_name) && ($employee->employeeProfiles->last_name) ? ($employee->employeeProfiles->first_name) && ($employee->employeeProfiles->last_name):"";
    return $fn . ' ' .  $ln  ;
},
],

];


$projects = SigProject::find()->andWhere(['status'=> 'active'])->All();

$columnindex = 0;
$headercolumns=[];
foreach ($projects as $project){

$projectcolumn = [
 'name'=>$project->name,
 'start'=>$columnindex + 3, //indeks kolom 3
 'end'=>$columnindex + 5, //indeks kolom 4
];
 array_push($headercolumns, $projectcolumn);
 $columnindex = $columnindex + 3;

}

foreach ($projects as $project)
{


 $gajipokokcolumns =

        [
         'label' => 'Gaji pokok ',
         'attribute' => 'gaji_pokok',

         'header' => '<div style="
         margin:0 -10px 0 -10px;


         "></div><div>Gaji pokok</div>',
         'headerOptions' => [
          'rowspan' => 2,
         ],
         'footerOptions'=>[
          'id' => 'gaji_pokok_' . $project->id,
          'value' => '0',

          //function ($data,$key,$index,$widget) {return 'gaji_pokok-' .$widget->options['value'];},
],

            'pageSummary'=>true,
            'pageSummaryFunc' => GridView::F_SUM,
            'options'=>[
             'value' => $project->id,
            ],
            'contentOptions' =>['class' => 'table_class'],
            'format' => 'raw',

        'value'=>function ($data,$key,$index,$widget) {
        return '<input type="text" class="gaji_pokok inputgaji col' .$widget->options['value'].' row'.$index.'" value="0" name="gaji_pokok-'.$data['id'].'-' . $widget->options['value'].'-'. $index. '">';

        },
       ];

  $tunjanganjabatancolumns =
  [
   //'label' => 'Tunjangan Jabatan',
     'header' => '<div style="

     ">' . $project->name . '</div><div>Tunjangan Jabatan</div>',
   'attribute' => 'tunjangan_jabatan',
   'footerOptions'=>[
    'id' => 'tunjangan_jabatan_' . $project->id,
    'value' => '0',
],
      'pageSummary'=>true,
      'options'=>[
       'value' => $project->id,
      ],
      'contentOptions' =>['class' => 'table_class'],
      'format' => 'raw',
  'value'=>function ($data,$key,$index,$widget) {
  return '<input type="text" class="tunjangan_jabatan inputgaji col' .$widget->options['value']. ' row'.$index.'" value="0" name="tunjangan_jabatan-'.$data['id'].'-' . $widget->options['value'].'-'. $index.'">';

  },
 ];

$pendapataninterncolumns =   [
 'header' => '<div style="
     margin:0 -10px 0 -10px;
 ">Pendapatan internal</div>',
   'attribute' => 'pendapatan_intern',
   'footerOptions'=>[
    'id' => 'pendapatan_intern_' . $project->id,
    'value' => '0',
],
      'pageSummary'=>true,
      'options'=>[
       'value' => $project->id,
      ],
      'contentOptions' =>['class' => 'table_class'],
      'format' => 'raw',
      'value'=>function ($data,$key,$index,$widget) {
      return '<input type="text" class="pendapatan_intern inputgaji col' .$widget->options['value']. '" value="0" name="pendapatan_intern-'.$data['id'].'-' . $widget->options['value'].'-'. $index.'">';

      },
 ];


 array_push($gridcolumns, $gajipokokcolumns, $tunjanganjabatancolumns, $pendapataninterncolumns);
}



$totalcolumns =  [
  'header' => 'Total',
       'format' => 'raw',
           'headerOptions' =>['style' => 'z-index:50;'],
  'options'=>[
   'value' => $project->id,
  ],
     //'contentOptions' =>['class' => 'table_class'],
   'value'=>function($data, $key,$index, $widget) {
    return '<div class="totalgaji" style="width:150px;" id="total-pendapatan-'. $index.'">Rp. </div>';

     },
 ];

array_push($gridcolumns, $totalcolumns);

?>

<?php

//echo \common\components\Headergrouping::widget([
echo GridView::widget([
	'id'=>'user-grid',
 //'bootstrap' => 'false',
	'summary'=>'Total {count} data',
 'filterPosition' => GridView::FILTER_POS_BODY,
 //'responsive' => true,
 'options' => [
  'id' => 'renola',
    //'style' => 'height:200px;',
 // 'class' => "table table-striped table-bordered",
  //'style' => '',
 ],
 'footerRowOptions' => [

    'style' => 'background-color: yellow; z-index:99;;'
 ],
 'tableOptions' => [
  //'name'=>'tasdadsa',
  'id' => 'fixTable',
  //'class' => "table table-striped table-bordered",
   // 'style' => 'overflow-x:scroll; overflow-y:scroll',
  //'style' => 'max-height:200px;',
  //'style' => 'overflow: auto;'
 ],
  'headerRowOptions' => [
//'colspan' => '3',
  ],
  'showFooter' => true,
  'showHeader' => true,
 'containerOptions' => [
  //'name'=>'tasdadsa',
  //'id' => 'fixTable',
  //'class' => "table table-striped table-bordered",
   // 'style' => 'overflow-x:scroll; overflow-y:scroll',

  'style' => 'overflow: auto; height:500px;',
 ],
 'hover'=> true,
 //'floatHeader' => true,
  'floatHeaderOptions'=>[
   'debug' => true,
  // 'scrollingTop'=>'0'

  ],
  //'resizableColumns'=>true,
 //'mergeHeaders'=>$headercolumns,
 //'showPageSummary' => true,
	//'dataProvider'=>$employeedataProvider,
 	'dataProvider'=>$dataProvider,
	'columns'=>$gridcolumns,

]);


$this->registerJs(
"

$(document).ready(function() {
     $('#fixTable').tableHeadFixer({'left' : 2,'right' : 1, 'foot' : true});






$('.inputgaji').change(function(){
 var res = $(this).attr('name');
 var res2 = res.split('-');


var tot = 0;

var col_tot = 0;

$('.row' + res2[3]).each(function(){
tot = parseInt(tot) + parseInt($(this).val());
//alert($(this).val());

 // 3 - row index
 // 2 - project index
 // 1 - employee id
 // 0 - gajipokok/..
});

$('.' + res2[0] + '.col' + res2[2]).each(function(){
col_tot = parseInt(col_tot) + parseInt($(this).val());
//alert($(this).val());

 // 3 - row index
 // 2 - project index
 // 1 - employee id
 // 0 - gajipokok/..
});

$('#' + res2[0] + '_' + res2[2]).html(col_tot);
//alert('#' + res2[0] + '_' + res2[2]);
$('#total-pendapatan-' + res2[3]).html('Rp. ' + tot);
});

});

"
, View::POS_END, 'my-options');


?>

<div class="form-group">
    <?= Html::submitButton('Create', ['class' => 'btn btn-primary']) ?>
</div>

    <?php ActiveForm::end(); ?>

</div>
