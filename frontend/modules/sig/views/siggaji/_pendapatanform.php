

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
//use yii\grid\GridView;
use kartik\grid\GridView;
use common\Headergrouping;
use app\modules\sig\models\SigProject;
use app\modules\sig\models\Siggaji;
use app\modules\hardware\models\Schedule;
use app\modules\sig\models\SiggajiSearch;
use app\modules\sig\models\Adjustment;
use app\modules\organization\models\Employee;
use app\modules\organization\models\Useremployee;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\View;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Siggaji */
/* @var $form yii\widgets\ActiveForm */

//$this->registerJsFile(Url::base() . '/js/jquery.floatThead.js' , ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Url::base() . '/js/tableHeadFixer.js' , ['depends' => [\yii\web\JqueryAsset::className()]]);


?>
<!--div class="well">
1. jumlah hari masuk<br/>
2. jumlah jam masuk<br/>


</div-->
   <h1>Pendapatan</h1>

<div class="siggaji-form">


    <?php $form = ActiveForm::begin(); ?>



<?php
echo 'start period';
echo DatePicker::widget([
 'name'=>'start_period',
     'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','style' => 'z-index:99;'],
        'value' => $from,
]);
echo '<br/>finish period';
echo DatePicker::widget([
 'name'=>'finish_period',
     'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','maxlength'=>'25','style' => 'z-index:99;'],
        'value' => $to,
]);
echo '<br/>';
echo '<br/>';

 ?>

 <div class="form-group">
  <?php
if(isset($_POST['print'])) {}else {
                         echo Html::submitButton( Yii::t('app', 'Search'), ['id'=>'search', 'name'=>'search','class' => 'btn btn-primary']);
//echo Html::a('Pdf', 'url', ['class' => 'btn btn-primary','name'=>'Pdf', 'target'=>'_blank'])
                  echo Html::submitButton( Yii::t('app', 'Print'), ['id'=>'print', 'name'=>'print','target'=>'_blank','class' => 'btn btn-success']);
          echo '<br/>';
         }
                   ?>
 </div>



<?php
$gridcolumns = [
 [
  'label' => 'ID ',
  //'attribute' => 'org_id',
     'headerOptions' =>['style' => 'z-index:50;',
 'rowspan' => 2,
    ],
     'contentOptions' =>['class' => 'table_class',
    // 'style' => 'z-index:99;'
    ],
 'value'=>function ($data) {
 //$employee = Employee::find()->andWhere(['id'=> $data['id']])->One();
 return '1';
 },
 ],
];


$projects = SigProject::find()->andWhere(['status'=> 'active'])->All();

$columnindex = 0;
$headercolumns=[];
foreach ($projects as $project){

$projectcolumn = [
 'name'=>$project->name,
 'start'=>$columnindex + 3, //indeks kolom 3
 'end'=>$columnindex + 5, //indeks kolom 4
];
 array_push($headercolumns, $projectcolumn);
 $columnindex = $columnindex + 3;

}




$totalcolumns =  [
  'header' => 'Total',
       'format' => 'raw',
           'headerOptions' =>['style' => 'z-index:50;'],
  'options'=>[
   'value' => $project->id,
  ],
     //'contentOptions' =>['class' => 'table_class'],
   'value'=>function($data, $key,$index, $widget) {
    return '<div class="totalgaji" style="width:150px;" id="total-pendapatan-'. $index.'">Rp. </div>';

     },
 ];

array_push($gridcolumns, $totalcolumns);

?>

<?php

//echo \common\components\Headergrouping::widget([
echo GridView::widget([
	'id'=>'user-grid',
	'summary'=>'Total {count} data',
 'filterPosition' => GridView::FILTER_POS_BODY,
 //'responsive' => true,
 'options' => [
  'id' => 'renola',

 ],
 'tableOptions' => [
  //'name'=>'tasdadsa',
  'id' => 'fixTable',

 ],
  'headerRowOptions' => [
//'colspan' => '3',
  ],
  //'showFooter' => true,
  'showHeader' => true,
 'containerOptions' => [
  //'name'=>'tasdadsa',
  //'id' => 'fixTable',
  //'class' => "table table-striped table-bordered",
   // 'style' => 'overflow-x:scroll; overflow-y:scroll',

  'style' => 'overflow: auto;',
   //height:500px;',
 ],
 'hover'=> true,
 //'floatHeader' => true,
  'floatHeaderOptions'=>[
   'debug' => true,
  // 'scrollingTop'=>'0'

  ],
  //'resizableColumns'=>true,
 //'mergeHeaders'=>$headercolumns,
 'showPageSummary' => true,
	//'dataProvider'=>$employeedataProvider,
 	'dataProvider'=>$dataProvider,
	//'columns'=>$gridcolumns,
 'columns' => [

[
 'label' => 'NIK ',
'value'=>function ($data) {
return $data->employee->org_id;
},
],

[
 'label' => 'Nama ',

'value'=>function ($data) {
$employee = Employee::findOne($data->employee_id);

if(isset($employee->employeeProfiles->first_name)) {
 $fn = $employee->employeeProfiles->first_name;
} else {
 $fn = '';
}

if(isset($employee->employeeProfiles->last_name)) {
 $ln = $employee->employeeProfiles->last_name;
} else {
 $ln = '';
}

return $fn . ' ' . $ln;
 //return $data->id;
},
],
[
 'label' => 'Golongan',
  //'pageSummary'=>true,
  'value'=>function  ($data,$key,$index,$widget) {
   $gol = $data->employee->getGolongan($data->employee->id);
    return $gol->id;
  },
],
[
 'label' => 'Gaji Pokok',
 'attribute' => 'gaji_pokok',
  'pageSummary'=>true,
],

[
 'label' => 'Tunjangan Jabatan',
 'attribute' => 'tunjangan_jabatan',
  'pageSummary'=>true,

],

[
 'label' => 'Pendapatan internal',
 'attribute' => 'pendapatan_intern',
  'pageSummary'=>true,

],
[
 'label' => 'Jumlah Hari',
  'pageSummary'=>true,
],
[
 'label' => 'Jumlah Jam',
  'pageSummary'=>true,
],


[
 'label' => 'Makan & Transport',
  'pageSummary'=>true,
  'value'=>function ($data) {
   $adjustment = Adjustment::find()
   ->andWhere(['employee_id' => $data->employee_id])
   ->andWhere(['in', 'start_period', [$_REQUEST['from']]])
   ->andWhere(['in', 'finish_period', [$_REQUEST['to']]])
   ->andWhere(['tipe'=>'makan'])
   ->One();
  // return $data->employee->getGolongan($data->employee->id)->tunjangan_makan_transport;
  return isset($adjustment->value_1) ? ($adjustment->value_1 *  $adjustment->value_2) : 0;
  },
],


[
 'label' => 'Lembur',
  'pageSummary'=>true,
  'value'=>function ($data) {
   $adjustment = Adjustment::find()
   ->andWhere(['employee_id' => $data->employee_id])
   ->andWhere(['in', 'start_period', [$_REQUEST['from']]])
   ->andWhere(['in', 'finish_period', [$_REQUEST['to']]])
   ->andWhere(['tipe'=>'lembur'])
   ->One();
  // return $data->employee->getGolongan($data->employee->id)->tunjangan_makan_transport;
  return isset($adjustment->value_1) ? ($adjustment->value_1 *  $adjustment->value_2) : 0;
  },

],
[
 'label' => 'Luar Kota',
  'pageSummary'=>true,
  'value'=>function ($data) {
   $adjustment = Adjustment::find()
   ->andWhere(['employee_id' => $data->employee_id])
   ->andWhere(['in', 'start_period', [$_REQUEST['from']]])
   ->andWhere(['in', 'finish_period', [$_REQUEST['to']]])
   ->andWhere(['tipe'=>'keluar kota'])
   ->One();
  // return $data->employee->getGolongan($data->employee->id)->tunjangan_makan_transport;
  return isset($adjustment->value_1) ? ($adjustment->value_1 *  $adjustment->value_2) : 0;
  },
],
[
 'label' => 'Premi Hadir',
  'pageSummary'=>true,
],
[
 'label' => 'Bonus Hadir',
  'pageSummary'=>true,
],
[
 'label' => 'Lain-Lain',
  'pageSummary'=>true,
  'value'=>function ($data) {
   $adjustment = Adjustment::find()
   ->andWhere(['employee_id' => $data->employee_id])
   ->andWhere(['in', 'start_period', [$_REQUEST['from']]])
   ->andWhere(['in', 'finish_period', [$_REQUEST['to']]])
   ->andWhere(['tipe'=>'lain-lain'])
   ->One();
  // return $data->employee->getGolongan($data->employee->id)->tunjangan_makan_transport;
  return isset($adjustment->value_1) ? ($adjustment->value_1 *  $adjustment->value_2) : 0;
  },
],
[
 'label' => 'Total pendapatan',
  'pageSummary'=>true,
  'value'=>function ($data) {
   $adjustments = Adjustment::find()
   ->andWhere(['employee_id' => $data->employee_id])
   ->andWhere(['in', 'start_period', [$_REQUEST['from']]])
   ->andWhere(['in', 'finish_period', [$_REQUEST['to']]])
   ->andWhere(['in','tipe',['makan','lembur','lain-lain', 'keluar kota']])
   ->All();
   $total = 0;
   foreach($adjustments as $adjustment){
    $toadd = isset($adjustment->value_1) ? ($adjustment->value_1 *  $adjustment->value_2) : 0;
    $total = $total + $toadd;
   }
  // return $data->employee->getGolongan($data->employee->id)->tunjangan_makan_transport;
  return $total;
  },
],

 ]

]);



?>
 <?php ActiveForm::end(); ?>



</div>
<div><br/>Bandung, <?php echo date("Y/m/d");?>
<br/><br/><br/><br/><br/>
<?php
$useremployee = Useremployee::find()->Where(['user_id' => Yii::$app->user->id])->One();

echo isset($useremployee) ? $useremployee->id : '(no employee data) - ' . Yii::$app->user->identity->username;?>
</div>
