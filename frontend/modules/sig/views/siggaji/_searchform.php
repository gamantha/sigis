

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\Datepicker;
//use yii\grid\GridView;
use kartik\grid\GridView;
use common\Headergrouping;
use app\modules\sig\models\SigProject;
use app\modules\sig\models\Siggaji;
use app\modules\sig\models\SiggajiSearch;
use app\modules\organization\models\Employee;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\View;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Siggaji */
/* @var $form yii\widgets\ActiveForm */

//$this->registerJsFile(Url::base() . '/js/jquery.floatThead.js' , ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Url::base() . '/js/tableHeadFixer.js' , ['depends' => [\yii\web\JqueryAsset::className()]]);


?>

<div class="siggaji-form">

 <p>
     <?= Html::a('Tambah PT', ['sigproject/create'], ['class' => 'btn btn-success']) ?>

     <?= Html::a('Hapus PT', ['sigproject/index'], ['class' => 'btn btn-danger']) ?>
 </p>
    <?php $form = ActiveForm::begin(); ?>
<p><i>* period yang baru harus sesudah period gaji terakhir</i></p>
<?php
echo 'start period';
echo Datepicker::widget([
 'name'=>'start_period',
     'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','style' => 'z-index:99;'],
]);
echo '<br/>finish period';
echo Datepicker::widget([
 'name'=>'finish_period',
     'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','maxlength'=>'25','style' => 'z-index:99;'],
]);
echo '<br/>';

 ?>

    <?= Html::submitButton('Search', ['class' => 'btn btn-primary', 'name' => 'search']) ?>

<?php














$this->registerJs(
"

$(document).ready(function() {
     $('#fixTable').tableHeadFixer({'left' : 2,'right' : 1, 'foot' : true});

$('.inputgaji').change(function(){
 var res = $(this).attr('name');
 var res2 = res.split('-');


var tot = 0;

var col_tot = 0;

$('.row' + res2[3]).each(function(){
tot = parseInt(tot) + parseInt($(this).val());
//alert($(this).val());

 // 3 - row index
 // 2 - project index
 // 1 - employee id
 // 0 - gajipokok/..
});

$('.' + res2[0] + '.col' + res2[2]).each(function(){
col_tot = parseInt(col_tot) + parseInt($(this).val());
//alert($(this).val());

 // 3 - row index
 // 2 - project index
 // 1 - employee id
 // 0 - gajipokok/..
});

$('#' + res2[0] + '_' + res2[2]).html(col_tot);
//alert('#' + res2[0] + '_' + res2[2]);
$('#total-pendapatan-' + res2[3]).html('Rp. ' + tot);
});

});

"
, View::POS_END, 'my-options');


?>

<div class="form-group">
    <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create']) ?>
</div>

    <?php ActiveForm::end(); ?>

</div>
