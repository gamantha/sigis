<?php
use yii\bootstrap\Button;
use yii\helpers\Url;
use yii\widgets\ListView;
use app\modules\sig\models\SigProject;

use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
/*
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\Datepicker;
//use yii\grid\GridView;
use kartik\grid\GridView;
use common\Headergrouping;

use app\modules\sig\models\Siggaji;
use app\modules\sig\models\SiggajiSearch;
use app\modules\organization\models\Employee;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\View;
use yii\helpers\Url;
*/


/* @var $this yii\web\View */


$this->title = Yii::t('app', 'Potongan - Pilih proyek');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Personalia'), 'url' => ['default/ketentuanpersonalia']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji dan Upah'), 'url' => ['default/gajidanupah']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/gajibulanan']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Perhitungan'), 'url' => ['default/perhitungan']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-index">

    <div class="body-content">

        <div class="row">

            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
            <p><h1>Gaji Bulanan</h1></p>
            <?php
                $id = $_REQUEST['id'];
             ?>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['siggaji/kasbon?id=' . $id]); ?>">Kasbon &raquo;</a></p>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['/sig/sigbpjs?id=' . $id]); ?>">BPJS &raquo;</a></p>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['default/pph?id=' . $id]); ?>">PPH &raquo;</a></p>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['/sig/sigrekpro?id=' . $id]); ?>">Rekapitulasi &raquo;</a></p>

            </div>
            <div class="col-lg-4">

            </div>
        </div>

    </div>
</div>
