<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Siggaji */

$this->title = 'Update Siggaji: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji dan Upah'), 'url' => ['default/gajidanupah']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="siggaji-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
