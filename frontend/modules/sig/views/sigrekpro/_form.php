<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\SigBpjs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sig-bpjs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tipe')->dropDownList([ 'bpjs_3' => 'Bpjs 3', 'bpjs_2' => 'Bpjs 2', 'bpjs_1' => 'Bpjs 1', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'value')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
