<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\Datepicker;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\sig\models\SigBpjsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sig Bpjs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sig-bpjs-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sig Bpjs', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    
<?php
echo 'start period';
echo Datepicker::widget([
 'name'=>'start_period',
     'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','style' => 'z-index:99;'],
]);
echo '<br/>finish period';
echo Datepicker::widget([
 'name'=>'finish_period',
     'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','maxlength'=>'25','style' => 'z-index:99;'],
]);
echo '<br/>';
echo '<br/>';
echo '<br/>';
echo '<br/>';
 ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            //['class' => 'yii\grid\SerialColumn'],

            'id',
            [
             'label' => 'NIK ',
             'contentOptions' =>['class' => 'table_class'],

             'value'=>function($data) {
        return isset($data->employee->org_id) ? ($data->employee->org_id):""; 
   //return ($data->departmentRole->department->department_name);
                },
            ],
            [
             'label' => 'Nama Depan ',
             'contentOptions' =>['class' => 'table_class'],
             'value'=>function($data) {
           return isset($data->employee->first_name) ? ($data->employee->first_name):""; 
   //return(date('Y-m-d', strtotime($data->employments->first_day)));
                },
            ],
            [
             'label' => 'Nama Belakang ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
   return isset($data->employee->last_name) ? ($data->employee->last_name):""; 
    //return(date('Y-m-d', strtotime($data->employments->start)));
                },
            ],
            [
             'label' => 'NO. KPJ ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
    return isset($data->employeeidentification->identification_detail[2]) ? ($data->employeeidentification->identification_detail[2]):""; 
   //return(date('Y-m-d', strtotime($data->employments->finish)));
                },
            ],
            
            [
             'label' => 'Tanggal Lahir ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
    return isset($data->employeeProfiles->birth_date) ? (date('Y-m-d', strtotime($data->employeeProfiles->birth_date))):"";
      //return(date('Y-m-d', strtotime($data->employeeProfile->birth_date)));
                },
            ],
            //GAJI POKOK DARI SIG_GAJI
            [
             'label' => 'Gaji ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
    return isset($data->sig_gaji->gaji_pokok) ? ($data->sig_gaji->gaji_pokok):"";                   
   //return ($data->employments->contract_type);
                },
            ],
            [
             'label' => 'Iuran BPJS ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
    return isset($data->sig_gaji->gaji_pokok) ? ($data->sig_gaji->gaji_pokok):"";                   
   //return ($data->employments->contract_type);
                },
            ],['class' => 'yii\grid\ActionColumn',
                       'template'=>'{update}{delete}',

                         ],



        ],
    ]); ?>

</div>
