<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\SigBpjs */

$this->title = 'Create Sig Bpjs';
$this->params['breadcrumbs'][] = ['label' => 'Sig Bpjs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sig-bpjs-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
