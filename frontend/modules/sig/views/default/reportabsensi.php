<?php
use yii\bootstrap\Button;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = Yii::t('app', 'Report Absensi');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Personalia'), 'url' => ['default/ketentuanpersonalia']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji dan Upah'), 'url' => ['default/gajidanupah']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/gajibulanan']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/perhitungan']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/absensi']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">

            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
            <p><h1>Report Absensi</h1></p>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['/hardware/schedule/attendance?id=&from=&to=']); ?>">Absensi perkaryawan &raquo;</a></p>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['/hardware/schedule/rekapitulasi?from=&to=']); ?>">Rekapitulasi (BELUM)&raquo;</a></p>

            </div>
            <div class="col-lg-4">

            </div>
        </div>

    </div>
</div>
