<?php
use yii\bootstrap\Button;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = Yii::t('app', 'Gaji Bulanan');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Personalia'), 'url' => ['default/ketentuanpersonalia']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji dan Upah'), 'url' => ['default/gajidanupah']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">

            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
            <p><h1>Gaji Bulanan</h1></p>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['siggaji/create']); ?>">Input Gaji &raquo;</a></p>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['siggaji/rekapitulasi']); ?>">Rekapitulasi &raquo;</a></p>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['siggaji/pilihproyek_payroll']); ?>">Payroll &raquo;</a></p>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['default/perhitungan']); ?>">Perhitungan &raquo;</a></p>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['sigslipgaji/index']); ?>">Slip Gaji &raquo;</a></p>

            </div>
            <div class="col-lg-4">

            </div>
        </div>

    </div>
</div>
