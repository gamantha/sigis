<?php
use yii\bootstrap\Button;
use yii\helpers\Url;
/* @var $this yii\web\View */


$id = $_REQUEST['id'];
$this->title = Yii::t('app', 'PPH');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Personalia'), 'url' => ['default/ketentuanpersonalia']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji dan Upah'), 'url' => ['default/gajidanupah']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/gajibulanan']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rekapitulasi'), 'url' => ['siggaji/rekapitulasi']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Potongan - Pilih Proyek'), 'url' => ['siggaji/potongan?id=' .$id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">

            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
                          <p><h1>Personalia</h1></p>
            <?php
                $id = $_REQUEST['id'];
             ?>
                                              
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['/sig/sigpph/create?id=' .$id]); ?>">Input Ketentuan Pajak &raquo;</a></p>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['/sig/sigpph/index?id=' .$id]); ?>">Perhitungan &raquo;</a></p>
            </div>
            <div class="col-lg-4">

            </div>
        </div>

    </div>
</div>
