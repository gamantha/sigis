<?php
use yii\bootstrap\Button;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = Yii::t('app', 'Perhitungan Absensi Gaji Bulanan');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Personalia'), 'url' => ['default/ketentuanpersonalia']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">

            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
            <p><h1>Perhitungan Absensi Gaji Bulanan</h1></p>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['perhitunganabsensigajibulanan']); ?>">Absen &raquo;</a></p>
            <p><a class="btn btn-default" href="">Verifikasi Absen &raquo;</a></p>
            <p><a class="btn btn-default" href="">Report &raquo;</a></p>
      
            </div>
            <div class="col-lg-4">

            </div>
        </div>

    </div>
</div>
