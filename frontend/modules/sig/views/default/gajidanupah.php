<?php
use yii\bootstrap\Button;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = Yii::t('app', 'Gaji & Upah');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Personalia'), 'url' => ['default/ketentuanpersonalia']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">

            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
                          <p><h1>Gaji & Upah</h1></p>
                                              <p><a class="btn btn-default" href="<?php echo Url::toRoute(['gajibulanan']); ?>">Gaji Bulanan &raquo;</a></p>
                                              <p><a class="btn btn-default" href="<?php echo Url::toRoute(['/organization/employee/index']); ?>">Gaji Harian &raquo;</a></p>
            </div>
            <div class="col-lg-4">

            </div>
        </div>

    </div>
</div>
