<?php
use yii\bootstrap\Button;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = Yii::t('app', 'Perhitungan Gaji Bulanan');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Personalia'), 'url' => ['default/ketentuanpersonalia']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">

            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
            <p><h1>Perhitungan Gaji Bulanan</h1></p>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['perhitunganabsensigajibulanan']); ?>">Absensi &raquo;</a></p>
            <p><a class="btn btn-default" href="">Potongan &raquo;</a></p>
            <p><a class="btn btn-default" href="">Pendaftaran &raquo;</a></p>
      
            </div>
            <div class="col-lg-4">

            </div>
        </div>

    </div>
</div>
