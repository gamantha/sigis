<?php
use yii\bootstrap\Button;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = Yii::t('app', 'Perhitungan');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Personalia'), 'url' => ['default/ketentuanpersonalia']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji dan Upah'), 'url' => ['default/gajidanupah']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/gajibulanan']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">

            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
            <p><h1>Perhitungan</h1></p>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['default/absensi']); ?>">Absensi &raquo;</a></p>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['siggaji/pilihproyek_potongan']); ?>">Potongan &raquo;</a></p>
                        <p><a class="btn btn-default" href="<?php echo Url::toRoute(['siggaji/pilihproyek_pendapatan']); ?>">Pendapatan &raquo;</a></p>


            </div>
            <div class="col-lg-4">

            </div>
        </div>

    </div>
</div>
