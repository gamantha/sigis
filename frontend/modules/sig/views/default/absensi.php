<?php
use yii\bootstrap\Button;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = Yii::t('app', 'Absensi');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Personalia'), 'url' => ['default/ketentuanpersonalia']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji dan Upah'), 'url' => ['default/gajidanupah']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/gajibulanan']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/perhitungan']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">

            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
            <p><h1>Perhitungan</h1></p>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['siggaji/create']); ?>">Absen (TIDAK PERLU) &raquo;</a></p>
            <p><a class="btn btn-default" href="<?php echo Url::toRoute(['siggaji/verifikasiabsen?id=&from=&to=']); ?>">Verifikasi Absen &raquo;</a></p>
                      <p><a class="btn btn-default" href="<?php echo Url::toRoute(['default/reportabsensi']); ?>">Report &raquo;</a></p>

            </div>
            <div class="col-lg-4">

            </div>
        </div>

    </div>
</div>
