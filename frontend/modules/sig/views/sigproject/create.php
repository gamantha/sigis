<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Sigproject */

$this->title = 'Create Sigproject';
$this->params['breadcrumbs'][] = ['label' => 'Sigprojects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sigproject-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
