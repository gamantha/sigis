<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\Datepicker;
use yii\widgets\ActiveForm;
use app\modules\organization\models\Employee;
use app\modules\organization\models\EmployeeProfile;
use app\modules\organization\models\DepartmentRole;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\sig\models\SigBpjsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = 'Sig Slip Gaji';

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji dan Upah'), 'url' => ['default/gajidanupah']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/gajibulanan']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pilih Proyek'), 'url' => ['siggaji/pilihproyek_payroll']];
?>
<style>
.yellow { background-color:  #f9f9bc;}
</style>

<div class="sig-payroll-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    
    
<?php
    
$form = ActiveForm::begin();
$employeelist = Employee::find()->andWhere(['status' => 'active'])->asArray()->All();
$employeelist2 = EmployeeProfile::find()->asArray()->All();
$employeelist3 = DepartmentRole::find()->asArray()->All();

  $employeearray=ArrayHelper::map($employeelist,'id','org_id');
  $employeearray2=ArrayHelper::map($employeelist2,'id','first_name');
  $employeearray3=ArrayHelper::map($employeelist3,'id','department_role_name');

echo $form->field($employee, 'id')->dropDownList($employeearray, ['id' => 'org_id', 'class'=>'input-large form-control','prompt'=>'Select...', ])->label('NIK');
echo $form->field($employeeprofile, 'id')->dropDownList($employeearray2, ['id' => 'first_name', 'class'=>'input-large form-control','prompt'=>'Select...', ])->label('Nama');
echo $form->field($departmentrole, 'id')->dropDownList($employeearray3, ['id' => 'department_role_name', 'class'=>'input-large form-control','prompt'=>'Select...', ])->label('Jabatan');
echo '<br/>';
 ?>
<div class="yellow">
<div class="row">
  <div class="col-sm-3">
        <br>
        <h5>&nbsp;&nbsp;&nbsp;GAJI POKOK</h5>
  </div>
  <div class="col-sm-2">
        <?= $form->field($employee, 'id')->textInput(['maxlength' => true])->label('') ?>
  </div>    
  <div class="col-sm-4" align="right">
      <br>
      <h5>Rp.</h5>
  </div>    
  <div class="col-sm-2">
        <?= $form->field($employee, 'id')->textInput(['maxlength' => true])->label('') ?>
  </div>    
</div>
<div class="row">
  <div class="col-sm-3">
        <br>
  </div>
  <div class="col-sm-2">
        <?= $form->field($employee, 'id')->textInput(['maxlength' => true])->label('') ?>
  </div>
  <div class="col-sm-4" align="right">
      <br>
      <h5>Rp.</h5>
  </div>    
  <div class="col-sm-2">
        <?= $form->field($employee, 'id')->textInput(['maxlength' => true])->label('') ?>
  </div>    
</div>

<div class="row">
  <div class="col-sm-3">
        <br>
  </div>
  <div class="col-sm-2">
        <?= $form->field($employee, 'id')->textInput(['maxlength' => true])->label('') ?>
  </div>
  <div class="col-sm-4" align="right">
      <br>
      <h5>Rp.</h5>
  </div>    
  <div class="col-sm-2">
        <?= $form->field($employee, 'id')->textInput(['maxlength' => true])->label('') ?>
  </div>    
</div>
<div class="row">
    <div class="col-sm-3">
        <br>
        <h5>&nbsp;&nbsp;&nbsp;Penambahan</h5>
    </div>
</div>
<div class="row">
    <div class="col-sm-3">
        <text>&nbsp;&nbsp;&nbsp;- Tunjangan Jabatan</text>
    </div>
    <div class="col-sm-1">
    </div>
    <div class="col-sm-3">
    </div>
    <div class="col-sm-3">
        <text>= Rp.</text>
    </div>
</div>
<div class="row">
    <div class="col-sm-3">
        <br>
        <text>&nbsp;&nbsp;&nbsp;- Tunjangan Makan dan Transport</text>
    </div>
    <div class="col-sm-1">
        <?= $form->field($employee, 'id')->textInput(['maxlength' => true])->label('') ?>
    </div>
    <div class="col-sm-3">
        <br>
        <text>hari X Rp.</text>
    </div>
    <div class="col-sm-3">
        <br>
        <text>= Rp.</text>
    </div>
</div><div class="row">
    <div class="col-sm-3">
        <br>
        <text>&nbsp;&nbsp;&nbsp;- Tunjangan Lembur</text>
    </div>
    <div class="col-sm-1">
        <?= $form->field($employee, 'id')->textInput(['maxlength' => true])->label('') ?>
    </div>
    <div class="col-sm-3">
        <br>
        <text>hari X Rp.</text>
    </div>
    <div class="col-sm-3">
        <br>
        <text>= Rp.</text>
    </div>
</div>
<div class="row">
    <div class="col-sm-3">
        <br>
        <text>&nbsp;&nbsp;&nbsp;- Premi Hadir</text>
    </div>
    <div class="col-sm-1">
    </div>
    <div class="col-sm-3">
    </div>
    <div class="col-sm-3">
        <text>= Rp.</text>
    </div>
</div><div class="row">
    <div class="col-sm-3">
        <br>
        <text>&nbsp;&nbsp;&nbsp;- Bonus Hadir</text>
    </div>
    <div class="col-sm-1">
    </div>
    <div class="col-sm-3">
    </div>
    <div class="col-sm-3">
        <text>= Rp.</text>
    </div>
</div><div class="row">
    <div class="col-sm-3">
        <br>
        <text>&nbsp;&nbsp;&nbsp;- Tunjangan Luar Kota</text>
    </div>
    
    <div class="col-sm-1">
        <?= $form->field($employee, 'id')->textInput(['maxlength' => true])->label('') ?>
    </div>
    <div class="col-sm-3">
        <br>
        <text>hari X Rp.</text>
    </div>
    <div class="col-sm-3">
        <br>
        <text>= Rp.</text>
    </div>
</div><div class="row">
    <div class="col-sm-3">
        <text>&nbsp;&nbsp;&nbsp;- ....................</text>
    </div>
    <div class="col-sm-1">
    </div>
    <div class="col-sm-3">
    </div>
    <div class="col-sm-3">
        <text>= Rp.</text>
    </div>
</div>
<div class="row">
  <div class="col-sm-3">
       
  </div>
  <div class="col-sm-2">
       
  </div>
  <div class="col-sm-4" align="right">
      <br>
      <h5>Rp.</h5>
  </div>    
  <div class="col-sm-2">
        <?= $form->field($employee, 'id')->textInput(['maxlength' => true])->label('') ?>
  </div>    
</div>
<div class="row">
    <div class="col-sm-2">
        <br>
        <h5>&nbsp;&nbsp;&nbsp;Potongan</h5>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <text>&nbsp;&nbsp;&nbsp;- Pengembalian Pinjaman</text>
    </div>
    <div class="col-sm-3">
        <text>= Rp.</text>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <text>&nbsp;&nbsp;&nbsp;- Keterlambatan</text>
    </div>
    <div class="col-sm-3">
        <text>= Rp.</text>
    </div>
</div><div class="row">
    <div class="col-sm-6">
        <text>&nbsp;&nbsp;&nbsp;- PPH 21</text>
    </div>
    <div class="col-sm-3">
        <text>= Rp.</text>
    </div>
</div><div class="row">
    <div class="col-sm-6">
        <text>&nbsp;&nbsp;&nbsp;- BPJS</text>
    </div>
    <div class="col-sm-3">
        <text>= Rp.</text>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <text>&nbsp;&nbsp;&nbsp;- ....................</text>
    </div>
    <div class="col-sm-3">
        <text>= Rp.</text>
    </div>
</div><div class="row">
    <div class="col-sm-6">
        <text>&nbsp;&nbsp;&nbsp;- ....................</text>
    </div>
    <div class="col-sm-3">
        <text>= Rp.</text>
    </div>
</div>
<div class="row">
  <div class="col-sm-3">
       
  </div>
  <div class="col-sm-2">
       
  </div>
  <div class="col-sm-4" align="right">
      <br>
      <h5>Rp.</h5>
  </div>    
  <div class="col-sm-2">
        <?= $form->field($employee, 'id')->textInput(['maxlength' => true])->label('') ?>
  </div>    
</div>
<div class="row">
  <div class="col-sm-3">
       
  </div>
  <div class="col-sm-2">
       
  </div>
  <div class="col-sm-4" align="right">
      <br>
      <h5>TOTAL PENERIMAAN Rp.</h5>
  </div>    
  <div class="col-sm-2">
        <?= $form->field($employee, 'id')->textInput(['maxlength' => true])->label('') ?>
  </div>    
</div>
    </div>
</div>
