<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\SigBpjs */

$this->title = 'Update Sig Bpjs: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sig Bpjs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sig-bpjs-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
