<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\SigBpjs */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sig Bpjs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sig-bpjs-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'employee' => $employee,
        'employeeprofile' => $employeeprofile,
        'employeeidentification' => $employeeidentification,
        'siggaji' => $siggaji,
        'attributes' => [
            'id',
            'org_id',
            'first_name',
            'last_name',
            'identification_detail',
            'birth_date',
            'gaji_pokok',
            'gaji_pokok',
            'tipe',
            'value',
        ],
    ]) ?>

</div>
