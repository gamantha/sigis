<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Sigpph */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="sigpph-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="well">
    <h3>TARIF</h3>
                        <div class="row">
                            <div class="col-sm-2">

                                <?= $form->field($model, 'operator[]')->dropDownList([ '<' => '<', '>' => '>', '<=' => '<=', '>=' =>                                     '>=', '==' => '=='])->label('Operator') ?>

                            </div>
                            
                            <div class="col-sm-8">

                                <?= $form->field($model, 'value_1[]')->textInput(['maxlength' => true])->label('Takehome Pay 1') ?>

                            </div>

                            <div class="col-sm-2">

                                <?= $form->field($model, 'value_2[]')->textInput(['maxlength' => true])->label('%') ?>

                            </div>

                        </div><!-- end:row -->


                        <div class="row">
                            
                            <div class="col-sm-2">

                                <?= $form->field($model, 'operator[]')->dropDownList([ '<' => '<', '>' => '>', '<=' => '<=', '>=' =>                                     '>=', '==' => '=='])->label('Operator') ?>

                            </div>
                            
                            <div class="col-sm-8">

                                <?= $form->field($model, 'value_1[]')->textInput(['maxlength' => true])->label('Takehome Pay 2') ?>

                            </div>
                            
                            <div class="col-sm-2">

                                <?= $form->field($model, 'value_2[]')->textInput(['maxlength' => true])->label('%') ?>

                            </div>

                        </div><!-- end:row -->
                        
                        <div class="row">

                            <div class="col-sm-2">

                                <?= $form->field($model, 'operator[]')->dropDownList([ '<' => '<', '>' => '>', '<=' => '<=', '>=' =>                                     '>=', '==' => '=='])->label('Operator') ?>

                            </div>
                            
                            <div class="col-sm-8">

                                <?= $form->field($model, 'value_1[]')->textInput(['maxlength' => true])->label('Takehome Pay 3') ?>

                            </div>

                            <div class="col-sm-2">

                                <?= $form->field($model, 'value_2[]')->textInput(['maxlength' => true])->label('%') ?>

                            </div>

                        </div><!-- end:row -->
        
                        <div class="row">

                            <div class="col-sm-2">

                               

                            </div>
                            
                            <div class="col-sm-8">

                                <h3>Tidak Punya NPWP</h3>

                            </div>

                            <div class="col-sm-2">

                                <?= $form->field($model, 'value_2[]')->textInput(['maxlength' => true])->label('%') ?>

                            </div>

                        </div><!-- end:row -->
        
    </div>
    
    <div class="well">
    <h3>PTKTP</h3>
    <?= $form->field($model, 'value_1[]')->textInput()->label('TK') ?>
    <?= $form->field($model, 'value_1[]')->textInput()->label('K0') ?>
    <?= $form->field($model, 'value_1[]')->textInput()->label('K1') ?>
    <?= $form->field($model, 'value_1[]')->textInput()->label('K2') ?>
    <?= $form->field($model, 'value_1[]')->textInput()->label('K3') ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
