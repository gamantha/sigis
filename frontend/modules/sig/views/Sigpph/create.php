<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Sigpph */

$id = $_REQUEST['id'];
$this->title = 'Create Sigpph';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji dan Upah'), 'url' => ['default/gajidanupah']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gaji Bulanan'), 'url' => ['default/gajibulanan']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rekapitulasi'), 'url' => ['siggaji/rekapitulasi']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Potongan - Pilih Proyek'), 'url' => ['siggaji/potongan?id=' .$id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'PPH'), 'url' => ['default/pph?id=' .$id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sigpph-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
