<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Sigpph */

$this->title = 'Update Sigpph: ' . ' ' . $model->sig_project_id;
$this->params['breadcrumbs'][] = ['label' => 'Sigpphs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sig_project_id, 'url' => ['view', 'sig_project_id' => $model->sig_project_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sigpph-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
