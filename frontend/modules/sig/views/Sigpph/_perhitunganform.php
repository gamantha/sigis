

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\Datepicker;
//use yii\grid\GridView;
use kartik\grid\GridView;
use common\Headergrouping;
use app\modules\sig\models\SigProject;
use app\modules\sig\models\Siggaji;
use app\modules\sig\models\SiggajiSearch;
use app\modules\organization\models\Employee;
use app\modules\organization\models\Useremployee;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\View;
use yii\helpers\Url;
use app\modules\sig\models\SigBpjs;
use app\modules\hardware\models\Schedule;
/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Siggaji */
/* @var $form yii\widgets\ActiveForm */

//$this->registerJsFile(Url::base() . '/js/jquery.floatThead.js' , ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Url::base() . '/js/tableHeadFixer.js' , ['depends' => [\yii\web\JqueryAsset::className()]]);


?>
   <h1>Rekapitulasi Gaji</h1>



<div class="siggaji-form">


    <?php $form = ActiveForm::begin(); ?>



<?php
echo 'start period';
echo Datepicker::widget([
 'name'=>'start_period',
     'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','style' => 'z-index:99;'],
]);
echo '<br/>finish period';
echo Datepicker::widget([
 'name'=>'finish_period',
     'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','maxlength'=>'25','style' => 'z-index:99;'],
]);
echo '<br/>';
echo '<br/>';

 ?>

 <div class="form-group">
  <?php
if(isset($_POST['print'])) {}else {
                         echo Html::submitButton( Yii::t('app', 'Search'), ['id'=>'search', 'name'=>'search','class' => 'btn btn-primary']);
//echo Html::a('Pdf', 'url', ['class' => 'btn btn-primary','name'=>'Pdf', 'target'=>'_blank'])
                  echo Html::submitButton( Yii::t('app', 'Print'), ['id'=>'print', 'name'=>'print','target'=>'_blank','class' => 'btn btn-success']);
          echo '<br/>';
         }
                   ?>
 </div>



<?php
$gridcolumns = [
 [
  'label' => 'ID ',
  //'attribute' => 'org_id',
     'headerOptions' =>['style' => 'z-index:50;',
 'rowspan' => 2,
    ],
     'contentOptions' =>['class' => 'table_class',
    // 'style' => 'z-index:99;'
    ],
 'value'=>function ($data) {
 //$employee = Employee::find()->andWhere(['id'=> $data['id']])->One();
 return '1';
 },
 ],
];


$projects = SigProject::find()->andWhere(['status'=> 'active'])->All();

$columnindex = 0;
$headercolumns=[];
foreach ($projects as $project){

$projectcolumn = [
 'name'=>$project->name,
 'start'=>$columnindex + 3, //indeks kolom 3
 'end'=>$columnindex + 5, //indeks kolom 4
];
 array_push($headercolumns, $projectcolumn);
 $columnindex = $columnindex + 3;

}




$totalcolumns =  [
  'header' => 'Total',
       'format' => 'raw',
           'headerOptions' =>['style' => 'z-index:50;'],
  'options'=>[
   'value' => $project->id,
  ],
     //'contentOptions' =>['class' => 'table_class'],
   'value'=>function($data, $key,$index, $widget) {
    return '<div class="totalgaji" style="width:150px;" id="total-pendapatan-'. $index.'">Rp. </div>';

     },
 ];

array_push($gridcolumns, $totalcolumns);

?>

<?php

//echo \common\components\Headergrouping::widget([
echo GridView::widget([
	'id'=>'user-grid',
	'summary'=>'Total {count} data',
 'filterPosition' => GridView::FILTER_POS_BODY,
 //'responsive' => true,
 'options' => [
  'id' => 'renola',

 ],
 'tableOptions' => [
  //'name'=>'tasdadsa',
  'id' => 'fixTable',

 ],
  'headerRowOptions' => [
//'colspan' => '3',
  ],
  //'showFooter' => true,
  'showHeader' => true,
 'containerOptions' => [
  //'name'=>'tasdadsa',
  //'id' => 'fixTable',
  //'class' => "table table-striped table-bordered",
   // 'style' => 'overflow-x:scroll; overflow-y:scroll',

  'style' => 'overflow: auto;',
   //height:500px;',
 ],
 'hover'=> true,
 //'floatHeader' => true,
  'floatHeaderOptions'=>[
   'debug' => true,
  // 'scrollingTop'=>'0'

  ],
  //'resizableColumns'=>true,
 //'mergeHeaders'=>$headercolumns,
 'showPageSummary' => true,
	//'dataProvider'=>$employeedataProvider,
 	'dataProvider'=>$dataProvider,
	//'columns'=>$gridcolumns,
 'columns' => [

[
 'label' => 'NIK ',
'value'=>function ($data) {
return $data->employee->org_id;
},
],
[

 'label' => 'TAKE HOME PAY/Th',

 'pageSummary'=>true,
 'value'=>function ($data) {
 return $data->gaji_pokok + $data->tunjangan_jabatan;
 },
],
[
 'label' => 'BI JAB 5%',
  'pageSummary'=>true,
  'value'=>function ($model, $key, $index, $column) {
  $bpjs = new SigBpjs();
  $schedule = new Schedule();
  $bpjs1 = SigBpjs::find()->All();
    $nilaipotongan = 0;
    foreach ($bpjs1 as $bpjsa){
    $nilaipotongan = $nilaipotongan+$bpjsa->value;
    /*$projectcolumn = [
     'name'=>$project->name,
     'start'=>$columnindex + 3, //indeks kolom 3
     'end'=>$columnindex + 5, //indeks kolom 4
    ];
     array_push($headercolumns, $projectcolumn);
     $columnindex = $columnindex + 3;*/
    }
  $rumusbpjs = (0.0089+0.003+0.057);  
  $arif = $bpjs->test();
  $arif=$bpjs->bpjs($model->employee->id,$_REQUEST['id'],$_REQUEST['start_period'],$_REQUEST['finish_period']);
  $rumusbpjs1=$nilaipotongan * $model->gaji_pokok / 10000;
  $kasbon=0;
  $pph=0;
  $haritelat=$schedule->countTelat($model->employee->id,$_REQUEST['start_period'],$_REQUEST['finish_period']);
  $denda=$schedule->getDenda($model->employee->id);
  $telat=$haritelat*$denda;
  return $telat;
  
  
  
  //$id,$project_id,$from,$to
        //return $data->gaji_pokok + $data->tunjangan_jabatan;
 },
],
[

 'label' => 'PKP',

 'pageSummary'=>true,
 'value'=>function ($data) {
 return $data->gaji_pokok + $data->tunjangan_jabatan;
 },
],
[

 'label' => 'DENDA NPWP',

 'pageSummary'=>true,
 'value'=>function ($data) {
 return $data->gaji_pokok + $data->tunjangan_jabatan;
 },
],
[

 'label' => 'PPH PER/Th',

 'pageSummary'=>true,
 'value'=>function ($data) {
 return $data->gaji_pokok + $data->tunjangan_jabatan;
 },
],
[

 'label' => 'PPH BULANAN',

 'pageSummary'=>true,
 'value'=>function ($data) {
 return $data->gaji_pokok + $data->tunjangan_jabatan;
 },
],
]

]);



?>
 <?php ActiveForm::end(); ?>



</div>
