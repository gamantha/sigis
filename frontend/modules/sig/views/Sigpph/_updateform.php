<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\modules\organization\models\Employee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employee-form">
<div class="well">
    <?php

    $this->registerJs('

    $("#authorize_button").click(function(){
    $("#submit_button").show();
    $("#cred_div").show();
    $(this).hide();
    });

    ', \yii\web\View::POS_END);

    $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'org_id')->textInput(['maxlength' => true])->label('NIK') ?>

   <?= $form->field($department, 'id')->dropDownList($departmentall, ['id' => 'dept_id', 'class'=>'input-large form-control','prompt'=>'Select...'])->label('Department'); ?>

    <?= $form->field($model, 'department_role_id')->widget(DepDrop::classname(), [
    'options'=>['id'=>'role_id', 'class'=>'input-large form-control'],
       'data'=>[$departmentrole->id => $departmentrole->department_role_name],
    'pluginOptions'=>[
        'depends'=>['dept_id'],
        'placeholder'=>'Select...',
        'url'=>Url::to(['/organization/employee/roles'])
    ]
])->label('Jabatan');
    ?>
    </div>
    <h3>STATUS KARYAWAN</h3>
    <div class="well">
    <?= $form->field($employment, 'first_day')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
         'options' => ['class' => 'form-control','maxlength' => '255',],
    ])->label('Hari Pertama Kerja') ?>



    <?= $form->field($employment, 'contract_type')->dropDownList([ 'percobaan' => 'Percobaan', 'kontrak' => 'Kontrak', 'tetap' => 'Tetap',], ['prompt' => '']) ?>


    <?= $form->field($employment, 'start')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
         'options' => ['class' => 'form-control','maxlength' => '255',],
    ])->label('Awal Periode') ?>
    <?= $form->field($employment, 'finish')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
     'options' => ['class' => 'form-control','maxlength' => '255',],
    ])->label('Akhir Periode') ?>
        <?= $form->field($employmentsig, 'status')->radioList([ 'K3' => 'K3', 'K2' => 'K2', 'K1' => 'K1', 'K0'=>'K0','TK'=>'TK',],
        [
        'itemOptions'=>[
         //'name' => 'k2',
         //'checked' => true,
        ],
        'item' => function($index, $label, $name, $checked, $value) {

 if($checked) {
$check = 'checked';} else {
 $check = '';
}

            $return = '<label class="modal-radio">';
            $return .= '<input type="radio" name="' . $name . '" '. $check . ' value="' . $value . '" tabindex="3">';
            $return .= '<i></i>';
            $return .= '<span>' . ucwords($label) . '</span>';
            $return .= '</label>';

            return $return;
        }
        ])?>

    </div>

    <h3>IDENTITAS KARYAWAN</h3>
    <div class="well">
    <?= $form->field($employeeprofile, 'first_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($employeeprofile, 'last_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($employeeprofile, 'birth_place')->textInput(['maxlength' => true]) ?>
    <?= $form->field($employeeprofile, 'birth_date')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
         'options' => ['class' => 'form-control','maxlength' => '255',],
    ]) ?>
   </div>

<h3>IDENTIFIKASI</h3>
<div class="well">
 <!--?= $form->field($employeeidentification, 'identification_type')->dropDownList([ 'ktp' => 'KTP', 'sim' => 'SIM', 'paspor' => 'PASPOR',], ['prompt' => ''])->label('Tipe Identifikasi') ?-->
<?= $form->field($employeeidentification[0], 'identification_type')->dropDownList([ 'ktp' => 'KTP', 'sim' => 'SIM', 'paspor' => 'PASPOR',], ['prompt' => '']) ?>


    <?php


$loop_index = 0;
foreach ($employeeidentification as $emp_id){
 echo $form->field($employeeidentification[$loop_index], 'identification_detail[]')->textInput( ['class'=>'input-large form-control', 'maxlength' => true, 'value'=>$emp_id['identification_detail']])->label('Nomor ' . $emp_id['identification_type']);

 $loop_index++;
}
 ?>


       </div>

<h3>INFORMASI CONTACT</h3>
<div class="well">



    <?php

    $loop_index = 0;
    foreach ($employeecontact as $emp_id){
     echo $form->field($employeecontact[$loop_index], 'contact_detail[]')->textInput( ['class'=>'input-large form-control', 'maxlength' => true, 'value'=>$emp_id['contact_detail']])->label('Nomor ' . $emp_id['contact_type']);
     $loop_index++;
    }
     ?>




</div>
<h3>INFORMASI BANK</h3>
    <div class="well">

     <?php

     $loop_index = 0;
     foreach ($employeebank as $emp_id){
      echo ' <p><strong>'.$emp_id['bank_name'].'</strong></p>';
      echo $form->field($employeebank[$loop_index], 'account_name[]')->textInput( ['class'=>'input-large form-control', 'maxlength' => true, 'value'=>$emp_id['account_name']])->label('nama account ');
            echo $form->field($employeebank[$loop_index], 'account_number[]')->textInput( ['class'=>'input-large form-control', 'maxlength' => true, 'value'=>$emp_id['account_number']])->label('no account ');
      $loop_index++;
     }
      ?>


   </div>

   <div id='cred_div' style="display:none;">
          <?= 'Username  ' . Html::textInput('username','',['class'=>'form-control']) ?>
               <?= '<br/>Password ' . Html::textInput('password','',['class'=>'form-control']) . "<br/>"?>
   </div>

    <div class="form-group">
<?php
                //if (\Yii::$app->user->can('createGolongan')) {
        //echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);

        //echo Html::buttonInput('Submit',['class' => 'btn btn-success', 'id' => 'authorize_button']);
echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
['id'=>'submit_button','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);


                 ?>

    </div>



    <?php ActiveForm::end(); ?>

</div>
