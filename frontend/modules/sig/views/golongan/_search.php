<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\GolonganSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="golongan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'golongan') ?>

    <?= $form->field($model, 'department_role_id') ?>

    <?= $form->field($model, 'masa_kerja_minimal') ?>

    <?= $form->field($model, 'tunjangan_makan_transport') ?>

    <?php // echo $form->field($model, 'tunjangan_premi_hadir') ?>

    <?php // echo $form->field($model, 'tunjangan_bonus_hadir') ?>

    <?php // echo $form->field($model, 'tunjangan_lembur') ?>

    <?php // echo $form->field($model, 'tunjangan_lembur_luarkota') ?>

    <?php // echo $form->field($model, 'denda_keterlambatan') ?>

    <?php // echo $form->field($model, 'plafon_kasbon') ?>

    <?php // echo $form->field($model, 'plafon_kesehatan') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
