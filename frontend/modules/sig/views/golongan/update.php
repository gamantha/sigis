<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Golongan */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Golongan',
]) . ' ' . $model->golongan;


$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ketentuan Personalia'), 'url' => ['default/ketentuanpersonalia']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Report'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->golongan, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="golongan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_updateform', [
        'model' => $model,
        'rolemodel' => $rolemodel,
        'departmentmodel' => $departmentmodel,
    ]) ?>

</div>