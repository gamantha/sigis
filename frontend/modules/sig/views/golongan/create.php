<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Golongan */

$this->title = Yii::t('app', 'Input Data');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ketentuan Personalia'), 'url' => ['default/ketentuanpersonalia']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="golongan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row well">
      <i>
* Kombinasi <strong>Department</strong> & <strong>Jabatan</strong> & <strong>Minimal Day</strong> & <strong>Minimal Month</strong> & <strong>Minimal Year</strong> HARUS UNIK<br/>
* 1 gologan = 1 jabatan? atau 1 golongan = n jabatan
    </i>
    </div>


    <?= $this->render('_form', [
        'model' => $model,
     //   'rolemodel' => $rolemodel,
        'departmentmodel' => $departmentmodel,
    ]) ?>

</div>
