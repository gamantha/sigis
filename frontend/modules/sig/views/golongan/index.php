<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use app\modules\sig\models\Golongan;
//use app\modules\organization\models\Department;
//use app\modules\organization\models\DepartmentRole;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\sig\models\GolonganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'Report Ketentuan Personalia');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ketentuan Personalia'), 'url' => ['default/ketentuanpersonalia']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="golongan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
          //  ['class' => 'yii\grid\SerialColumn'],

            'golongan',
            //'id',
            [
             'label' => 'Department ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
   return ($data->departmentRole->department->department_name);
                },
            ],
            [
             'label' => 'Jabatan ',
              'value'=>function($data) {
   return ($data->departmentRole->department_role_name);
                },
            ],
            //'masa_kerja_minimal',


            [
             'header' => 'Masa Kerja<br/>
              Minimal',
                             'contentOptions' =>['style'=>['']],
              'value'=>function($data) {
if(isset($data->masa_kerja_minimal)) {
 $interval = new DateInterval($data->masa_kerja_minimal);
//echo

 return $interval->format('%y year ') . $interval->format('%m months ') . $interval->format('%d days');
 //explode(":",$data->masa_kerja_minimal)[0] . ' hari, ' .
 //explode(":",$data->masa_kerja_minimal)[1] . ' bulan, ' . explode(":",$data->masa_kerja_minimal)[2] . ' tahun');

} else {
 return '';
}


                },
            ],



            //'tunjangan_makan_transport',
            [
             'header' => 'Premi &<br/>
              Bonus Hadir',
                             'contentOptions' =>['style'=>['']],
              'value'=>function($data) {
               return ($data->tunjangan_premi_hadir + $data->tunjangan_bonus_hadir);
                },
            ],
            [
             'header' => 'Tunjangan Makan &<br/>Transport',
                      'attribute' => 'tunjangan_makan_transport',
              'value'=>function($data) {
               return ($data->tunjangan_makan_transport);
                },
            ],
            // 'tunjangan_premi_hadir',
            // 'tunjangan_bonus_hadir',
            // 'tunjangan_lembur',
            [
             'header' => 'Lembur',
                      'attribute' => 'tunjangan_lembur',
              'value'=>function($data) {
               return ($data->tunjangan_lembur);
                },
            ],
            [
             'header' => 'Luar Kota',
                     'attribute' => 'tunjangan_lembur_luarkota',
             //   'filter'=>ArrayHelper::map(Golongan::find()->asArray()->all(), 'id', 'tunjangan_lembur_luarkota'),
              'value'=>function($data) {
               return ($data->tunjangan_lembur_luarkota);
                },
            ],
            // 'tunjangan_lembur_luarkota',
            // 'denda_keterlambatan',
            [
             'header' => 'Plafon<br>Kasbon',
                     'attribute' => 'plafon_kasbon',
             //   'filter'=>ArrayHelper::map(Golongan::find()->asArray()->all(), 'id', 'tunjangan_lembur_luarkota'),
              'value'=>function($data) {
               return ($data->plafon_kasbon);
                },
            ],
            [
             'header' => 'Plafon<br>Kesehatan',
                     'attribute' => 'plafon_kesehatan',
             //   'filter'=>ArrayHelper::map(Golongan::find()->asArray()->all(), 'id', 'tunjangan_lembur_luarkota'),
              'value'=>function($data) {
               return ($data->plafon_kesehatan);
                },
            ],
          //   'plafon_kasbon',
            // 'plafon_kesehatan',

            //            ['class' => 'yii\grid\ActionColumn'],

                        ['class' => 'yii\grid\ActionColumn',
                                   'template'=>'{update}{delete}',

                                     ],
        ],
    ]); ?>

</div>
