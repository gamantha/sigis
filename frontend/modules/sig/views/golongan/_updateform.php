<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\organization\models\DepartmentRole;
use app\modules\organization\models\Department;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Golongan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="golongan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $departments=Department::find()->all();
     $roles=DepartmentRole::find()->all();
      $deptsListData=ArrayHelper::map($departments,'id','department_name');
         $rolesListData=ArrayHelper::map($roles,'id','department_role_name');
    echo $form->field($model, 'id')->textInput(['readonly' => true]);

    echo $form->field($model, 'golongan')->textInput(['maxlength' => true]);


$rol =  DepartmentRole::find()->where(['id' =>$model->department_role_id])->one();
    ?>
    <?= $form->field($departmentmodel, 'id')
->dropDownList($deptsListData, ['id' => 'dept_id', 'class'=>'input-large form-control','prompt'=>'Select...', 'selected' => '2' ])
    //->textInput(['readonly' => false, 'value' => $rol->department->department_name])

    ->label('Department') ?>



    <?= $form->field($model, 'department_role_id')->widget(DepDrop::classname(), [
    'options'=>['id'=>'role_id', 'class'=>'input-large form-control'],
         'data'=>[$rolemodel->id => $rolemodel->department_role_name],
    'pluginOptions'=>[
        'depends'=>['dept_id'],
        'placeholder'=>'Select...',
        'url'=>Url::to(['/sig/golongan/roles'])
    ]
])->label('Jabatan');
    ?>
<div class="well">
  <p><strong>Masa Kerja Minimal</strong></p>
    <?= $form->field($model, 'day')->textInput(['value'=>explode(":",$model->masa_kerja_minimal)[0]]) ?>
        <?= $form->field($model, 'month')->textInput(['value'=>explode(":",$model->masa_kerja_minimal)[1]]) ?>
            <?= $form->field($model, 'year')->textInput(['value'=>explode(":",$model->masa_kerja_minimal)[2]]) ?>
           </div>
<?php
 /*echo '<br/><br/><br/>sasa';
    $mkm =  explode(":",$model->masa_kerja_minimal);
    echo $mkm[0];
    */
 ?>

 <?= $form->field($model, 'tunjangan_makan_transport', [
 'template' => '{label} <div class="form-group"><div class="input-group"> <div class="input-group-addon">Rp. </div>{input}{error}{hint}<div class="input-group-addon">/ hari</div></div></div>'
])->textInput();
 ?>
 <?= $form->field($model, 'tunjangan_premi_hadir', [
 'template' => '{label} <div class="form-group"><div class="input-group"> <div class="input-group-addon">Rp. </div>{input}{error}{hint}<div class="input-group-addon">/ hari</div></div></div>'
])->textInput();
 ?>
 <?= $form->field($model, 'tunjangan_bonus_hadir', [
 'template' => '{label} <div class="form-group"><div class="input-group"> <div class="input-group-addon">Rp. </div>{input}{error}{hint}<div class="input-group-addon">/ hari</div></div></div>'
])->textInput();
 ?>
 <?= $form->field($model, 'tunjangan_lembur', [
 'template' => '{label} <div class="form-group"><div class="input-group"> <div class="input-group-addon">Rp. </div>{input}{error}{hint}<div class="input-group-addon">/ hari</div></div></div>'
])->textInput();
 ?>
 <?= $form->field($model, 'tunjangan_lembur_luarkota', [
 'template' => '{label} <div class="form-group"><div class="input-group"> <div class="input-group-addon">Rp. </div>{input}{error}{hint}<div class="input-group-addon">/ hari</div></div></div>'
])->textInput();
 ?>
 <?= $form->field($model, 'denda_keterlambatan', [
 'template' => '{label} <div class="form-group"><div class="input-group"> <div class="input-group-addon">Rp. </div>{input}{error}{hint}<div class="input-group-addon">/ hari</div></div></div>'
])->textInput();
 ?>
 <?= $form->field($model, 'plafon_kasbon', [
 'template' => '{label} <div class="form-group"><div class="input-group"> <div class="input-group-addon">Rp. </div>{input}{error}{hint}<div class="input-group-addon">/ hari</div></div></div>'
])->textInput();
 ?>
 <?= $form->field($model, 'plafon_kesehatan', [
 'template' => '{label} <div class="form-group"><div class="input-group"> <div class="input-group-addon">Rp. </div>{input}{error}{hint}<div class="input-group-addon">/ hari</div></div></div>'
])->textInput();
 ?>

    <div class="form-group">

     <?php
                     //if (\Yii::$app->user->can('createGolongan')) {
                     if(true){
             echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
     }
                      ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
