<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Golongan */

$this->title = $model->golongan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ketentuan Personalia'), 'url' => ['default/ketentuanpersonalia']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Report'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;






?>
<div class="golongan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'golongan',
            'department_role_id',
            'masa_kerja_minimal',
            'tunjangan_makan_transport',
            'tunjangan_premi_hadir',
            'tunjangan_bonus_hadir',
            'tunjangan_lembur',
            'tunjangan_lembur_luarkota',
            'denda_keterlambatan',
            'plafon_kasbon',
            'plafon_kesehatan',
        ],
    ]) ?>

</div>
