<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\organization\models\Department;
use app\modules\organization\models\DepartmentRole;
use kartik\widgets\DepDrop;
use app\modules\sig\models\Golongan;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Golongan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="golongan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $departments=Department::find()->all();
 //    $roles=DepartmentRole::find()->all();
      $departments=ArrayHelper::map($departments,'id','department_name');
   //      $rolesListData=ArrayHelper::map($roles,'id','department_role_name');

    $lookup = Array("0" => "a","1" => "b","2" => "c","3" => "d","4" => "e","5" => "f","6" => "g","7" => "h","8" => "i","9" => "j","10" => "k","11" => "l","12" => "m","13" => "n","14" => "o","15" => "p","16" => "q","17" => "r","18" => "s","19" => "t","20" => "u","21" => "v","22" => "w","23" => "x","24" => "y","25" => "z");
    $golonganexist=Golongan::find()->select(['golongan'])->orderBy(['golongan' => SORT_ASC])->asArray()->All();
    $gols = ArrayHelper::map($golonganexist, 'golongan', 'golongan');

    $nextgol='';
    $largest=0;
    foreach($gols as $gol){
        foreach($lookup as $key=>$value){
            if($gol==$value){
                if($key>$largest){
                    $largest=$key;
                    $nextgol=$value;
                }
            }
        }
    }
if(sizeof($golonganexist) > 0)
    $largest=$largest+1;
    $next_character = $lookup[$largest];
    $golonganbaru=[$next_character=>$next_character];

    //array_push ($gols,$golonganbaru);
    $gols[$next_character]=$next_character;
    ?>

    <?= Html::dropDownList('golongan', $next_character, $gols, ['class'=>'input-large form-control']) ?>


    <?= $form->field($departmentmodel, 'id')->dropDownList($departments, ['id' => 'dept_id', 'class'=>'input-large form-control','prompt'=>'Select...'])->label('Deparment'); ?>

    <?= $form->field($model, 'department_role_id')->widget(DepDrop::classname(), [
    'options'=>['id'=>'role_id', 'class'=>'input-large form-control'],
    'pluginOptions'=>[
        'depends'=>['dept_id'],
        'placeholder'=>'Select...',
        'url'=>Url::to(['/sig/golongan/roles'])
    ]
])->label('Jabatan');
    ?>
<div class="well">
 <p><strong>Masa Kerja Minimal</strong></p>
    <?= $form->field($model, 'day')->dropDownList(
        ['0'=>'0','1' => '1', '2'=>'2','3'=>'3', '4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10','11'=>'11','12'=>'12']
    )?>
        <!--?= Html::dropDownList('golongan', $next_character, $gols, ['class'=>'input-large form-control']) ?-->
        <?= $form->field($model, 'month')->dropDownList(
            ['0'=>'0','1' => '1', '2'=>'2','3'=>'3', '4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10','11'=>'11','12'=>'12']
        )?>
            <?= $form->field($model, 'year')->dropDownList(
                ['0'=>'0','1' => '1', '2'=>'2','3'=>'3', '4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10','11'=>'11','12'=>'12']
                 // Flat array ('id'=>'label')
              //  ['prompt'=>'0']    // options
            )?>

    <?= $form->field($model, 'tunjangan_makan_transport', [
    'template' => '{label} <div class="form-group"><div class="input-group"> <div class="input-group-addon">Rp. </div>{input}{error}{hint}<div class="input-group-addon">/ hari</div></div></div>'
])->textInput();
    ?>
    <?= $form->field($model, 'tunjangan_premi_hadir', [
    'template' => '{label} <div class="form-group"><div class="input-group"> <div class="input-group-addon">Rp. </div>{input}{error}{hint}<div class="input-group-addon">/ hari</div></div></div>'
])->textInput();
    ?>
    <?= $form->field($model, 'tunjangan_bonus_hadir', [
    'template' => '{label} <div class="form-group"><div class="input-group"> <div class="input-group-addon">Rp. </div>{input}{error}{hint}<div class="input-group-addon">/ hari</div></div></div>'
])->textInput();
    ?>
    <?= $form->field($model, 'tunjangan_lembur', [
    'template' => '{label} <div class="form-group"><div class="input-group"> <div class="input-group-addon">Rp. </div>{input}{error}{hint}<div class="input-group-addon">/ hari</div></div></div>'
])->textInput();
    ?>
    <?= $form->field($model, 'tunjangan_lembur_luarkota', [
    'template' => '{label} <div class="form-group"><div class="input-group"> <div class="input-group-addon">Rp. </div>{input}{error}{hint}<div class="input-group-addon">/ hari</div></div></div>'
])->textInput();
    ?>
    <?= $form->field($model, 'denda_keterlambatan', [
    'template' => '{label} <div class="form-group"><div class="input-group"> <div class="input-group-addon">Rp. </div>{input}{error}{hint}<div class="input-group-addon">/ hari</div></div></div>'
])->textInput();
    ?>
    <?= $form->field($model, 'plafon_kasbon', [
    'template' => '{label} <div class="form-group"><div class="input-group"> <div class="input-group-addon">Rp. </div>{input}{error}{hint}<div class="input-group-addon">/ hari</div></div></div>'
])->textInput();
    ?>
    <?= $form->field($model, 'plafon_kesehatan', [
    'template' => '{label} <div class="form-group"><div class="input-group"> <div class="input-group-addon">Rp. </div>{input}{error}{hint}<div class="input-group-addon">/ hari</div></div></div>'
])->textInput();
    ?>


    <div class="form-group">
     <?php

             echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);

                      ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
