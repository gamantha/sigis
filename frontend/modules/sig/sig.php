<?php

namespace app\modules\sig;

class sig extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\sig\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
