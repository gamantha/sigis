<?php

namespace app\modules\organization\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;
use common\models\Model;
use app\modules\organization\models\Employee;
use app\modules\organization\models\EmployeeSearch;
use app\modules\organization\models\EmployeeBank;
use app\modules\organization\models\EmployeeContact;
use app\modules\organization\models\EmployeeIdentification;
use app\modules\organization\models\EmployeeProfile;
use app\modules\organization\models\Employment;
use app\modules\organization\models\EmploymentSig;
use app\modules\organization\models\Department;
use app\modules\organization\models\DepartmentRole;
use app\modules\organization\models\DepartmentRoleSuperiority;
use app\modules\sig\models\Golongan;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\View;

/**
 * EmployeeController implements the CRUD actions for Employee model.
 */
class EmployeeController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Employee models.
     * @return mixed
     */
    public function actionIndex()
    {
        $employee = new Employee();
        $employee->getGolongan('75');
        $searchModel = new EmployeeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Employee model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }




    /**
     * Creates a new Employee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Employee();
        $modelsBank = [new EmployeeBank];
        $department = new Department();
        $employment = new Employment();

        $employeeprofile = new EmployeeProfile();
        $employeeidentification = new EmployeeIdentification();
        $idtosave = new employeeidentification();
        $employeecontact = new EmployeeContact();
        $employmentsig = new EmploymentSig();


        $bankquery = new EmployeeBank();

        $bankprovider = new ActiveDataProvider([
            'query' => $bankquery,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);


        //if (\Yii::$app->user->can('createGolongan')) {
        if (true){
        if ($model->load(Yii::$app->request->post())
        && $employment->load(Yii::$app->request->post())
        && $employeeprofile->load(Yii::$app->request->post())
        && $employeeidentification->load(Yii::$app->request->post())
        && $employeecontact->load(Yii::$app->request->post())
        && $employmentsig->load(Yii::$app->request->post())
        ) {

  $employeebanks=EmployeeBank::find()->where(['employee_id' => $model->id])->All();
         $oldIDs = ArrayHelper::map($employeebanks, 'id', 'id');
         $employeebanks = Model::createMultiple(EmployeeBank::classname(), $employeebanks);
         Model::loadMultiple($employeebanks, Yii::$app->request->post());
         $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($employeebanks, 'id', 'id')));


         // ajax validation
         if (Yii::$app->request->isAjax) {
          echo 'IS AJAX';
             Yii::$app->response->format = Response::FORMAT_JSON;
             return ArrayHelper::merge(
                 ActiveForm::validateMultiple($employeebanks),
                 ActiveForm::validate($model)
             );
         }


// validate all models
$valid = $model->validate();
$valid = Model::validateMultiple($modelsBank) && $valid;



$authorized = true;
if (isset($_POST['password']) && $_POST['password'] != '' && isset($_POST['username']) && $_POST['username'] != '') {
if ($_POST['password'] == $_POST['username']){
 $authorized = true;
}
}
//if (isset($_POST['password']) && $_POST['password'] != '') {
if ($authorized) {
 if ($valid) {
     $transaction = \Yii::$app->db->beginTransaction();
         $model->status = 'active';
     try {
         if ($flag = $model->save(false)) {
               Yii::$app->db->createCommand()
                  ->batchInsert(EmployeeContact::tableName(), ['employee_id', 'contact_type','contact_detail'],
                  [
                   [$model->id, 'street',$employeecontact['contact_detail'][0]],
                   [$model->id, 'home',$employeecontact['contact_detail'][1]],
                   [$model->id, 'mobile',$employeecontact['contact_detail'][2]],
                   [$model->id, 'mobile',$employeecontact['contact_detail'][3]],
                   [$model->id, 'email',$employeecontact['contact_detail'][4]],
                  ])
                  ->execute();

             Yii::$app->db->createCommand()
             ->batchInsert(employeeidentification::tableName(), ['employee_id', 'identification_type','identification_detail'],
             [
               [$model->id, $employeeidentification->identification_type,$employeeidentification['identification_detail'][0]],
               [$model->id, 'npwp',$employeeidentification['identification_detail'][1]],
               [$model->id, 'kpj',$employeeidentification['identification_detail'][2]],
             ])
             ->execute();


               $employment->employee_id = $model->id;
               $employeeprofile->employee_id = $model->id;
               $employmentsig->employee_id = $model->id;
               $employmentsig->employee_id = $model->id;
               $employmentsig->save();
               $employeeprofile->save();
               $employment->status = 'active';
               $employment->save();
               $employmentsig->save();



               if (! empty($deletedIDs)) {
                   EmployeeBank::deleteAll(['id' => $deletedIDs]);
               }
               foreach ($employeebanks as $employeebank) {
                   $employeebank->employee_id = $model->id;
                   if (! ($flag = $employeebank->save(false))) {
                       $transaction->rollBack();
                       break;
                   }
               }



         }
         if ($flag) {
             $transaction->commit();
             //return $this->redirect(['view', 'id' => $model->id]);
             Yii::$app->getSession()->setFlash('success', 'Data karyawan tersimpan');
            return $this->redirect(['index']);
         }
     } catch (Exception $e) {
         $transaction->rollBack();
     }
 }


/*
      if (!$model->save()) {
       Yii::$app->getSession()->setFlash('error', 'jabatan harus diisi');
      return $this->redirect(['index']);
     } else {

     }
     */



    } else {
      Yii::$app->getSession()->setFlash('warning', 'NOT AUTHORIZED');
       //    return $this->redirect(['index']);
       return $this->render('create', [
           'model' => $model,
           'department' => $department,
           'employment' => $employment,
           'employeeprofile' => $employeeprofile,
           'employeeidentification' => $employeeidentification,
           'employeecontact' => $employeecontact,
           'employmentsig' => $employmentsig,
             'employeebanks' => (empty($employeebanks)) ? [new EmployeeBank] : $employeebanks,
        //   'modelBank' => (empty($modelBank)) ? [new EmployeeBank] : $modelBank,
       ]);
        //  echo  $this->renderAjax('_authorization',['model'=>$model]);
    }

        } else {
            return $this->render('create', [
                'model' => $model,
                'department' => $department,
                'employment' => $employment,
                'employeeprofile' => $employeeprofile,
                'employeeidentification' => $employeeidentification,
                'employeecontact' => $employeecontact,
                'employmentsig' => $employmentsig,
                  'employeebanks' => (empty($employeebanks)) ? [new EmployeeBank] : $employeebanks,
             //   'modelBank' => (empty($modelBank)) ? [new EmployeeBank] : $modelBank,
            ]);
        }

       } else {
        Yii::$app->getSession()->setFlash('error', 'You don\'t have permission');

        //return $this->redirect(\Yii::$app->request->getReferrer());

         return $this->redirect('../../sig/default/datakaryawan');
        }

    }

    /**
     * Updates an existing Employee model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
       $idtosave = new EmployeeIdentification();
      $contactstosave = new EmployeeContact();

       /**VALUE-VALUE INI SELALU ADA**/
       $departmentrole = DepartmentRole::find()->where(['id' => $model->department_role_id])->one();
       $department = $departmentrole->department;
       $departmentall=Department::find()->all();
       $departmentroleall=DepartmentRole::find()->all();
       $dlist=ArrayHelper::map($departmentall,'id','department_name');
       $drlist=ArrayHelper::map($departmentroleall,'id','department_role_name');

       /**VALUE-VALUE INI BELUM TENTU ADA**/
        if(null !== $employment=Employment::find()->where(['employee_id' => $model->id])
    ->andWhere(['status'=>'active'])
        ->One()){
        } else {
         $employment = new Employment();
        }

        if(null !== $employeeprofile = $model->employeeProfiles){
        } else {
                 $employeeprofile = new EmployeeProfile();
        }

        if(sizeof($employeeidentification=EmployeeIdentification::find()->where(['employee_id' => $model->id])->All()) > 0)
        {

        }else {
             $employeeidentification = [new EmployeeIdentification()];
        }
        if(sizeof($employeecontact=EmployeeContact::find()->where(['employee_id' => $model->id])->All()) > 0 )  {
        } else {
                 $employeecontact = [new EmployeeContact()];
        }


    if (null !== $employmentsig = EmploymentSig::find()->andWhere(['employee_id' => $model->id])
    ->one()) {
//echo '<br/><br/><br/>ninaninaninaninaninaninia';
    } else {
         $employmentsig = new EmploymentSig();
    }
        $employeebanks=EmployeeBank::find()->where(['employee_id' => $model->id])->All();


        if ($model->load(Yii::$app->request->post())
        && $employment->load(Yii::$app->request->post())
        && $employmentsig->load(Yii::$app->request->post())
        && $employeeidentification[0]->load(Yii::$app->request->post())
        && $employeeprofile->load(Yii::$app->request->post())
        && $idtosave->load(Yii::$app->request->post())
        && $contactstosave->load(Yii::$app->request->post())
        ) {

           echo '<pre>';

           print_r(Yii::$app->request->post());
           echo '</pre>';

         $oldIDs = ArrayHelper::map($employeebanks, 'id', 'id');
         $employeebanks = Model::createMultiple(EmployeeBank::classname(), $employeebanks);
         Model::loadMultiple($employeebanks, Yii::$app->request->post());
         $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($employeebanks, 'id', 'id')));

         if (Yii::$app->request->isAjax) {
          echo 'IS AJAX';
             Yii::$app->response->format = Response::FORMAT_JSON;
             return ArrayHelper::merge(
                 ActiveForm::validateMultiple($employeebanks),
                 ActiveForm::validate($model)
             );
         }

                  $model->save();
                   $employeeprofile->employee_id = $model->id;
                  $employeeprofile->save();
                   $employment->employee_id = $model->id;
                   $employment->status = 'active';
                  $employment->save();
                  $employmentsig->employee_id = $model->id;
                  $employmentsig->save();
                       $employeeidentification[0]->employee_id = $model->id;
                  $employeeidentification[0]->save();

                  $i = 0;
                 foreach ($employeeidentification as $emp_id){
                   $emp_id->identification_detail = $idtosave->identification_detail[$i];
                   $emp_id->save();
                   $i++;
                  }

                  $i = 0;
                 foreach ($employeecontact as $emp_id){
                   $emp_id->contact_detail = $contactstosave->contact_detail[$i];
                   $emp_id->save();
                   $i++;
                  }
                  $i = 0;

         $valid = $model->validate();
         $valid = Model::validateMultiple($employeebanks) && $valid;

         if ($valid) {

          echo '<br/><br/><br/><br/>';
          echo 'VALID';
                                   $transaction = \Yii::$app->db->beginTransaction();
                                   try {
                                       if ($flag = $model->save(false)) {
                                           if (! empty($deletedIDs)) {
                                               EmployeeBank::deleteAll(['id' => $deletedIDs]);
                                           }
                                           foreach ($employeebanks as $employeebank) {
                                            echo '<br/><br/><br/><br/>';
                                            echo 'INI SIMPAN BANK';
                                               $employeebank->employee_id = $model->id;
                                               if (! ($flag = $employeebank->save(false))) {
                                                   $transaction->rollBack();
                                                   break;
                                               }
                                           }
                                       }
                                       if ($flag) {
                                           $transaction->commit();
                                                  Yii::$app->getSession()->setFlash('success', 'Data karyawan terupdate');
                                           return $this->redirect(['index']);
                                       }
                                   } catch (Exception $e) {
                                       $transaction->rollBack();
                                   }
                               } else {
                                echo '<br/><br/><br/><br/>';
                                echo 'NO BANKS BANK';

                               }

        } else {

            return $this->render('update', [
                'model' => $model,
                'departmentrole' => $departmentrole,
                'department' => $department,
                'departmentall' => $dlist,
                'employment' => $employment,
                'employeeprofile' => $employeeprofile,
                'employeeidentification' => $employeeidentification,
                'employeecontact' => $employeecontact,
                 'employeebanks' => (empty($employeebanks)) ? [new EmployeeBank] : $employeebanks,
                'employmentsig' => $employmentsig,
            ]);

        }
    }

    /**
     * Deletes an existing Employee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        if (true) {
        //if (\Yii::$app->user->can('createGolongan')) {
         $model = $this->findModel($id);
         $model->status = 'inactive';
         $model->save();
} else {

Yii::$app->getSession()->setFlash('error', 'You don\'t have permission');
}
return $this->redirect(['index']);


    }

    public function actionFinishcontract($id){
      $employment = Employment::findOne($id);
      $employment->status = 'inactive';
      $employment->save();
      Yii::$app->getSession()->setFlash('success', 'Kontrak selesai');
      return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Employee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Employee the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Employee::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public static function getDeparments()
    {
     $data=  Department::find()->all();
     $value=(count($data)==0)? [''=>'']: \yii\helpers\ArrayHelper::map($data, 'id','department_name');

    return $value;
    }

    public static function getRolesbyDepartments($dept_id)
    {
     $data = DepartmentRole::find()->where(['department_id'=>$dept_id])->select(['id','department_role_name AS name'])->asArray()->all();
     $value = (count($data) == 0) ? ['' => ''] : $data;

     return $value;

    }

    public static function actionRoles()
    {

     $out = [];
if (isset($_POST['depdrop_parents'])) {
$parents = $_POST['depdrop_parents'];

    if ($parents != null) {
        $cat_id = $parents[0];
        $out = self::getRolesbyDepartments($cat_id);
        echo Json::encode(['output' => $out, 'selected' => '']);
return;
    }
}
echo Json::encode(['output' => '', 'selected' => '']);


    }



}
