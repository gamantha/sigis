<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\organization\models\Employee */

$this->title = Yii::t('app', 'Create Employee');


$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Karyawan'), 'url' => ['/sig/default/datakaryawan']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'department' => $department,
        'departmentrole' => $departmentrole,
        'departmentall' => $departmentall,
        'employment' => $employment,
        'employeeprofile' => $employeeprofile,
        'employeeidentification' => $employeeidentification,
        'employeecontact' => $employeecontact,
        'employeebank' => $employeebank,
        'employmentsig' => $employmentsig,
    ]) ?>

</div>
