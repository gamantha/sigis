<?php

use yii\helpers\Html;
use yii\jui\Datepicker;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use app\modules\organization\models\Employee;
use app\modules\organization\models\EmployeeSearch;
use app\modules\organization\models\EmployeeBank;
use app\modules\organization\models\EmployeeContact;
use app\modules\organization\models\EmployeeIdentification;
use app\modules\organization\models\EmployeeProfile;
use app\modules\organization\models\Employment;
use app\modules\organization\models\EmploymentSig;
use app\modules\organization\models\Department;
use app\modules\organization\models\DepartmentRole;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\modules\organization\models\Employee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employee-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'org_id')->textInput(['maxlength' => true])->label('NIK') ?>

   <?= $form->field($department, 'id')->dropDownList($departmentall, ['id' => 'dept_id', 'class'=>'input-large form-control','prompt'=>'Select...'])->label('Deparment'); ?>

    <?= $form->field($model, 'department_role_id')->widget(DepDrop::classname(), [
    'options'=>['id'=>'role_id', 'class'=>'input-large form-control'],
    'pluginOptions'=>[
        'depends'=>['dept_id'],
        'placeholder'=>'Select...',
        'url'=>Url::to(['/organization/employee/roles'])
    ]
])->label('Jabatan');
    ?>

    <?= $form->field($employment, 'first_day')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
    ])->label('Hari Pertama Kerja') ?>


    	   <?= $form->field($employment, 'contract_type')->dropDownList([ 'percobaan' => 'Percobaan', 'kontrak' => 'Kontrak', 'tetap' => 'Tetap',], ['prompt' => '']) ?>


    <?= $form->field($employment, 'start')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
    ])->label('Awal Periode') ?>
    <?= $form->field($employment, 'finish')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
     'options' => ['class' => 'form-control','maxlength' => '255',],
    ])->label('Akhir Periode') ?>

    <h3>IDENTITAS KARYAWAN</h3>
    <div class="well">
    <?= $form->field($employeeprofile, 'first_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($employeeprofile, 'last_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($employeeprofile, 'birth_place')->textInput(['maxlength' => true]) ?>
    <?= $form->field($employeeprofile, 'birth_date')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
   </div>

<h3>IDENTIFIKASI</h3>
<div class="well">
    <?= $form->field($employeeidentification, 'identification_detail[]')->textInput( ['id' => 'ktp_id', 'class'=>'input-large form-control', 'maxlength' => true])->label('Nomor KTP') ?>

    <?= $form->field($employeeidentification, 'identification_detail[]')->textInput(['id' => 'sim_id', 'class'=>'input-large form-control', 'maxlength' => true])->label('Nomor SIM') ?>

    <?= $form->field($employeeidentification, 'identification_detail[]')->textInput(['id' => 'paspor_id', 'class'=>'input-large form-control', 'maxlength' => true])->label('Nomor Paspor') ?>



    <?= $form->field($employeeidentification, 'identification_detail[]')->textInput(['maxlength' => true])->label('Nomor NPWP') ?>

    <?= $form->field($employeeidentification, 'identification_detail[]')->textInput(['maxlength' => true])->label('Nomor KPJ') ?>

        	   <?= $form->field($employmentsig, 'value')->radioList([ 'k3' => 'K3', 'k2' => 'K2', 'k1' => 'K1', 'k0'=>'K0','tk'=>'TK',], ['prompt' => '']) ?>

       </div>

<h3>INFORMASI CONTACT</h3>
<div class="well">

    <?= $form->field($employeecontact, 'contact_detail[]')->textInput(['maxlength' => true])->label('Alamat Rumah') ?>
        <?= $form->field($employeecontact, 'contact_detail[]')->textInput(['maxlength' => true])->label('Telepon Rumah') ?>
            <?= $form->field($employeecontact, 'contact_detail[]')->textInput(['maxlength' => true])->label('No HP #1') ?>
                <?= $form->field($employeecontact, 'contact_detail[]')->textInput(['maxlength' => true])->label('No HP #2') ?>
    <?= $form->field($employeecontact, 'contact_detail[]')->textInput(['maxlength' => true])->label('Alamat Email') ?>

</div>
<h3>INFORMASI BANK</h3>
    <div class="well">
 <p><strong>BCA</strong></p>
    <?= $form->field($employeebank, 'account_name[]')->textInput(['maxlength' => true])->label('Nama Account') ?>
    <?= $form->field($employeebank, 'account_number[]')->textInput(['maxlength' => true])->label('No Account') ?>
 <p><strong>BTN</strong></p>
    <?= $form->field($employeebank, 'account_name[]')->textInput(['maxlength' => true])->label('Nama Account') ?>
    <?= $form->field($employeebank, 'account_number[]')->textInput(['maxlength' => true])->label('No Account') ?>
   </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
