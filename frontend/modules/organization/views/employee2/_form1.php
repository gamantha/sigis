<?php

use yii\helpers\Html;
use yii\jui\Datepicker;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use app\modules\organization\models\Department;
use app\modules\organization\models\DepartmentRole;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\sig\models\Golongan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="golongan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $department=Department::find()->all();
     $departmentrole=DepartmentRole::find()->all();
      $department=ArrayHelper::map($department,'id','department_name');
         $departmentrole=ArrayHelper::map($departmentrole,'id','department_role_name');
    echo $form->field($model, 'id')->textInput(['readonly' => true]);

$departmentrole =  DepartmentRole::find()->where(['id' =>$model->department_role_id])->one();
    ?>
    <?= $form->field($department, 'id')
->dropDownList($deptsListData, ['id' => 'dept_id', 'class'=>'input-large form-control','prompt'=>'Select...', 'selected' => '2' ])
    //->textInput(['readonly' => false, 'value' => $rol->department->department_name])

    ->label('Department') ?>


    <?= $form->field($departmentrole, 'id')->widget(DepDrop::classname(), [
    'options'=>['id'=>'role_id', 'class'=>'input-large form-control'],
     'data'=>[$departmentrole->id => $departmentrole->department_role_name],
    'pluginOptions'=>[
        'depends'=>['dept_id'],
        'placeholder'=>'Select...',
        'url'=>Url::to(['/organization/employee/roles'])
    ]
])->label('Jabatan');
    ?>



    <?= $form->field($model, 'masa_kerja_minimal')->textInput() ?>

    <?= $form->field($model, 'tunjangan_makan_transport')->textInput() ?>

    <?= $form->field($model, 'tunjangan_premi_hadir')->textInput() ?>

    <?= $form->field($model, 'tunjangan_bonus_hadir')->textInput() ?>

    <?= $form->field($model, 'tunjangan_lembur')->textInput() ?>

    <?= $form->field($model, 'tunjangan_lembur_luarkota')->textInput() ?>

    <?= $form->field($model, 'denda_keterlambatan')->textInput() ?>

    <?= $form->field($model, 'plafon_kasbon')->textInput() ?>

    <?= $form->field($model, 'plafon_kesehatan')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
