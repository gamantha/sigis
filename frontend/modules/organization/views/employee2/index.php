<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\organization\models\Employee;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\organization\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Report Karyawan');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Karyawan'), 'url' => ['/sig/default/datakaryawan']];

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
    
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'org_id',
            'department_role_id',
            [
             'label' => 'Department ',
             'contentOptions' =>['class' => 'table_class'],
             'value'=>function($data) {
   return ($data->departmentRole->department->department_name);
                },
            ],
            [
             'label' => 'Tanggal Masuk ',
             'contentOptions' =>['class' => 'table_class'],
             'value'=>function($data) {
   return($data->employments->first_day);
                },
            ],
            [
             'label' => 'Mulai Kontrak ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
   return ($data->employments->start);
                },
            ],
            [
             'label' => 'Habis Kontrak ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
   return ($data->employments->finish);
                },
            ],
            [
             'label' => 'Jenis Kontrak ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
   return ($data->employments->contract_type);
                },
            ],
            [
             'label' => 'First Name ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
   return ($data->employeeProfiles->first_name);
                },
            ],
            [
             'label' => 'Last Name ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
   return ($data->employeeProfiles->last_name);
                },
            ],
            [
             'label' => 'Tanggal Lahir ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
   return ($data->employeeProfiles->birth_date);
                },
            ],
            [
             'label' => 'Tempat Lahir ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
   return ($data->employeeProfiles->birth_place);
                },
            ],
            'status',
            
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

