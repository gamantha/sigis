<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\organization\models\DepartmentRole */

$this->title = Yii::t('app', 'Create Department Role');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Department Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-role-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
