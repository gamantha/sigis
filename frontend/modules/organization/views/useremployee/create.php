<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\organization\models\Useremployee */

$this->title = Yii::t('app', 'Create Useremployee');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Useremployees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="useremployee-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
