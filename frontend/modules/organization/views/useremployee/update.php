<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\organization\models\Useremployee */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Useremployee',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Useremployees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="useremployee-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
