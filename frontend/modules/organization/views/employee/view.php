<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\organization\models\DepartmentRole;

/* @var $this yii\web\View */
/* @var $model app\modules\organization\models\Employee */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Employees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'departmentrole' => $departmentrole,
        'department' => $department,
        'employment' => $employment,
        'employeeprofile' => $employeeprofile,
        'employeeidentification' => $employeeidentification,
        'employeecontact' => $employeecontact,
        'employeebank' => $employeebank,
        'attributes' => [
            'id',
            'org_id',
            'department_role_id',
            'department_role_name',
            'first_day',
            'start',
            'finish',
            'contract_type',
            'first_name',
            'last_name',
            'identification_type',
            'identification_detail',
            'birth_date',
            'birth_place',
            'contact_type',
            'contact_detail',
            'bank_name',
            'account_name',
            'account_number',
            'status',

        ],
    ]) ?>

</div>
