<?php

use yii\helpers\Html;
use yii\jui\Datepicker;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use app\modules\organization\models\Employee;
use app\modules\organization\models\EmployeeSearch;
use app\modules\organization\models\EmployeeBank;
use app\modules\organization\models\EmployeeContact;
use app\modules\organization\models\EmployeeIdentification;
use app\modules\organization\models\EmployeeProfile;
use app\modules\organization\models\Employment;
use app\modules\organization\models\EmploymentSig;
use app\modules\organization\models\Department;
use app\modules\organization\models\DepartmentRole;
use yii\helpers\Url;
use kartik\widgets\DepDrop;
/* @var $this yii\web\View */
/* @var $model app\modules\organization\models\Employee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employee-form">
 <?php

    $form = ActiveForm::begin(['id' => 'dynamic-form']);
     ?>

     <div class="well">
<?php
$this->registerJs('

$("#authorize_button").click(function(){
 //alert("sasd");
$("#submit_button").show();
$("#cred_div").show();
$(this).hide();
});

$(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
    console.log("beforeInsert");
});

$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    console.log("afterInsert");
});

$(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
    if (! confirm("Are you sure you want to delete this item?")) {
        return false;
    }
    return true;
});

$(".dynamicform_wrapper").on("afterDelete", function(e) {
    console.log("Deleted item!");
});

$(".dynamicform_wrapper").on("limitReached", function(e, item) {
    alert("Limit reached");
});

', \yii\web\View::POS_END);

$departments=Department::find()->all();
//    $roles=DepartmentRole::find()->all();
  $departments=ArrayHelper::map($departments,'id','department_name');

 ?>




    <?= $form->field($model, 'org_id')->textInput(['maxlength' => true])->label('NIK') ?>

   <?= $form->field($department, 'id')->dropDownList($departments, ['id' => 'dept_id', 'class'=>'input-large form-control','prompt'=>'Select...'])->label('Department'); ?>

    <?= $form->field($model, 'department_role_id')->widget(DepDrop::classname(), [
    'options'=>['id'=>'role_id', 'class'=>'input-large form-control'],
    'pluginOptions'=>[
        'depends'=>['dept_id'],
        'placeholder'=>'Select...',
        'url'=>Url::to(['/organization/employee/roles'])
    ]
])->label('Jabatan');
    ?>

    <?= $form->field($employment, 'first_day')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','maxlength' => '255',],
    ])->label('Hari Pertama Kerja') ?>


    <?= $form->field($employment, 'contract_type')->dropDownList([ 'percobaan' => 'Percobaan', 'kontrak' => 'Kontrak', 'tetap' => 'Tetap',], ['prompt' => ''])->label('Tipe Kontrak') ?>


    <?= $form->field($employment, 'start')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','maxlength' => '255',],
    ])->label('Awal Periode') ?>

    <?= $form->field($employment, 'finish')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
     'options' => ['class' => 'form-control','maxlength' => '255',],
    ])->label('Akhir Periode') ?>

    <h3>IDENTITAS KARYAWAN</h3>
    <div class="well">
    <?= $form->field($employeeprofile, 'first_name')->textInput(['maxlength' => true])->label('Nama Depan') ?>
    <?= $form->field($employeeprofile, 'last_name')->textInput(['maxlength' => true])->label('Nama Belakang') ?>
    <?= $form->field($employeeprofile, 'birth_place')->textInput(['maxlength' => true])->label('Tempat Lahir') ?>
    <?= $form->field($employeeprofile, 'birth_date')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
      'options' => ['class' => 'form-control','maxlength' => '255',],
    ])->label('Tanggal Lahir') ?>
    <?= $form->field($employeeidentification, 'identification_type')->dropDownList([ 'ktp' => 'KTP', 'sim' => 'SIM', 'paspor' => 'PASPOR',], ['prompt' => ''])->label('Tipe Identifikasi') ?>

    <?= $form->field($employeeidentification, 'identification_detail[]')->textInput(['maxlength' => true])->label('Nomor Identifikasi') ?>
    <?= $form->field($employeeidentification, 'identification_detail[]')->textInput(['maxlength' => true])->label('Nomor NPWP') ?>

    <?= $form->field($employeeidentification, 'identification_detail[]')->textInput(['maxlength' => true])->label('Nomor KPJ') ?>
    <?= $form->field($employmentsig, 'status')->radioList([ 'K3' => 'K3', 'K2' => 'K2', 'K1' => 'K1', 'K0'=>'K0','TK'=>'TK',], ['prompt' => '']) ?>
   </div>



<h3>INFORMASI CONTACT</h3>
<div class="well">

    <?= $form->field($employeecontact, 'contact_detail[]')->textInput(['maxlength' => true])->label('Alamat Rumah') ?>
        <?= $form->field($employeecontact, 'contact_detail[]')->textInput(['maxlength' => true])->label('Telepon Rumah') ?>
            <?= $form->field($employeecontact, 'contact_detail[]')->textInput(['maxlength' => true])->label('No HP #1') ?>
                <?= $form->field($employeecontact, 'contact_detail[]')->textInput(['maxlength' => true])->label('No HP #2') ?>
    <?= $form->field($employeecontact, 'contact_detail[]')->textInput(['maxlength' => true])->label('Alamat Email') ?>

</div>




<div class="well">

 <div class="row">

     <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Banks</h4></div>
<div class="panel-body">

<?php DynamicFormWidget::begin([
'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
'widgetBody' => '.container-items', // required: css class selector
'widgetItem' => '.item', // required: css class
'limit' => 4, // the maximum times, an element can be cloned (default 999)
'min' => 1, // 0 or 1 (default 1)
'insertButton' => '.add-item', // css class
'deleteButton' => '.remove-item', // css class
'model' => $employeebanks[0],
'formId' => 'dynamic-form',
'formFields' => [
  'employee_id',
  'bank_name',
],
]); ?>
<div class="container-items">
<?php foreach ($employeebanks as $i => $employeebank): ?>

<div class="item panel panel-default"><!-- widgetBody -->
  <div class="panel-heading">
      <h3 class="panel-title pull-left">Bank</h3>
      <div class="pull-right">
          <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
          <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
      </div>
      <div class="clearfix"></div>
  </div>
  <div class="panel-body">

   <?php
       // necessary for update action.
       if (! $employeebank->isNewRecord) {
           echo Html::activeHiddenInput($employeebank, "[{$i}]id");
       }



echo $form->field($employeebank, "[{$i}]bank_name")->dropDownList([ 'bca' => 'BCA', 'btn' => 'BTN',], ['class'=>'input-large form-control','prompt'=>'Select...'])->label('Department');
?>
   <div class="row">
       <div class="col-sm-6">
           <?= $form->field($employeebank, "[{$i}]account_name")->textInput(['maxlength' => true]) ?>
       </div>
       <div class="col-sm-6">
           <?= $form->field($employeebank, "[{$i}]account_number")->textInput(['maxlength' => true]) ?>
       </div>
   </div><!-- .row -->

  </div>
</div>



<?php endforeach; ?>

</div>

     </div>
     </div>
 </div>




<?php DynamicFormWidget::end(); ?>






<div id='cred_div' style="display:none;">
       <?= 'Username  ' . Html::textInput('username','',['class'=>'form-control']) ?>
            <?= '<br/>Password ' . Html::textInput('password','',['class'=>'form-control']) . "<br/>"?>
</div>

    <div class="form-group">
     <?php

                      //echo Html::buttonInput('Submit',['class' => 'btn btn-success', 'id' => 'authorize_button']);
             echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['id'=>'submit_button','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ]);
     ;

                      ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


