 <?php

use yii\helpers\Html;
use yii\jui\Datepicker;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use app\modules\organization\models\Employee;
use app\modules\organization\models\EmployeeSearch;
use app\modules\organization\models\EmployeeBank;
use app\modules\organization\models\EmployeeContact;
use app\modules\organization\models\EmployeeIdentification;
use app\modules\organization\models\EmployeeProfile;
use app\modules\organization\models\Employment;
use app\modules\organization\models\EmploymentSig;
use app\modules\organization\models\Department;
use app\modules\organization\models\DepartmentRole;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\grid\GridView;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\modules\organization\models\Employee */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="employee-form">
 <?php

    $form = ActiveForm::begin(['id' => 'dynamic-form']);
     ?>
<div class="well">
    <?php

    $this->registerJs('

    $("#authorize_button").click(function(){
    $("#submit_button").show();
    $("#cred_div").show();
    $(this).hide();
    });

    $(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
        console.log("beforeInsert");

    });

    $(".dynamicform_wrapper").on("afterInsert", function(e, item) {
        console.log("afterInsert");
    });

    $(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
        if (! confirm("Are you sure you want to delete this item?")) {
            return false;
        }
        return true;
    });

    $(".dynamicform_wrapper").on("afterDelete", function(e) {
        console.log("Deleted item!");
    });

    $(".dynamicform_wrapper").on("limitReached", function(e, item) {
        alert("Limit reached");
    });

    ', \yii\web\View::POS_END);

    //$form = ActiveForm::begin();


    ?>

    <?= $form->field($model, 'org_id')->textInput(['maxlength' => true])->label('NIK') ?>

   <?= $form->field($department, 'id')->dropDownList($departmentall, ['id' => 'dept_id', 'class'=>'input-large form-control','prompt'=>'Select...'])->label('Department'); ?>

    <?= $form->field($model, 'department_role_id')->widget(DepDrop::classname(), [
    'options'=>['id'=>'role_id', 'class'=>'input-large form-control'],
       'data'=>[$departmentrole->id => $departmentrole->department_role_name],
    'pluginOptions'=>[
        'depends'=>['dept_id'],
        'placeholder'=>'Select...',
        'url'=>Url::to(['/organization/employee/roles'])
    ]
])->label('Jabatan');
    ?>
    </div>
    <h3>STATUS KARYAWAN</h3>
    <div class="well">
    <?= $form->field($employment, 'first_day')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
         'options' => ['class' => 'form-control','maxlength' => '255',],
    ])->label('Hari Pertama Kerja') ?>




    <?= $form->field($employment, 'contract_type')->dropDownList([ 'percobaan' => 'Percobaan', 'kontrak' => 'Kontrak', 'tetap' => 'Tetap',], ['prompt' => '']) ?>


    <?= $form->field($employment, 'start')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
         'options' => ['class' => 'form-control','maxlength' => '255',],
    ])->label('Awal Periode') ?>
    <?= $form->field($employment, 'finish')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
     'options' => ['class' => 'form-control','maxlength' => '255',],
    ])->label('Akhir Periode') ?>
        <?= $form->field($employmentsig, 'status')->radioList([ 'K3' => 'K3', 'K2' => 'K2', 'K1' => 'K1', 'K0'=>'K0','TK'=>'TK',],
        [
        'itemOptions'=>[
         //'name' => 'k2',
         //'checked' => true,
        ],
        'item' => function($index, $label, $name, $checked, $value) {
                if($checked) {
               $check = 'checked';} else {
                $check = '';
               }

            $return = '<label class="modal-radio">';
            $return .= '<input type="radio" name="' . $name . '" '. $check . ' value="' . $value . '" tabindex="3">';
            $return .= '<i></i>';
            $return .= '<span>' . ucwords($label) . '</span>';
            $return .= '</label>';

            return $return;
        }
        ])?>

        <?php

        $employmentsquery = Employment::find()->andWhere(['employee_id' =>$_REQUEST['id']])->andWhere(['in','status',['active','inactive']]);

        $employmentdp = new ActiveDataProvider([
            'query' => $employmentsquery,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        echo GridView::widget([
            'dataProvider' => $employmentdp,
          //  'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'contract_type',
                'start',
                'finish',
                'status',
                [
                 'class' => 'yii\grid\ActionColumn',
                 'controller' => 'EmployeeController',
                 'template' => '{FinishContract}',
                 'buttons' => [
                   'FinishContract' => function($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => 'FinishContract',
                   'data-confirm' => 'are you sure?']);
                   }
                 ],
                 'urlCreator' => function ($action, $model, $key, $index){
                  $url = 'finishcontract?id=' . $model->id;
                  return $url;
                 }
                ],
               ],
     ]);


         ?>

    </div>

    <h3>IDENTITAS KARYAWAN</h3>
    <div class="well">
    <?= $form->field($employeeprofile, 'first_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($employeeprofile, 'last_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($employeeprofile, 'birth_place')->textInput(['maxlength' => true]) ?>
    <?= $form->field($employeeprofile, 'birth_date')->widget(DatePicker::classname(), [
    'language' => 'en',
    'dateFormat' => 'yyyy-MM-dd',
         'options' => ['class' => 'form-control','maxlength' => '255',],
    ]) ?>
   </div>

<h3>IDENTIFIKASI</h3>
<div class="well">
 <!--?= $form->field($employeeidentification, 'identification_type')->dropDownList([ 'ktp' => 'KTP', 'sim' => 'SIM', 'paspor' => 'PASPOR',], ['prompt' => ''])->label('Tipe Identifikasi') ?-->



    <?php
//echo '<br/><br/>';
    //print_r($employeeidentification);
    //echo '<br/><br/>asa   ;lsakd;ak print';
//

$loop_index = 0;

foreach ($employeeidentification as $emp_id){
 echo $form->field($employeeidentification[$loop_index], 'identification_type')->dropDownList([ 'ktp' => 'KTP', 'sim' => 'SIM', 'paspor' => 'PASPOR',], ['prompt' => '']);
 echo $form->field($employeeidentification[$loop_index], 'identification_detail[]')->textInput( ['class'=>'input-large form-control', 'maxlength' => true, 'value'=>$emp_id['identification_detail']])->label('Nomor ' . $emp_id['identification_type']);

 $loop_index++;
}

 ?>


       </div>

<h3>INFORMASI CONTACT</h3>
<div class="well">



    <?php

    $loop_index = 0;
    foreach ($employeecontact as $emp_id){
     echo $form->field($employeecontact[$loop_index], 'contact_detail[]')->textInput( ['class'=>'input-large form-control', 'maxlength' => true, 'value'=>$emp_id['contact_detail']])->label('Nomor ' . $emp_id['contact_type']);
     $loop_index++;
    }
     ?>




</div>

    <div class="well">

     <div class="row">

         <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Banks</h4></div>
<div class="panel-body">

 <?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
    'widgetBody' => '.container-items', // required: css class selector
    'widgetItem' => '.item', // required: css class
    'limit' => 4, // the maximum times, an element can be cloned (default 999)
    'min' => 1, // 0 or 1 (default 1)
    'insertButton' => '.add-item', // css class
    'deleteButton' => '.remove-item', // css class
    'model' => $employeebanks[0],
    'formId' => 'dynamic-form',
    'formFields' => [
      'employee_id',
      'bank_name',
    ],
]); ?>
 <div class="container-items">
 <?php foreach ($employeebanks as $i => $employeebank): ?>

  <div class="item panel panel-default"><!-- widgetBody -->
      <div class="panel-heading">
          <h3 class="panel-title pull-left">Bank</h3>
          <div class="pull-right">
              <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
              <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
          </div>
          <div class="clearfix"></div>
      </div>
      <div class="panel-body">

       <?php
           // necessary for update action.
           if (! $employeebank->isNewRecord) {
               echo Html::activeHiddenInput($employeebank, "[{$i}]id");
           }
       ?>
       <?= $form->field($employeebank, "[{$i}]bank_name")->dropDownList([ 'bca' => 'BCA', 'btn' => 'BTN',], ['class'=>'input-large form-control','prompt'=>'Select...'])->label('Department') ?>
       <div class="row">
           <div class="col-sm-6">
               <?= $form->field($employeebank, "[{$i}]account_name")->textInput(['maxlength' => true]) ?>
           </div>
           <div class="col-sm-6">
               <?= $form->field($employeebank, "[{$i}]account_number")->textInput(['maxlength' => true]) ?>
           </div>
       </div><!-- .row -->

      </div>
  </div>



<?php endforeach; ?>

 </div>

         </div>
         </div>
     </div>

<?php DynamicFormWidget::end(); ?>
   <div id='cred_div' style="display:none;">
          <?= 'Username  ' . Html::textInput('username','',['class'=>'form-control']) ?>
               <?= '<br/>Password ' . Html::textInput('password','',['class'=>'form-control']) . "<br/>"?>
   </div>

    <div class="form-group">
<?php
                //if (\Yii::$app->user->can('createGolongan')) {
        //echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);

        //echo Html::buttonInput('Submit',['class' => 'btn btn-success', 'id' => 'authorize_button']);
echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
['id'=>'submit_button','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
                 ?>

    </div>
    <?php ActiveForm::end(); ?>

</div>

