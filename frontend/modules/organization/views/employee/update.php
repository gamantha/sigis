<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\organization\models\Employee */

$this->title = Yii::t('app', 'Update Data Karyawan: ', [
    'modelClass' => 'Employee',
]) . ' ' . $model->org_id ;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Employees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="employee-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_updateform', [
        'model' => $model,
        'department' => $department,
        'departmentrole' => $departmentrole,
        'departmentall' => $departmentall,
        'employment' => $employment,
        'employeeprofile' => $employeeprofile,
        'employeeidentification' => $employeeidentification,
        'employeecontact' => $employeecontact,
        'employeebanks' => $employeebanks,
        'employmentsig' => $employmentsig,
    ]) ?>

</div>