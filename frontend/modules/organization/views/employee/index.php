<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\organization\models\Employee;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\organization\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Report Karyawan');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Karyawan'), 'url' => ['/sig/default/datakaryawan']];

$this->params['breadcrumbs'][] = $this->title;
?>

<?php
        Modal::begin([
                'header'=>'<h4>Branches</h4>',
                'id'=>'modal',
                'size'=>'modal-lg',
            ]);
        echo "<div id='modalContent'></div>";
        Modal::end();
?>

<div class="employee-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

      //      'id',
            'org_id',
    //        'department_role_id',
            [
             'label' => 'Department ',
             'contentOptions' =>['class' => 'table_class'],

             'value'=>function($data) {
        return isset($data->departmentRole->department->department_name) ? ($data->departmentRole->department->department_name):"";
   //return ($data->departmentRole->department->department_name);
                },
            ],
            [
             'label' => 'Tanggal Masuk ',
             'contentOptions' =>['class' => 'table_class'],
             'value'=>function($data) {
           return isset($data->employments->first_day) ? (date('Y-m-d', strtotime($data->employments->first_day))):"";
   //return(date('Y-m-d', strtotime($data->employments->first_day)));
                },
            ],
            [
             'label' => 'Mulai Kontrak ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
   return isset($data->employments->start) ? (date('Y-m-d', strtotime($data->employments->start))):"";
    //return(date('Y-m-d', strtotime($data->employments->start)));
                },
            ],
            [
             'label' => 'Habis Kontrak ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
    return isset($data->employments->finish) ? (date('Y-m-d', strtotime($data->employments->finish))):"";
   //return(date('Y-m-d', strtotime($data->employments->finish)));
                },
            ],
            [
             'label' => 'Jenis Kontrak ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
    return isset($data->employments->contract_type) ? ($data->employments->contract_type):"";
   //return ($data->employments->contract_type);
                },
            ],
            [
             'label' => 'First Name ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
    return isset($data->employeeProfiles->first_name) ? ($data->employeeProfiles->first_name):"";
   //return ($data->employeeProfile->first_name);
                },
            ],
            [
             'label' => 'Last Name ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
    return isset($data->employeeProfiles->last_name) ? ($data->employeeProfiles->last_name):"";
   //return ($data->employeeProfile->last_name);
                },
            ],
            [
             'label' => 'Tanggal Lahir ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
    return isset($data->employeeProfiles->birth_date) ? (date('Y-m-d', strtotime($data->employeeProfiles->birth_date))):"";
      //return(date('Y-m-d', strtotime($data->employeeProfile->birth_date)));
                },
            ],
            [
             'label' => 'Tempat Lahir ',
                'contentOptions' =>['class' => 'table_class'],
              'value'=>function($data) {
   return isset($data->employeeProfiles->birth_place) ? ($data->employeeProfiles->birth_place):"";
                  //return ($data->employeeProfile->birth_place);
                },
            ],
            'status',

//            ['class' => 'yii\grid\ActionColumn'],

            ['class' => 'yii\grid\ActionColumn',
                       'template'=>'{update}{delete}',

                         ],



        ],
    ]); ?>

</div>

