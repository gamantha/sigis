<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\modules\organization\models\Employee */

$this->title = Yii::t('app', 'Input Data Karyawan');


$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sig'), 'url' => ['/sig/default']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Karyawan'), 'url' => ['/sig/default/datakaryawan']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="employee-form">
    <?php


     ?>
        <?php $form = ActiveForm::begin(); ?>


        <div class="form-group">
    <?php

            echo Html::submitButton('Accept',['name'=>'buttonaccept']);

                     ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
