<?php

namespace app\modules\organization\models;

/**
 * This is the ActiveQuery class for [[DepartmentSuperiority]].
 *
 * @see DepartmentSuperiority
 */
class DepartmentSuperiorityQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return DepartmentSuperiority[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DepartmentSuperiority|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}