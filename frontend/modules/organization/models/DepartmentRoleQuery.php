<?php

namespace app\modules\organization\models;

/**
 * This is the ActiveQuery class for [[DepartmentRole]].
 *
 * @see DepartmentRole
 */
class DepartmentRoleQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return DepartmentRole[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DepartmentRole|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}