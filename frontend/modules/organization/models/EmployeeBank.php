<?php

namespace app\modules\organization\models;

use Yii;

/**
 * This is the model class for table "employee_bank".
 *
 * @property integer $id
 * @property integer $employee_id
 * @property string $bank_name
 * @property string $account_name
 * @property string $account_number
 *
 * @property Employee $employee
 */
class EmployeeBank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee_bank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id'], 'integer'],
            [['bank_name'], 'string'],
            [['account_name', 'account_number'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'employee_id' => Yii::t('app', 'Employee ID'),
            'bank_name' => Yii::t('app', 'Bank Name'),
            'account_name' => Yii::t('app', 'Account Name'),
            'account_number' => Yii::t('app', 'Account Number'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @inheritdoc
     * @return EmployeeBankQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EmployeeBankQuery(get_called_class());
    }
}
