<?php

namespace app\modules\organization\models;

/**
 * This is the ActiveQuery class for [[EmployeeIdentification]].
 *
 * @see EmployeeIdentification
 */
class EmployeeIdentificationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return EmployeeIdentification[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EmployeeIdentification|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}