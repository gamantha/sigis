<?php

namespace app\modules\organization\models;

use Yii;

/**
 * This is the model class for table "employee_contact".
 *
 * @property integer $id
 * @property integer $employee_id
 * @property string $contact_type
 * @property string $contact_detail
 *
 * @property Employee $employee
 */
class EmployeeContact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee_contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id'], 'integer'],
            [['contact_type'], 'string'],
            [['contact_detail'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'employee_id' => Yii::t('app', 'Employee ID'),
            'contact_type' => Yii::t('app', 'Contact Type'),
            'contact_detail' => Yii::t('app', 'Contact Detail'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @inheritdoc
     * @return EmployeeContactQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EmployeeContactQuery(get_called_class());
    }
}
