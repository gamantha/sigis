<?php

namespace app\modules\organization\models;

use Yii;
use app\modules\sig\models\Golongan;
use app\modules\organization\models\Employee;
use app\modules\organization\models\UserEmployee;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "employee".
 *
 * @property integer $id
 * @property integer $department_role_id
 * @property string $org_id
 * @property string $status
 *
 * @property DepartmentRole $departmentRole
 * @property EmployeeBank[] $employeeBanks
 * @property EmployeeContact[] $employeeContacts
 * @property EmployeeIdentification[] $employeeIdentifications
 * @property EmployeeProfile[] $employeeProfiles
 * @property Employment[] $employments
 * @property EmploymentSig[] $employmentSigs
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['department_role_id'], 'integer'],

                      ['org_id', 'required'],
            [['status'], 'string'],

      //      [['department_role_id','account_number'], 'integer'],
      //      [['first_day','start','finish','birth_date'], 'safe'],
       //     [['status','department_role_name','contract_type','first_name','last_name','identification_type','identification_detail','birth_place','contact_type','contact_detail','bank_name','account_name'], 'string'],

            [['org_id'], 'string', 'max' => 255],
                       ['department_role_id', 'required'],
         //   [['department_role_id','account_number'], 'integer'],
          //  [['first_day','start','finish','birth_date'], 'safe'],
         //   [['status','department_role_name','contract_type','first_name','last_name','identification_type','identification_detail','birth_place','contact_type','contact_detail','bank_name','account_name'], 'string'],
            [['org_id'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'department_role_id' => Yii::t('app', 'Department Role ID'),
            'org_id' => Yii::t('app', 'NIK'),
            'status' => Yii::t('app', 'Status'),
            'first_day' => Yii::t('app', 'First Day'),
            'start' => Yii::t('app', 'Start'),
            'finish' => Yii::t('app', 'Finish'),
            'contract_type' => Yii::t('app', 'Tipe Kontrak'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'identification_type' => Yii::t('app', 'Tipe Identifikasi'),
            'identification_detail' => Yii::t('app', 'Detail Identifikasi'),
            'birth_date' => Yii::t('app', 'Tanggal Lahir'),
            'birth_place' => Yii::t('app', 'Tempat Lahir'),
            'contact_type' => Yii::t('app', 'Tipe Kontak'),
            'contact_detail' => Yii::t('app', 'Detail Kontak'),
            'bank_name' => Yii::t('app', 'Nama Bank'),
            'account_name' => Yii::t('app', 'Nama Akun'),
            'account_number' => Yii::t('app', 'Nomor Akun'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentRole()
    {
        return $this->hasOne(DepartmentRole::className(), ['id' => 'department_role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeBanks()
    {
        return $this->hasMany(EmployeeBank::className(), ['employee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeContacts()
    {
        return $this->hasMany(EmployeeContact::className(), ['employee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeIdentifications()
    {
        return $this->hasMany(EmployeeIdentification::className(), ['employee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeProfiles()
    {
        return $this->hasOne(EmployeeProfile::className(), ['employee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployments()
    {
        return $this->hasOne(Employment::className(), ['employee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmploymentSigs()
    {
        return $this->hasMany(EmploymentSig::className(), ['employee_id' => 'id']);
    }

    public function userEmployee()
    {
     $user_employee_mapping = UserEmployee::find()->andWhere(['user_id' => Yii::$app->user->id])->One();
     if(isset($user_employee_mapping->employee_id)) {
      //echo 'ADA';
      $employee = Employee::findOne($user_employee_mapping->employee_id);
     } else {
      //echo 'NOOOOOOOOOOOOOOOOOOOOOO';
      return false;
     }
     return $employee;
    }
    public function getGolongan($id)
    {
     //$id is employyee_id;
        //return $this->hasMany(EmploymentSig::className(), ['employee_id' => 'id']);


        $employee = parent::findOne($id);
      //  echo '<br/><br/><br/>';
        //echo $employee->departmentRole->id;
        $returns = Golongan::find()
        ->andWhere(['department_role_id' => $employee->departmentRole->id])
        //->andWhere(['status'=>'active'])
        //->andWhere(['masa_kerja_minimal' => ''])
        ->All();


        $retval = new Golongan();
        if(sizeof($returns) > 0){
         foreach ($returns as $return) {
          //echo $return->golongan;
          //echo $employee->employments->first_day;
          $datetime1 = new \DateTime($employee->employments->first_day);
          $dateinterval = new \DateInterval('P1Y2M0D');
          $datetime1->add($dateinterval);
          $now = new \DateTime('now');



  if ($datetime1 < $now) {
   //echo '<br/>already past minimal';
   $interval = $datetime1->diff($now);
   //$retval = $return;
  } else {
   //echo '<br/>now is NOT yet past minimal';
   $interval = $datetime1->diff($now);
      $retval = $return;
  }

  /*echo '<br/>'. $interval->format('%R%a days');
  echo '<br/>'. $interval->format('%R%d days');
  echo '<br/>'. $interval->format('%R%m months');
  echo '<br/>'. $interval->format('%R%y years');
*/

         }


        } else {

        //  echo 'ga ada';
        }

        //$models = Writer::find()->asArray()->all();
 //return ArrayHelper::map($return, 'id', 'golongan');

        //return $return;
       // echo 'golongan : ';
//echo $retval->id;
        return $retval;

        //return $this->hasOne(Golongan::className(), ['department_role_id' => 'id']);

    }

    /**
     * @inheritdoc
     * @return EmployeeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EmployeeQuery(get_called_class());
    }
}