<?php

namespace app\modules\organization\models;

use Yii;

/**
 * This is the model class for table "terminal".
 *
 * @property integer $id
 * @property string $status
 */
class Terminal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'terminal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @inheritdoc
     * @return TerminalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TerminalQuery(get_called_class());
    }
}
