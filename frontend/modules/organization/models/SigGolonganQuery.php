<?php

namespace app\modules\organization\models;

/**
 * This is the ActiveQuery class for [[SigGolongan]].
 *
 * @see SigGolongan
 */
class SigGolonganQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SigGolongan[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SigGolongan|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}