<?php

namespace app\modules\organization\models;

/**
 * This is the ActiveQuery class for [[Useremployee]].
 *
 * @see Useremployee
 */
class UseremployeeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Useremployee[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Useremployee|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}