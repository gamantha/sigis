<?php

namespace app\modules\organization\models;

use Yii;

/**
 * This is the model class for table "employee_identification".
 *
 * @property integer $employee_id
 * @property string $identification_type
 * @property string $identification_detail
 *
 * @property Employee $employee
 */
class EmployeeIdentification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee_identification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id'], 'integer'],
            [['identification_type'], 'string'],
            [['identification_detail'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'employee_id' => Yii::t('app', 'Employee ID'),
            'identification_type' => Yii::t('app', 'Identification Type'),
            'identification_detail' => Yii::t('app', 'Identification Detail'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @inheritdoc
     * @return EmployeeIdentificationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EmployeeIdentificationQuery(get_called_class());
    }
}
