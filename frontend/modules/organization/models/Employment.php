<?php

namespace app\modules\organization\models;

use Yii;

/**
 * This is the model class for table "employment".
 *
 * @property integer $id
 * @property integer $employee_id
 * @property string $first_day
 * @property string $contract_type
 * @property string $start
 * @property string $finish
 *
 * @property Employee $employee
 */
class Employment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id'], 'integer'],
            [['first_day', 'start', 'finish'], 'safe'],
            [['contract_type','status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'employee_id' => Yii::t('app', 'Employee ID'),
            'first_day' => Yii::t('app', 'First Day'),
            'contract_type' => Yii::t('app', 'Contract Type'),
            'start' => Yii::t('app', 'Start'),
            'finish' => Yii::t('app', 'Finish'),
                    'status' => Yii::t('app', 'Status'), 
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @inheritdoc
     * @return EmploymentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EmploymentQuery(get_called_class());
    }
}
