<?php

namespace app\modules\organization\models;

/**
 * This is the ActiveQuery class for [[EmployeeContact]].
 *
 * @see EmployeeContact
 */
class EmployeeContactQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return EmployeeContact[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EmployeeContact|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}