<?php

namespace app\modules\organization\models;

/**
 * This is the ActiveQuery class for [[EmploymentSig]].
 *
 * @see EmploymentSig
 */
class EmploymentSigQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return EmploymentSig[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EmploymentSig|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}