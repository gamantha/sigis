<?php

namespace app\modules\organization\models;

use Yii;

/**
 * This is the model class for table "department_superiority".
 *
 * @property integer $department_role_id
 * @property integer $superior_department_role_id
 *
 * @property DepartmentRole $departmentRole
 * @property DepartmentRole $superiorDepartmentRole
 */
class DepartmentSuperiority extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'department_superiority';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['department_role_id', 'superior_department_role_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'department_role_id' => Yii::t('app', 'Department Role ID'),
            'superior_department_role_id' => Yii::t('app', 'Superior Department Role ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentRole()
    {
        return $this->hasOne(DepartmentRole::className(), ['id' => 'department_role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuperiorDepartmentRole()
    {
        return $this->hasOne(DepartmentRole::className(), ['id' => 'superior_department_role_id']);
    }

    /**
     * @inheritdoc
     * @return DepartmentSuperiorityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DepartmentSuperiorityQuery(get_called_class());
    }
}
