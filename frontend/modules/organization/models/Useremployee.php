<?php

namespace app\modules\organization\models;

use Yii;

/**
 * This is the model class for table "useremployee".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $employee_id
 *
 * @property Employee $employee
 * @property User $user
 */
class Useremployee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'useremployee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'employee_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'employee_id' => Yii::t('app', 'Employee ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return UseremployeeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UseremployeeQuery(get_called_class());
    }
}
