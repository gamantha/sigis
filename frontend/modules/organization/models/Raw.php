<?php

namespace app\modules\organization\models;

use Yii;

/**
 * This is the model class for table "raw".
 *
 * @property integer $id
 * @property integer $terminal_id
 */
class Raw extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'raw';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['terminal_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'terminal_id' => Yii::t('app', 'Terminal ID'),
        ];
    }

    /**
     * @inheritdoc
     * @return DepartmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DepartmentQuery(get_called_class());
    }
}
