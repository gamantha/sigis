<?php

namespace app\modules\organization\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\organization\models\Useremployee;

/**
 * UseremployeeSearch represents the model behind the search form about `app\modules\organization\models\Useremployee`.
 */
class UseremployeeSearch extends Useremployee
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'employee_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Useremployee::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'employee_id' => $this->employee_id,
        ]);

        return $dataProvider;
    }
}
