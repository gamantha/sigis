<?php

namespace app\modules\organization\models;

/**
 * This is the ActiveQuery class for [[Employment]].
 *
 * @see Employment
 */
class EmploymentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Employment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Employment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}