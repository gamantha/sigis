<?php

namespace app\modules\organization\models;

use Yii;

/**
 * This is the model class for table "employment_sig".
 *
 * @property integer $id
 * @property integer $employee_id
 * @property string $status
 *
 * @property Employee $employee
 */
class EmploymentSig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employment_sig';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id'], 'integer'],
               [['status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'employee_id' => Yii::t('app', 'Employee ID'),
            'status' => Yii::t('app', 'Status'),

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @inheritdoc
     * @return EmploymentSigQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EmploymentSigQuery(get_called_class());
    }
}
