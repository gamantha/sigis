<?php

namespace app\modules\organization\models;

use Yii;

/**
 * This is the model class for table "sig_golongan".
 *
 * @property integer $id
 * @property string $golongan
 * @property integer $department_role_id
 * @property string $masa_kerja_minimal
 * @property integer $tunjangan_makan_transport
 * @property integer $tunjangan_premi_hadir
 * @property integer $tunjangan_bonus_hadir
 * @property integer $tunjangan_lembur
 * @property integer $tunjangan_lembur_luarkota
 * @property integer $denda_keterlambatan
 * @property integer $plafon_kasbon
 * @property integer $plafon_kesehatan
 *
 * @property DepartmentRole $departmentRole
 */
class SigGolongan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sig_golongan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'department_role_id', 'tunjangan_makan_transport', 'tunjangan_premi_hadir', 'tunjangan_bonus_hadir', 'tunjangan_lembur', 'tunjangan_lembur_luarkota', 'denda_keterlambatan', 'plafon_kasbon', 'plafon_kesehatan'], 'integer'],
            [['masa_kerja_minimal'], 'safe'],
            [['golongan'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'golongan' => Yii::t('app', 'Golongan'),
            'department_role_id' => Yii::t('app', 'Department Role ID'),
            'masa_kerja_minimal' => Yii::t('app', 'Masa Kerja Minimal'),
            'tunjangan_makan_transport' => Yii::t('app', 'Tunjangan Makan Transport'),
            'tunjangan_premi_hadir' => Yii::t('app', 'Tunjangan Premi Hadir'),
            'tunjangan_bonus_hadir' => Yii::t('app', 'Tunjangan Bonus Hadir'),
            'tunjangan_lembur' => Yii::t('app', 'Tunjangan Lembur'),
            'tunjangan_lembur_luarkota' => Yii::t('app', 'Tunjangan Lembur Luarkota'),
            'denda_keterlambatan' => Yii::t('app', 'Denda Keterlambatan'),
            'plafon_kasbon' => Yii::t('app', 'Plafon Kasbon'),
            'plafon_kesehatan' => Yii::t('app', 'Plafon Kesehatan'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentRole()
    {
        return $this->hasOne(DepartmentRole::className(), ['id' => 'department_role_id']);
    }

    /**
     * @inheritdoc
     * @return SigGolonganQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SigGolonganQuery(get_called_class());
    }
}
