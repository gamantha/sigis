<?php

namespace app\modules\organization\models;

use Yii;

/**
 * This is the model class for table "department_role".
 *
 * @property integer $id
 * @property integer $department_id
 * @property string $department_role_name
 *
 * @property Department $department
 * @property DepartmentSuperiority[] $departmentSuperiorities
 * @property DepartmentSuperiority[] $departmentSuperiorities0
 * @property Employee[] $employees
 * @property SigGolongan[] $sigGolongans
 */
class DepartmentRole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'department_role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['department_id'], 'integer'],
            [['department_role_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'department_id' => Yii::t('app', 'Department ID'),
            'department_role_name' => Yii::t('app', 'Department Role Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentSuperiorities()
    {
        return $this->hasMany(DepartmentSuperiority::className(), ['department_role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentSuperiorities0()
    {
        return $this->hasMany(DepartmentSuperiority::className(), ['superior_department_role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployees()
    {
        return $this->hasMany(Employee::className(), ['department_role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSigGolongans()
    {
        return $this->hasMany(SigGolongan::className(), ['department_role_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return DepartmentRoleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DepartmentRoleQuery(get_called_class());
    }
}
