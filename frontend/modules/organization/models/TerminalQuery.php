<?php

namespace app\modules\organization\models;

/**
 * This is the ActiveQuery class for [[Terminal]].
 *
 * @see Terminal
 */
class TerminalQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Terminal[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Terminal|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}