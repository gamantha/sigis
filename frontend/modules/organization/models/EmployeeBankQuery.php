<?php

namespace app\modules\organization\models;

/**
 * This is the ActiveQuery class for [[EmployeeBank]].
 *
 * @see EmployeeBank
 */
class EmployeeBankQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return EmployeeBank[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EmployeeBank|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}