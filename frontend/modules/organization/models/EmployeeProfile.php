<?php

namespace app\modules\organization\models;

use Yii;

/**
 * This is the model class for table "employee_profile".
 *
 * @property integer $id
 * @property integer $employee_id
 * @property string $first_name
 * @property string $last_name
 * @property string $birth_date
 * @property string $birth_place
 *
 * @property Employee $employee
 */
class EmployeeProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id'], 'integer'],
            [['birth_date'], 'safe'],
            [['first_name', 'last_name', 'birth_place'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'employee_id' => Yii::t('app', 'Employee ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'birth_date' => Yii::t('app', 'Birth Date'),
            'birth_place' => Yii::t('app', 'Birth Place'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @inheritdoc
     * @return EmployeeProfileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EmployeeProfileQuery(get_called_class());
    }
}
