<?php

namespace app\modules\organization\models;

/**
 * This is the ActiveQuery class for [[EmployeeProfile]].
 *
 * @see EmployeeProfile
 */
class EmployeeProfileQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return EmployeeProfile[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EmployeeProfile|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}