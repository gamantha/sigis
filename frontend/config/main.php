<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
     use kartik\mpdf\Pdf;

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
     'timezone' => 'Asia/Jakarta',
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'organization' => [
            'class' => 'app\modules\organization\organization'
        ],

        'sig' => [
            'class' => 'app\modules\sig\sig'
        ],
        'hardware' => [
            'class' => 'app\modules\hardware\hardware'
        ],

        'gridview' =>  [
             'class' => '\kartik\grid\Module'
             // enter optional module parameters below - only if you need to
             // use your own export download action or custom translation
             // message source
             // 'downloadAction' => 'gridview/export/download',
             // 'i18n' => []
         ],
    ],
    'components' => [


     'pdf' => [
    'class' => Pdf::classname(),
    'format' => Pdf::FORMAT_A4,
    'orientation' => Pdf::ORIENT_PORTRAIT,
    'destination' => Pdf::DEST_BROWSER,
    // refer settings section for all configuration options
],

     'fingerprint' => [
         'class' => 'common\components\Fingerprint',
   ],

        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
           'enablePrettyUrl' => true,
           'showScriptName' => true,
           'enableStrictParsing' => false,
           'rules' => [
           '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                        '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                        '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
           ],

        ],
    ],
    'params' => $params,
];
