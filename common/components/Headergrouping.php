<?php
namespace common\components;
use yii\base\Widget;
use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;



class Headergrouping extends \kartik\grid\GridView{




 public $mergeHeaders = array();
	private $_mergeindeks = array();
	private $_nonmergeindeks = array();

	public function renderItems()
	{
		if($this->dataProvider->getTotalCount()>0 || $this->showTableOnEmpty)
		{
			echo "<table id='fixtable' class=\"table table-striped table-bordered\">\n";
   	//echo "<table>\n";
			if(!empty($this->mergeHeaders)){
				echo "<thead>\n";
				echo $this->renderGroupHeaders();
				echo "</thead>\n";
			}
			else {
				echo $this->renderTableHeader();
			}
			echo $this->renderTableBody();
			//echo $this->renderTableFooter();
			echo "</table>";
		}
		else
			echo $this->renderEmptyText();
	}

	public function renderGroupHeaders()
	{
		$this->setMergeIndeks();
		$this->setNonMergeIndeks();
		//if($this->filterPosition===self::FILTER_POS_HEADER)
			//$this->renderFilter();
		echo "<tr>\n";

		ob_start();
		echo "<tr>\n";
		$i=0;

		foreach($this->columns as $column){
			if(in_array($i, $this->_mergeindeks)):
				$column->headerOptions['colspan']='1';
				echo $column->renderHeaderCell();

			endif;
			$i++;
		}

		echo "</tr>\n";
		$header_bottom = ob_get_clean();

		$i=0;

  foreach($this->columns as $column){
			for($m=0;$m<count($this->mergeHeaders);$m++){
				if($i==$this->mergeHeaders[$m]["start"]):
					$column->headerOptions['colspan']=$this->mergeHeaders[$m]["end"]-$this->mergeHeaders[$m]["start"]+1;
     //$column->contentOptions
					$column->header = $this->mergeHeaders[$m]["name"];
					//$column->id = NULL;
					echo $column->renderHeaderCell();
				endif;
			}
			if(in_array($i, $this->_nonmergeindeks)){
				$column->headerOptions['rowspan']='2';
				echo $column->renderHeaderCell();
			}
			$i++;
		}

		echo "</tr>\n";

		echo $header_bottom;
		//if($this->filterPosition===self::FILTER_POS_BODY)
			//$this->renderFilter();
	}

	protected function setMergeIndeks()
	{
		for($i=0;$i<count($this->mergeHeaders);$i++)
			for($j=$this->mergeHeaders[$i]["start"];$j<= $this->mergeHeaders[$i]["end"];$j++)
				$this->_mergeindeks[] = $j;
	}

	protected function setNonMergeIndeks()
	{
		foreach($this->columns as $key=>$val) $h[] = $key;
		$this->_nonmergeindeks = array_diff($h, $this->_mergeindeks);
	}

 public function renderTableRow($model, $key, $index)
 {
     $cells = [];
     /* @var $column Column */
     foreach ($this->columns as $column) {
         $cells[] = $column->renderDataCell($model, $key, $index);
     }
     if ($this->rowOptions instanceof Closure) {
         $options = call_user_func($this->rowOptions, $model, $key, $index, $this);
     } else {
         $options = $this->rowOptions;
     }
     $options['data-key'] = is_array($key) ? json_encode($key) : (string) $key;
     return Html::tag('tr', implode('', $cells), $options);
 }

 public function renderDataCell($model, $key, $index)
 {
     if ($this->contentOptions instanceof Closure) {
         $options = call_user_func($this->contentOptions, $model, $key, $index, $this);
     } else {
         $options = $this->contentOptions;
     }
     return Html::tag('td', $this->renderDataCellContent($model, $key, $index), $options);
 }



}
?>