/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50626
Source Host           : 127.0.0.1:3306
Source Database       : sigisdb

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2016-04-29 11:54:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sig_gaji`
-- ----------------------------
DROP TABLE IF EXISTS `sig_gaji`;
CREATE TABLE `sig_gaji` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `sig_project_id` int(11) DEFAULT NULL,
  `gaji_pokok` int(11) DEFAULT NULL,
  `tunjangan_jabatan` int(11) DEFAULT NULL,
  `pendapatan_intern` int(11) DEFAULT NULL,
  `bca` int(11) DEFAULT NULL,
  `btn` int(11) DEFAULT NULL,
  `start_period` datetime DEFAULT NULL,
  `end_period` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sig_gaji_employee_1` (`employee_id`),
  KEY `fk_sig_gaji_sig_project_1` (`sig_project_id`),
  CONSTRAINT `fk_sig_gaji_employee_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`),
  CONSTRAINT `fk_sig_gaji_sig_project_1` FOREIGN KEY (`sig_project_id`) REFERENCES `sig_project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=404 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sig_gaji
-- ----------------------------
INSERT INTO `sig_gaji` VALUES ('187', '63', '3', '1', '4', '6', '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('188', '64', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('189', '65', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('190', '66', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('191', '67', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('192', '68', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('193', '69', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('194', '70', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('195', '71', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('196', '72', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('197', '73', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('198', '74', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('199', '75', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('200', '76', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('201', '77', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('202', '78', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('203', '79', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('204', '80', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('205', '81', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('206', '82', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('207', '83', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('208', '84', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('209', '85', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('210', '86', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('211', '87', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('212', '88', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('213', '89', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('214', '90', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('215', '91', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('216', '92', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('217', '93', '3', null, null, null, '5', '6', '2016-04-24 00:00:00', '2016-04-25 00:00:00');
INSERT INTO `sig_gaji` VALUES ('218', '63', '3', '45', '45', '45', '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('219', '64', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('220', '65', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('221', '66', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('222', '67', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('223', '68', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('224', '69', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('225', '70', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('226', '71', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('227', '72', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('228', '73', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('229', '74', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('230', '75', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('231', '76', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('232', '77', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('233', '78', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('234', '79', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('235', '80', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('236', '81', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('237', '82', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('238', '83', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('239', '84', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('240', '85', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('241', '86', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('242', '87', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('243', '88', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('244', '89', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('245', '90', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('246', '91', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('247', '92', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('248', '93', '3', null, null, null, '8', '23', '2016-04-07 00:00:00', '2016-04-14 00:00:00');
INSERT INTO `sig_gaji` VALUES ('249', '63', '3', '444', '4', '4', '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('250', '64', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('251', '65', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('252', '66', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('253', '67', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('254', '68', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('255', '69', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('256', '70', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('257', '71', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('258', '72', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('259', '73', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('260', '74', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('261', '75', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('262', '76', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('263', '77', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('264', '78', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('265', '79', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('266', '80', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('267', '81', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('268', '82', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('269', '83', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('270', '84', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('271', '85', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('272', '86', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('273', '87', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('274', '88', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('275', '89', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('276', '90', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('277', '91', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('278', '92', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('279', '93', '3', null, null, null, '8', '23', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('280', '63', '3', '444', '4', '4', '4', '4', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('281', '64', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('282', '65', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('283', '66', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('284', '67', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('285', '68', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('286', '69', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('287', '70', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('288', '71', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('289', '72', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('290', '73', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('291', '74', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('292', '75', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('293', '76', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('294', '77', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('295', '78', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('296', '79', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('297', '80', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('298', '81', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('299', '82', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('300', '83', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('301', '84', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('302', '85', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('303', '86', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('304', '87', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('305', '88', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('306', '89', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('307', '90', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('308', '91', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('309', '92', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('310', '93', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('311', '63', '3', '444', '4', '4', '4', '4', '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('312', '64', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('313', '65', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('314', '66', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('315', '67', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('316', '68', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('317', '69', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('318', '70', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('319', '71', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('320', '72', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('321', '73', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('322', '74', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('323', '75', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('324', '76', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('325', '77', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('326', '78', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('327', '79', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('328', '80', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('329', '81', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('330', '82', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('331', '83', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('332', '84', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('333', '85', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('334', '86', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('335', '87', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('336', '88', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('337', '89', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('338', '90', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('339', '91', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('340', '92', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('341', '93', '3', null, null, null, null, null, '2016-04-27 00:00:00', '2016-04-28 00:00:00');
INSERT INTO `sig_gaji` VALUES ('342', '63', '3', '67', '67', '67', '22', '22', '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('343', '64', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('344', '65', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('345', '66', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('346', '67', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('347', '68', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('348', '69', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('349', '70', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('350', '71', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('351', '72', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('352', '73', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('353', '74', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('354', '75', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('355', '76', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('356', '77', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('357', '78', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('358', '79', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('359', '80', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('360', '81', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('361', '82', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('362', '83', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('363', '84', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('364', '85', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('365', '86', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('366', '87', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('367', '88', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('368', '89', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('369', '90', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('370', '91', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('371', '92', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('372', '93', '3', null, null, null, null, null, '2016-04-15 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('373', '63', '3', '2', '2', null, '22', '22', '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('374', '64', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('375', '65', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('376', '66', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('377', '67', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('378', '68', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('379', '69', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('380', '70', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('381', '71', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('382', '72', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('383', '73', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('384', '74', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('385', '75', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('386', '76', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('387', '77', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('388', '78', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('389', '79', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('390', '80', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('391', '81', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('392', '82', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('393', '83', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('394', '84', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('395', '85', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('396', '86', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('397', '87', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('398', '88', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('399', '89', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('400', '90', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('401', '91', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('402', '92', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
INSERT INTO `sig_gaji` VALUES ('403', '93', '3', null, null, null, null, null, '2016-04-12 00:00:00', '2016-04-16 00:00:00');
