CREATE TABLE `auth_assignment` (
`item_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
`user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
`created_at` int(11) NULL DEFAULT NULL,
PRIMARY KEY (`item_name`, `user_id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `auth_item` (
`name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
`type` int(11) NOT NULL,
`description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
`rule_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
`data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
`created_at` int(11) NULL DEFAULT NULL,
`updated_at` int(11) NULL DEFAULT NULL,
PRIMARY KEY (`name`) ,
INDEX `rule_name` (`rule_name`),
INDEX `idx-auth_item-type` (`type`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `auth_item_child` (
`parent` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
`child` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
PRIMARY KEY (`parent`, `child`) ,
INDEX `child` (`child`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `auth_rule` (
`name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
`data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
`created_at` int(11) NULL DEFAULT NULL,
`updated_at` int(11) NULL DEFAULT NULL,
PRIMARY KEY (`name`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `department` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`organization_id` int(11) NULL DEFAULT NULL,
`department_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
PRIMARY KEY (`id`) ,
INDEX `fk_department_organization_1` (`organization_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=16;

CREATE TABLE `department_role` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`department_id` int(11) NULL DEFAULT NULL,
`department_role_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
PRIMARY KEY (`id`) ,
INDEX `fk_department_role_department_1` (`department_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=24;

CREATE TABLE `department_superiority` (
`department_role_id` int(11) NULL DEFAULT NULL,
`superior_department_role_id` int(11) NULL DEFAULT NULL,
INDEX `fk_department_superiority_department_role_1` (`department_role_id`),
INDEX `fk_department_superiority_department_role_2` (`superior_department_role_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci;

CREATE TABLE `employee` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`department_role_id` int(11) NULL DEFAULT NULL,
`org_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`status` enum('inactive','active') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
PRIMARY KEY (`id`) ,
INDEX `org_id` (`org_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=114;

CREATE TABLE `employee_bank` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`employee_id` int(11) NULL DEFAULT NULL,
`bank_name` enum('btn','bca') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`account_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`account_number` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`order` int(11) NULL DEFAULT NULL,
PRIMARY KEY (`id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=105;

CREATE TABLE `employee_contact` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`employee_id` int(11) NULL DEFAULT NULL,
`contact_type` enum('country','street','city','work','home','mobile','province','email') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`contact_detail` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
PRIMARY KEY (`id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=274;

CREATE TABLE `employee_identification` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`employee_id` int(11) NULL DEFAULT NULL,
`identification_type` enum('kpj','npwp','sim','paspor','ktp') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`identification_detail` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
PRIMARY KEY (`id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=236;

CREATE TABLE `employee_profile` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`employee_id` int(11) NULL DEFAULT NULL,
`first_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`last_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`birth_date` datetime NULL DEFAULT NULL,
`birth_place` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
PRIMARY KEY (`id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=68;

CREATE TABLE `employment` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`employee_id` int(11) NULL DEFAULT NULL,
`first_day` datetime NULL DEFAULT NULL,
`contract_type` enum('percobaan','kontrak','tetap') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`start` datetime NULL DEFAULT NULL,
`finish` datetime NULL DEFAULT NULL,
PRIMARY KEY (`id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=68;

CREATE TABLE `employment_sig` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`employee_id` int(11) NULL DEFAULT NULL,
`status` enum('K3','K2','K1','K0','TK') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`value` enum('false','true') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
PRIMARY KEY (`id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=24;

CREATE TABLE `ijin` (
`id` bigint(11) NOT NULL AUTO_INCREMENT,
`employee_id` int(11) NOT NULL,
`tipeijin_id` int(11) NOT NULL,
`start` date NOT NULL,
`finish` date NOT NULL,
`status` enum('approved','rejected','pending') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
`manager_id` int(11) NOT NULL,
`created` datetime NOT NULL,
`modified` datetime NOT NULL,
PRIMARY KEY (`id`) ,
INDEX `employee_id` (`employee_id`),
INDEX `tipeijin_id` (`tipeijin_id`),
INDEX `tipeijin_id_2` (`tipeijin_id`),
INDEX `tipeijin_id_3` (`tipeijin_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=1;

CREATE TABLE `leave` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`leave_type` enum('cuti','ijin','sakit','lembur','exempt') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`employee_id` int(11) NULL DEFAULT NULL,
`start_period` datetime NULL DEFAULT NULL,
`finish_period` datetime NULL DEFAULT NULL,
`status` enum('pending','rejected','approved') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
PRIMARY KEY (`id`) ,
INDEX `fk_leave_employee_1` (`employee_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=1;

CREATE TABLE `log` (
`id` bigint(20) NOT NULL AUTO_INCREMENT,
`level` int(11) NULL DEFAULT NULL,
`category` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`log_time` double NULL DEFAULT NULL,
`prefix` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
`message` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
PRIMARY KEY (`id`) ,
INDEX `idx_log_level` (`level`),
INDEX `idx_log_category` (`category`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=15;

CREATE TABLE `logreno` (
`id` bigint(20) NULL DEFAULT NULL,
`user_id` int(11) NULL DEFAULT NULL,
`table` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`data` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
`action` enum('deleted','updated','created') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`created` datetime NULL DEFAULT NULL
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci;

CREATE TABLE `migration` (
`version` varchar(180) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
`apply_time` int(11) NULL DEFAULT NULL,
PRIMARY KEY (`version`) 
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci;

CREATE TABLE `organization` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`organization_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
PRIMARY KEY (`id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=100;

CREATE TABLE `quota_ijin` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`golongan_id` int(11) NOT NULL,
`tipeijin_id` int(11) NOT NULL,
`quota_bulan` int(11) NOT NULL,
`quota_tahun` int(11) NOT NULL,
PRIMARY KEY (`id`) ,
INDEX `golongan_id` (`golongan_id`),
INDEX `golongan_id_2` (`golongan_id`),
INDEX `golongan_id_3` (`golongan_id`),
INDEX `golongan_id_4` (`golongan_id`),
INDEX `golongan_id_5` (`golongan_id`),
INDEX `golongan_id_6` (`golongan_id`),
INDEX `tipeijin_id` (`tipeijin_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=41;

CREATE TABLE `raw` (
`terminal_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`pin` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
`datetime` datetime NOT NULL,
`verified` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
`status` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
`workcode` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
UNIQUE INDEX `row` (`pin`, `datetime`, `verified`, `status`, `workcode`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci;

CREATE TABLE `ref_tipeijin` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`tipeijin` enum('sakit','ijin','cuti','telat','travel') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`time_limit` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`sesudah` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
PRIMARY KEY (`id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=11;

CREATE TABLE `role` (
`user_id` int(11) NULL DEFAULT NULL,
`role` enum('admin','user') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci;

CREATE TABLE `schedule` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`department_role_id` int(11) NULL DEFAULT NULL,
`start_period` date NULL DEFAULT NULL,
`finish_period` date NULL DEFAULT NULL,
`schedule_type` enum('onetime','yearly','monthly','weekly','daily') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
`day` int(11) NULL DEFAULT NULL,
`start_time` time NULL DEFAULT NULL,
`finish_time` time NULL DEFAULT NULL,
`start_scan` time NULL DEFAULT NULL,
`finish_scan` time NULL DEFAULT NULL,
`optional` enum('false','true','exception') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
`priority` int(11) NULL DEFAULT NULL,
PRIMARY KEY (`id`) ,
INDEX `fk_schedule_weekly_employee_cat_1` (`department_role_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=1;

CREATE TABLE `schedule_grouping` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`group_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`schedule_id_in` int(11) NULL DEFAULT NULL,
`schedule_id_out` int(11) NULL DEFAULT NULL,
PRIMARY KEY (`id`) ,
INDEX `fk_schedule_grouping_schedule_2` (`schedule_id_in`),
INDEX `fk_schedule_grouping_schedule_3` (`schedule_id_out`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=1;

CREATE TABLE `sig_bpjs` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`tipe` enum('bpjs_3','bpjs_2','bpjs_1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`value` int(11) NULL DEFAULT NULL,
PRIMARY KEY (`id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=1;

CREATE TABLE `sig_gaji` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`employee_id` int(11) NULL DEFAULT NULL,
`sig_project_id` int(11) NULL DEFAULT NULL,
`gaji_pokok` int(11) NULL DEFAULT NULL,
`tunjangan_jabatan` int(11) NULL DEFAULT NULL,
`pendapatan_intern` int(11) NULL DEFAULT NULL,
`bca` int NULL,
`btn` int NULL,
`start_period` datetime NULL DEFAULT NULL,
`end_period` datetime NULL DEFAULT NULL,
PRIMARY KEY (`id`) ,
INDEX `fk_sig_gaji_employee_1` (`employee_id`),
INDEX `fk_sig_gaji_sig_project_1` (`sig_project_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=162;

CREATE TABLE `sig_golongan` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`golongan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`department_role_id` int(11) NULL DEFAULT NULL,
`masa_kerja_minimal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`tunjangan_makan_transport` int(11) NULL DEFAULT NULL,
`tunjangan_premi_hadir` int(11) NULL DEFAULT NULL,
`tunjangan_bonus_hadir` int(11) NULL DEFAULT NULL,
`tunjangan_lembur` int(11) NULL DEFAULT NULL,
`tunjangan_lembur_luarkota` int(11) NULL DEFAULT NULL,
`denda_keterlambatan` int(11) NULL DEFAULT NULL,
`plafon_kasbon` int(11) NULL DEFAULT NULL,
`plafon_kesehatan` int(11) NULL DEFAULT NULL,
PRIMARY KEY (`id`) ,
INDEX `fk_sig_golongan_department_role_1` (`department_role_id`),
INDEX `golongan` (`golongan`),
INDEX `golongan_2` (`golongan`, `id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=56;

CREATE TABLE `sig_kasbon` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`employee_id` int(11) NULL DEFAULT NULL,
`sig_project_id` int(11) NULL DEFAULT NULL,
`pengajuankasbon` int(11) NULL DEFAULT NULL,
`potonganperbulan` int(11) NULL DEFAULT NULL,
`keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
`status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
PRIMARY KEY (`id`) ,
INDEX `fk_sig_potongan_kasbon_employee_1` (`employee_id`),
INDEX `fk_sig_kasbon_sig_project_1` (`sig_project_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=21;

CREATE TABLE `sig_pengembalian_kasbon` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`kasbon_id` int(11) NULL DEFAULT NULL,
`pembayaran_ke` int(11) NULL DEFAULT NULL,
`jatuh_tempo` date NULL DEFAULT NULL,
`besaran` int(11) NULL DEFAULT NULL,
`status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
PRIMARY KEY (`id`) ,
INDEX `fk_sig_pengembalian_kasbon_sig_kasbon_1` (`kasbon_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=231;

CREATE TABLE `sig_pph` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`sig_project_id` int(11) NULL DEFAULT NULL,
`tipe` enum('K3','K2','K1','K0','TK','tidak_punya_npwp','takehome_pay_3','takehome_pay_2','takehome_pay_1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`value_1` int(11) NULL DEFAULT NULL,
`value_2` int(11) NULL DEFAULT NULL,
`operator` enum('==','<=','<','>=','>') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
PRIMARY KEY (`id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=34;

CREATE TABLE `sig_project` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`shortname` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`status` enum('inactive','active') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
PRIMARY KEY (`id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=6;

CREATE TABLE `terminal` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`organization_id` int(11) NULL DEFAULT NULL,
`model_id` int(11) NULL DEFAULT NULL,
`name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`configuration` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'json file:\r\nip,\r\nport,\r\nkey',
`status` enum('inactive','active') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
PRIMARY KEY (`id`) ,
INDEX `fk_terminal_model_1` (`model_id`),
INDEX `fk_terminal_organization_1` (`organization_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=4;

CREATE TABLE `terminal_model` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`model_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`make` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`year` int(11) NULL DEFAULT NULL,
`default_configuration` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
PRIMARY KEY (`id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=2;

CREATE TABLE `user` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
`auth_key` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
`password_hash` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
`password_reset_token` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
`email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
`status` smallint(6) NOT NULL DEFAULT 10,
`created_at` int(11) NOT NULL,
`updated_at` int(11) NOT NULL,
PRIMARY KEY (`id`) ,
UNIQUE INDEX `username` (`username`),
UNIQUE INDEX `email` (`email`),
UNIQUE INDEX `password_reset_token` (`password_reset_token`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=6;

CREATE TABLE `useremployee` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`user_id` int(11) NULL DEFAULT NULL,
`employee_id` int(11) NULL DEFAULT NULL,
PRIMARY KEY (`id`) ,
INDEX `fk_useremployee_employee_1` (`employee_id`),
INDEX `fk_useremployee_user_1` (`user_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=1;


ALTER TABLE `auth_assignment` ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`);
ALTER TABLE `auth_item` ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`);
ALTER TABLE `auth_item_child` ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`);
ALTER TABLE `auth_item_child` ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`);
ALTER TABLE `department` ADD CONSTRAINT `fk_department_organization_1` FOREIGN KEY (`organization_id`) REFERENCES `organization` (`id`);
ALTER TABLE `department_role` ADD CONSTRAINT `fk_department_role_department_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`);
ALTER TABLE `department_superiority` ADD CONSTRAINT `fk_department_superiority_department_role_1` FOREIGN KEY (`department_role_id`) REFERENCES `department_role` (`id`);
ALTER TABLE `department_superiority` ADD CONSTRAINT `fk_department_superiority_department_role_2` FOREIGN KEY (`superior_department_role_id`) REFERENCES `department_role` (`id`);
ALTER TABLE `ijin` ADD CONSTRAINT `employee` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`);
ALTER TABLE `leave` ADD CONSTRAINT `fk_leave_employee_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`);
ALTER TABLE `quota_ijin` ADD CONSTRAINT `golongan` FOREIGN KEY (`golongan_id`) REFERENCES `sig_golongan` (`id`);
ALTER TABLE `quota_ijin` ADD CONSTRAINT `tipeijin` FOREIGN KEY (`tipeijin_id`) REFERENCES `ref_tipeijin` (`id`);
ALTER TABLE `schedule` ADD CONSTRAINT `fk_schedule_department_role_1` FOREIGN KEY (`department_role_id`) REFERENCES `department_role` (`id`);
ALTER TABLE `schedule_grouping` ADD CONSTRAINT `fk_schedule_grouping_schedule_1` FOREIGN KEY (`schedule_id_in`) REFERENCES `schedule` (`id`);
ALTER TABLE `schedule_grouping` ADD CONSTRAINT `fk_schedule_grouping_schedule_2` FOREIGN KEY (`schedule_id_out`) REFERENCES `schedule` (`id`);
ALTER TABLE `sig_gaji` ADD CONSTRAINT `fk_sig_gaji_employee_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`);
ALTER TABLE `sig_gaji` ADD CONSTRAINT `fk_sig_gaji_sig_project_1` FOREIGN KEY (`sig_project_id`) REFERENCES `sig_project` (`id`);
ALTER TABLE `sig_golongan` ADD CONSTRAINT `fk_sig_golongan_department_role_1` FOREIGN KEY (`department_role_id`) REFERENCES `department_role` (`id`);
ALTER TABLE `sig_kasbon` ADD CONSTRAINT `fk_sig_kasbon_sig_project_1` FOREIGN KEY (`sig_project_id`) REFERENCES `sig_project` (`id`);
ALTER TABLE `sig_kasbon` ADD CONSTRAINT `fk_sig_potongan_kasbon_employee_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`);
ALTER TABLE `sig_pengembalian_kasbon` ADD CONSTRAINT `fk_sig_pengembalian_kasbon_sig_kasbon_1` FOREIGN KEY (`kasbon_id`) REFERENCES `sig_kasbon` (`id`);
ALTER TABLE `terminal` ADD CONSTRAINT `fk_terminal_model_1` FOREIGN KEY (`model_id`) REFERENCES `terminal_model` (`id`);
ALTER TABLE `terminal` ADD CONSTRAINT `fk_terminal_organization_1` FOREIGN KEY (`organization_id`) REFERENCES `organization` (`id`);
ALTER TABLE `useremployee` ADD CONSTRAINT `fk_useremployee_employee_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`);
ALTER TABLE `useremployee` ADD CONSTRAINT `fk_useremployee_user_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

