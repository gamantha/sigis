/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50626
Source Host           : 127.0.0.1:3306
Source Database       : sigisdb

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2016-04-28 12:14:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `schedule_grouping`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_grouping`;
CREATE TABLE `schedule_grouping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) DEFAULT NULL,
  `schedule_id_in` int(11) DEFAULT NULL,
  `schedule_id_out` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_schedule_grouping_schedule_2` (`schedule_id_in`),
  KEY `fk_schedule_grouping_schedule_3` (`schedule_id_out`),
  CONSTRAINT `fk_schedule_grouping_schedule_1` FOREIGN KEY (`schedule_id_in`) REFERENCES `schedule` (`id`),
  CONSTRAINT `fk_schedule_grouping_schedule_2` FOREIGN KEY (`schedule_id_out`) REFERENCES `schedule` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of schedule_grouping
-- ----------------------------
INSERT INTO `schedule_grouping` VALUES ('1', 'masuk - pulang role 12', '1', '5');
